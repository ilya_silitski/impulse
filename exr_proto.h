#ifndef EXR_PROTO_H__
#define EXR_PROTO_H__

#include <cstdint>
typedef enum
{
    EXR_NOT_RUNNING,
    EXR_RUN,
    EXR_ATTACH,
    EXR_DEBUG,
    EXR_PAUSE,
    EXR_STOP,
    EXR_STEP,
    EXR_CONT,
    EXR_JUMP,
    EXR_GET_STATE,
    EXR_GET_VARS,
    EXR_STDOUT,
    EXR_STDERR,
    EXR_CONNECTED,
    EXR_TERMINATED
} exr_cmd_t;

typedef enum
{
    EXT_TRM_EXITCODE,
    EXT_TRM_SIGNAL
} exr_trm_type_t;

typedef struct
{
    int32_t type;
    int32_t value;
} exr_trm_stat_t;

typedef struct
{
    int32_t line;
    int32_t paused;
} exr_state_t;

#endif
