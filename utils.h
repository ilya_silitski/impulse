#ifndef UTILS_H
#define UTILS_H

#include <QVariantList>
#include <QDebug>
#include <math.h>

inline double PRETTY_LEN(double value) {
    return value * 1000.0;
}

inline double PRETTY_ANG(double value) {
    return value * 180.0 / M_PI;
}

inline double PARSE_LEN(double value) {
    return value / 1000.0;
}

inline double PARSE_ANG(double value) {
    return value * M_PI / 180.0;
}

inline void PARSE_POSITION(double *src, double *des) {
    for (int i = 0; i < 6; i++)
    {
        des[i] = i < 3 ? PARSE_LEN(src[i]) : PARSE_ANG(src[i]);
    }
}

inline void PARSE_POSE(double *src, double *des) {
    for (int i = 0; i < 6; i++)
    {
        des[i] = PARSE_ANG(src[i]);
    }
}

inline void PRETTY_POSITION(double *src, double *des) {
    for (int i = 0; i < 6; i++)
    {
        des[i] = i < 3 ? PRETTY_LEN(src[i]) : PRETTY_ANG(src[i]);
    }
}

inline QVariantList PRETTY_POSITION(QVariantList position) {
    for (int i = 0; i < 6; i++)
    {
        position[i] = i < 3 ? PRETTY_LEN(position[i].toDouble()) : PRETTY_ANG(position[i].toDouble());
    }
    return position;
}

inline void PRETTY_POSE(double *src, double *des) {
    for (int i = 0; i < 6; i++)
    {
        des[i] = PRETTY_ANG(src[i]);
    }
}

inline QVariantList PRETTY_POSE(QVariantList pose) {
    for (int i = 0; i < 6; i++)
    {
        pose[i] = PRETTY_ANG(pose[i].toDouble());
    }
    return pose;
}

inline void COPY_VECTOR(double *src, double *des, int len) {
    for (int i = 0; i < len; i++)
    {
        des[i] = src[i];
    }
}

inline void COPY_VECTOR(u_int8_t *src, u_int8_t *des, int len) {
    for (int i = 0; i < len; i++)
    {
        des[i] = src[i];
    }
}

inline void COPY_VECTOR_TO_LIST(double *src, QVariantList *des, int len) {
    for (int i = 0; i < len; i++)
    {
        des->append(src[i]);
    }
}

inline void COPY_VECTOR_TO_LIST(uint8_t *src, QVariantList *des, int len) {
    for (int i = 0; i < len; i++)
    {
        des->append(src[i]);
    }
}

inline void COPY_IO_TO_LIST(uint8_t *src, QVariantList *des, size_t len) {
    for (size_t i = 0; i < len; i++)
    {
        for (size_t j = 0; j < sizeof(src); j++)
        {
            des->append((src[i] >> j) & 1);
        }
    }
}

inline void COPY_IO_TO_VECTOR(uint8_t *src, bool *des, size_t len) {
    for (size_t i = 0; i < len; i++)
    {
        for (size_t j = 0; j < sizeof(src); j++)
        {
            des[i * sizeof(src) + j] = ((src[i] >> j) & 1);
        }
    }
}

inline void COPY_LIST_TO_VECTOR(QVariantList *src, double *des) {
    for (int i = 0; i < src->size(); i++)
    {
        des[i] =  src->at(i).toDouble();
    }
}

inline double DISTANCE(double *from, double *to, int size) {
    double result = 0;
    for (int i = 0; i < size; i++)
    {
        result += pow(to[i] - from[i], 2);
    }
    return sqrt(result);
}



#endif // UTILS_H
