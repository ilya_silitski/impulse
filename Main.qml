import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts
import Backend
import "Elements"
import "Components"
import "Tabs"

Window {
    id: window
    width: 1280
    height: 800
    visible: true
    color: "#ffffff"

    Backend {
        id: backend
    }

    Component.onCompleted: {
        backend.startup();
    }

    readonly property string robotModel: "N7"
    readonly property bool robotRunning: realTimeData ? realTimeData.state === Backend.Run : false

    property bool readOnlyMode: false

    property var programsList
    property var currentSetup
    property var currentProgram
    property var currentProgramName
    property var logsHistory
    property var realTimeData
    property string executionStatus: "stopped"

    property string status: {
        if (!realTimeData) return "N/A";
        switch (realTimeData.state) {
        case Backend.Idle: return "Idle";
        case Backend.Off: return "Off";
        case Backend.Stby: return "Stand By";
        case Backend.On: return "On";
        case Backend.Run:
            switch (realTimeData.motionMode) {
            case Backend.Hold: return "Hold";
            case Backend.ZeroGravity: return "Zero Gravity";
            case Backend.Jog: return "Jogging";
            case Backend.Move: return "Moving";
            default: return "Run"
            }
        case Backend.Failure: return "Failure";
        case Backend.ForceExit: return "Force Exit";
        default: return "UNKNOWN";
        }
    }

    Connections {
        target: backend

//        function onControlUnavailable() {
//            readOnlyMode = true;
//            messageModal.title = "Read-only mode";
//            messageModal.description = "Control socket disconnected, proceeding in read-only mode.";
//            messageModal.type = MessageModal.Warning;
//            messageModal.okButtonType = PushButton.Primary;
//            messageModal.okText = "Continue";
//            messageModal.open();
//        }

        function onGeneralDataUpdated(data) {
            realTimeData = data;
        }

        function onEmulatorDataUpdated(data) {
            motionTab.updateRobotScene(data.position, data.pose);
        }

        function onLogsUpdated(history) {
            logsHistory = history;
        }

        function onCurrentSetupUpdated(currentSetup) {
            window.currentSetup = JSON.parse(currentSetup);
        }

        function onCurrentToolUpdated(currentTool) {
            console.log("Current Tool:", currentTool);
            setupTab.currentTool = JSON.parse(currentTool);
        }

        function onCurrentPayloadUpdated(currentPayload) {
            console.log("Current Payload:", currentPayload);
            setupTab.currentPayload = JSON.parse(currentPayload);
        }

        function onProgramsListUpdated(programsList) {
            window.programsList = programsList.filter(a => a.type === 1);
        }

        function onCurrentProgramUpdated(currentProgram) {
            window.currentProgram = JSON.parse(currentProgram);
        }

        function onCurrentProgramNameUpdated(currentProgramName) {
            window.currentProgramName = currentProgramName;
        }

        function onCurrentCommandUpdated(currentCommand) {
            programTab.currentCommand = JSON.parse(currentCommand);
        }

        function onProgramMessage(type, title, message) {
            messageModal.title = title;
            messageModal.description = message;
            messageModal.open();
        }

        function onBufferUpdated(bufferEmpty) {
            programTab.bufferEmpty = bufferEmpty;
        }

        function onOpenZeroGravity() {
            if (currentTab !== Main.Tab.Motion) goToTab(Main.Tab.Motion);
            motionTab.zeroGravityOpen = true;
        }

        function onCloseZeroGravity() {
            motionTab.zeroGravityOpen = false;
        }

        function onProgramRunning() {
            if (programInterruptedModal.visible) programInterruptedModal.close();
            executionStatus = "running";
        }

        function onProgramPaused() {
            executionStatus = "paused";
        }

        function onProgramFinished(type, val) {
            executionStatus = "stopped";
            let msg = 0
            switch (type)
            {
            case 0:
                msg = ' exit code ' + val;
                break;
            case 1:
                msg = ' signal ' + val;
                break;
            case 2:
                msg = 'unkown reason';
                break;
            default:
                msg = 'unkown reason';
                break;
            }
            programInterruptedModal.title = "Program Finished";
            programInterruptedModal.description = 'Program was finished with' + msg + '.';
            programInterruptedModal.open();
        }

        function onProgramStopped() {
            executionStatus = "stopped";
        }

//        function onExecutingCommandIdUpdated(executingCommandId) {
//            programTab.executingCommandId = executingCommandId;
//        }

//        function onCurrentPointIdUpdated(pointId) {
//            programTab.currentPointId = pointId;
//        }

        function onShowPopup(popupType, message, buttonText, playSound, soundName) {
            popupCommandModal.type = popupType;
            popupCommandModal.description = message;
            popupCommandModal.okText = buttonText;
            popupCommandModal.open();
            if (playSound) backend.playSound(soundName);
        }

        function onCoordinateSystemUpdated(inTCP) {
            motionTab.coordinateSystem = inTCP;
        }

        function onToolUpdated(tool) {
            console.log("Tool:", JSON.stringify(tool));
            motionTab.tool = tool;
        }

        function onPayloadUpdated(payload) {
            console.log("Payload:", JSON.stringify(payload));
            setupTab.payload = payload;
        }

        function onZeroGravityUpdated(enabled) {
            motionTab.zeroGravityActive = enabled;
        }
    }

    enum Tab {
        Program,
        Setup,
        Motion,
        IO,
        Dashboard
    }

    property int currentTab: Main.Tab.Program

    function goToTab(tab) {
        window.currentTab = tab;
    }

    function prettyPosition(position) {
        return position.map((p, i) => i < 3 ? p * 1000 : p * 180 / Math.PI);
    }

    function prettyPose(pose) {
        return pose.map(p => p * 180 / Math.PI);
    }

    function arrivedToPoint(target, current, delta = 0.05) {
        return current.every((x, i)=> Math.abs(target[i] - x) < delta);
    }

    MessageModal {
        id: messageModal

        onClosed: {
            type = MessageModal.Info
            title = "Title"
            description = "Description"
            okText = "Ok"
            cancelText = "Cancel"
            showCancel = false
            showIcon = true
            okButtonType = PushButton.Danger
            cancelButtonType = PushButton.Default
        }

        onOk: messageModal.close();
    }

    MessageModal {
        id: programInterruptedModal
        showCancel: true
        cancelButtonType: PushButton.Danger
        okButtonType: PushButton.Primary
        okText: "Restart"
        type: MessageModal.Warning

        onOk: backend.runProgram()
        onCancel: programInterruptedModal.close()
    }

    MessageModal {
        id: popupCommandModal
        title: "Info"
        okButtonType: PushButton.Primary
        okText: "Ok"

        onOk: {
            backend.continueProgram();
            popupCommandModal.close();
        }
    }

    Keyboard {
        id: keyboard

        function newProgram() {
            keyboard.show("newProgram");
        }

        function comment(defaultText) {
            keyboard.defaultText = defaultText;
            keyboard.show("comment");
        }

        function expression(defaultText) {
            keyboard.defaultText = defaultText;
            keyboard.show("expression");
        }

        function renamePoint(defaultText, names, pointId) {
            keyboard.defaultText = defaultText;
            keyboard.names = names;
            keyboard.pointId = pointId;
            keyboard.show("renamePoint");
        }

        function renameVariable(defaultText, names, variableId) {
            keyboard.defaultText = defaultText;
            keyboard.names = names;
            keyboard.variableId = variableId;
            keyboard.show("renameVariable");
        }

        function renameIO(defaultText, names, mode, index) {
            keyboard.defaultText = defaultText;
            keyboard.names = names;
            keyboard.ioIndex = index;
            keyboard.show(mode);
        }

        function renameTool(defaultText, names, toolId = "") {
            keyboard.defaultText = defaultText;
            keyboard.names = names;
            keyboard.toolId = toolId;
            keyboard.show("renameTool");
        }

        function renamePayload(defaultText, names, payloadId = "") {
            keyboard.defaultText = defaultText;
            keyboard.names = names;
            keyboard.payloadId = payloadId;
            keyboard.show("renamePayload");
        }

        function message(defaultText) {
            keyboard.defaultText = defaultText;
            keyboard.show("message");
        }

        function buttonText(defaultText) {
            keyboard.defaultText = defaultText;
            keyboard.show("buttonText");
        }

        onSave: {
            switch (keyboard.mode) {
            case "newProgram":
                backend.createNewProgram(result);
                break;
            case "comment":
                backend.changeCommandParameter("comment", result);
                break;
            case "expression":
                backend.changeCommandParameter("expression", result);
                break;
            case "message":
                backend.changeCommandParameter("message", result);
                break;
            case "buttonText":
                backend.changeCommandParameter("buttonText", result);
                break;
            case "renamePoint":
                backend.renamePoint(keyboard.pointId, result);
                break;
            case "renameVariable":
                backend.renameVariable(keyboard.variableId, result);
                break;
            case "renameTool":
                if (keyboard.toolId) backend.renameTool(result, keyboard.toolId);
                else backend.renameTool(result);
                break;
            case "renamePayload":
                if (keyboard.payloadId) backend.renamePayload(result, keyboard.payloadId);
                else backend.renamePayload(result);
                break;
            case "renameDigitalInput":
                backend.renameIO("di", keyboard.ioIndex, result);
                break;
            case "renameDigitalOutput":
                backend.renameIO("do", keyboard.ioIndex, result);
                break;
            case "renameAnalogInput":
                backend.renameIO("ai", keyboard.ioIndex, result);
                break;
            case "renameAnalogOutput":
                backend.renameIO("ao", keyboard.ioIndex, result);
                break;
            }
        }
    }

    TabBar {
        id: tabBar
        height: 60
        position: TabBar.Left
        anchors.left: parent.left
        anchors.top: parent.top
        currentIndex: window.currentTab
        font.pixelSize: 14

        TabButton {
            id: programTabButton
            width: 120
            height: parent.height
            text: "Program"
            anchors.left: parent.left
            anchors.top: parent.top
            font.weight: checked ? Font.DemiBold : Font.Normal
            enabled: !motionTab.hasOperation

            onClicked: parent.enabled ? goToTab(Main.Tab.Program) : null

            contentItem: Text {
                text: parent.text
                font: parent.font

                color: parent.enabled ? parent.checked ? "#4682b4" : "#000000" : "#888888"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            background: Rectangle {
                border.width: 0
                color: parent.down ? "#f4f4f4" : parent.checked ? "#e8e8e8" : "#ffffff"
                anchors.fill: parent
                radius: 12

                Rectangle {
                    height: parent.height / 2
                    width: parent.width
                    border.width: 0
                    color: parent.color
                    anchors.bottom: parent.bottom
                }
            }
        }

        TabButton {
            id: setupTabButton
            width: 120
            height: parent.height
            text: "Setup"
            anchors.left: programTabButton.right
            anchors.top: parent.top
            font.weight: checked ? Font.DemiBold : Font.Normal
            enabled: !motionTab.hasOperation

            onClicked: parent.enabled ? goToTab(Main.Tab.Setup) : null

            contentItem: Text {
                text: parent.text
                font: parent.font
                color: parent.enabled ? parent.checked ? "#4682b4" : "#000000" : "#888888"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            background: Rectangle {
                border.width: 0
                color: parent.down ? "#f4f4f4" : parent.checked ? "#e8e8e8" : "#ffffff"
                anchors.fill: parent
                radius: 12

                Rectangle {
                    height: parent.height / 2
                    width: parent.width
                    border.width: 0
                    color: parent.color
                    anchors.bottom: parent.bottom
                }
            }
        }

        TabButton {
            id: motionTabButton
            width: 120
            height: parent.height
            text: "Motion"
            anchors.left: setupTabButton.right
            anchors.top: parent.top
            font.weight: checked ? Font.DemiBold : Font.Normal

            onClicked: parent.enabled ? goToTab(Main.Tab.Motion) : null

            contentItem: Text {
                text: parent.text
                font: parent.font
                color: parent.enabled ? parent.checked ? "#4682b4" : "#000000" : "#888888"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            background: Rectangle {
                border.width: 0
                color: parent.down ? "#f4f4f4" : parent.checked ? "#e8e8e8" : "#ffffff"
                anchors.fill: parent
                radius: 12

                Rectangle {
                    height: parent.height / 2
                    width: parent.width
                    border.width: 0
                    color: parent.color
                    anchors.bottom: parent.bottom
                }
            }
        }

        TabButton {
            id: ioTabButton
            width: 120
            height: parent.height
            text: "I/O"
            anchors.left: motionTabButton.right
            anchors.top: parent.top
            font.weight: checked ? Font.DemiBold : Font.Normal
            enabled: !motionTab.hasOperation

            onClicked: parent.enabled ? goToTab(Main.Tab.IO) : null

            contentItem: Text {
                text: parent.text
                font: parent.font
                color: parent.enabled ? parent.checked ? "#4682b4" : "#000000" : "#888888"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            background: Rectangle {
                border.width: 0
                color: parent.down ? "#f4f4f4" : parent.checked ? "#e8e8e8" : "#ffffff"
                anchors.fill: parent
                radius: 12

                Rectangle {
                    height: parent.height / 2
                    width: parent.width
                    border.width: 0
                    color: parent.color
                    anchors.bottom: parent.bottom
                }
            }
        }

        TabButton {
            id: dashboardTabButton
            width: 120
            height: parent.height
            text: "Dashboard"
            anchors.left: ioTabButton.right
            anchors.top: parent.top
            font.weight: checked ? Font.DemiBold : Font.Normal
            enabled: !motionTab.hasOperation

            onClicked: parent.enabled ? goToTab(Main.Tab.Dashboard) : null

            contentItem: Text {
                text: parent.text
                font: parent.font
                color: parent.enabled ? parent.checked ? "#4682b4" : "#000000" : "#888888"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            background: Rectangle {
                border.width: 0
                color: parent.down ? "#f4f4f4" : parent.checked ? "#e8e8e8" : "#ffffff"
                anchors.fill: parent
                radius: 12

                Rectangle {
                    height: parent.height / 2
                    border.width: 0
                    color: parent.color
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                }
            }
        }
    }

    Text {
        text: window.currentProgramName || "[No Program Selected]"
        anchors.left: tabBar.right
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.verticalCenter: tabBar.verticalCenter
        font.weight: Font.DemiBold
        font.pixelSize: 18
        height: 36
        color: window.currentProgramName ? "#555555" : "#888888"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    StackLayout {
        id: mainWindowStackLayout
        anchors.left: parent.left
        anchors.top: tabBar.bottom
        anchors.right: parent.right
        anchors.bottom: footer.top
        currentIndex: window.currentTab

        ProgramTab {
            id: programTab
        }

        SetupTab {
            id: setupTab
        }

        MotionTab {
            id: motionTab
        }

        IOTab {
            id: ioTab
        }

        DashboardTab {
            id: dashboardTab
        }
    }

    Frame {
        id: footer
        height: 60
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.leftMargin: 0
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        font.pixelSize: 18

        background: Rectangle {
            border.width: 1
            border.color: "#d9d9d9"
            color: "#ffffff"
            anchors.fill: parent
        }

        SwipeView {
            id: footerView
            interactive: false
            currentIndex: currentView
            anchors.fill: parent

            property int currentView: 1

            Item {

                Row {
                    spacing: 12

                    PushButton {
                        enabled: !window.readOnlyMode
                        width: 96
                        text: "Off"

                        onClicked: backend.turnRobotOff()
                    }

                    PushButton {
                        enabled: !window.readOnlyMode
                        width: 96
                        text: "On"

                        onClicked: backend.turnRobotOn()
                    }

                    PushButton {
                        enabled: !window.readOnlyMode
                        width: 96
                        text: "Run"

                        onClicked: backend.releaseBrakes()
                    }
                }

                Item {
                    anchors.right: parent.right
                    width: 180
                    height: 36

                    Icon {
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        source: "qrc:///img/left"
                        size: 24
                    }

                    Row {
                        anchors.centerIn: parent
                        spacing: 6

                        Text {
                            height: 24
                            font.pixelSize: 18
                            verticalAlignment: Text.AlignVCenter

                            text: window.status
                        }
                    }

                    MouseArea {
                        anchors.fill: parent

                        onClicked: footerView.currentView = 1
                    }
                }
            }

            Item {

                Item {
                    anchors.left: parent.left
                    width: 180
                    height: 36

                    Row {
                        anchors.centerIn: parent
                        spacing: 6

                        Text {
                            height: 24
                            font.pixelSize: 18
                            verticalAlignment: Text.AlignVCenter

                            text: window.status
                        }
                    }

                    Icon {
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter
                        source: "qrc:///img/right"
                        size: 24
                    }

                    MouseArea {
                        anchors.fill: parent

                        onClicked: footerView.currentView = 0
                    }
                }

                Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: 36
                    spacing: 12

                    Label {
                        height: 36
                        text: "Speed:"
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignVCenter
                    }

                    Slide {
                        id: speedSlider
                        anchors.verticalCenter: parent.verticalCenter
                        width: 240
                        height: 24
                        from: 0.01
                        to: 1
                        stepSize: 0.01
                        value: 1
                        enabled: !readOnlyMode

                        onValueChanged: {
                            speedInput.text = (value * 100).toFixed(0) + '%';
                        }
                        onMoved: {
                            backend.changeSpeed(Math.round(value * 100));
                        }
                    }

                    Label {
                        id: speedInput
                        width: 72
                        height: 36
                        text: "100%"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                }

                Row {
                    anchors.right: parent.right
                    width: 180
                    height: 36
                    spacing: 12

                    PushButton {
                        enabled: !motionTab.hasOperation &&
                                 !readOnlyMode &&
                                 !programTab.hasError
                        iconSource: window.executionStatus === "running" ? "qrc:///img/pause" : "qrc:///img/play"
                        type: window.executionStatus === "running" ? PushButton.Primary : PushButton.Success
                        display: AbstractButton.IconOnly
                        shape: PushButton.Circle

                        onClicked: {
                            switch (window.executionStatus){
                            case "running": backend.pauseProgram(); break;
                            case "paused": backend.continueProgram(); break;
                            case "stopped": backend.runProgram(); break;
                            }
                        }
                    }

                    PushButton {
                        enabled: !readOnlyMode
                        iconSource: "qrc:///img/stop"
                        type: PushButton.Danger
                        display: AbstractButton.IconOnly
                        shape: PushButton.Circle

                        onClicked: if (window.executionStatus !== "stopped") backend.stopProgram()
                    }

                    PushButton {
                        enabled: window.executionStatus === "paused" &&
                                 !readOnlyMode
                        iconSource: "qrc:///img/step"
                        display: AbstractButton.IconOnly
                        shape: PushButton.Circle

                        onClicked: if (window.executionStatus === "paused") backend.stepProgram()
                    }
                }
            }
        }

    }

}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.66}
}
##^##*/
