#ifndef TCS_H
#define TCS_H

#define HOST "127.0.0.1"
#define PORT 23032
#define MAX_SIZE 4096

#include <QObject>
#include <QTcpSocket>
#include <QMutex>
#include <QTimer>
#include <QEventLoop>

typedef uint32_t tcs_size_t;
typedef uint32_t tcs_type_t;

typedef enum
{
    TCS_CST_NONE,
    TCS_CST_CONNECTED,
    TCS_CST_DISCONNECTED,
    TCS_CST_CHUNK_RECEIVED,
    TCS_CST_CHUNK_TO_LONG,
    TCS_CST_CHUNK_TO_SHORT,
    TCS_CST_TIMEOUT
} tcs_conn_state_t;

typedef enum
{
    TCS_RST_LEN,
    TCS_RST_TYPE,
    TCS_RST_PLOAD,
} tcs_rcv_state_t;

typedef struct tcs_inst_t_ tcs_inst_t;

class TCS : public QObject
{
    Q_OBJECT
public:
    TCS(QString host = HOST, quint16 port = PORT, quint32 max_size = MAX_SIZE);
    ~TCS();

    bool start();
    bool terminate();
    QString get_connection_error();
    bool send_command(tcs_type_t type, void *payload, size_t size);

private slots:
    void sock_connected();
    void sock_ready_read();
    void sock_error(QAbstractSocket::SocketError err);
    void sock_disconnected();

signals:
    void connected();
    void chunk_received(tcs_conn_state_t conn_state, tcs_type_t type, tcs_size_t payload_size, void *payload);
    void error(QAbstractSocket::SocketError err);
    void disconnected();

private:
    // TCP socket
    QTcpSocket *sock;

    QString host;
    quint16 port;
    quint32 max_size;

    tcs_size_t chunk_size;
    tcs_type_t chunk_type;
    tcs_size_t payload_size;
    uint8_t *payload;

    tcs_rcv_state_t rcv_state;
    tcs_conn_state_t conn_state;
    tcs_size_t ptr = 0;

    QMutex mutex;

};

#endif // TCS_H
