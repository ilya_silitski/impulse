import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import "../Components"
import "../Elements"

TabPane {
    id: setupTab

    property int currentTab: 0
    property alias currentTool: toolsTabPane.currentTool
    property alias currentPayload: payloadsTabPane.currentPayload
    property var payload: null

    Card {
        id: tabsCard
        width: 180
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        Column {
            anchors.fill: parent
            spacing: 12

            ToggleButton {
                width: parent.width
                text: "Tools"
                active: setupTab.currentTab === 0
                onClicked: setupTab.currentTab = 0
            }

            ToggleButton {
                width: parent.width
                text: "Payloads"
                active: setupTab.currentTab === 1
                onClicked: setupTab.currentTab = 1
            }

            ToggleButton {
                enabled: false
                width: parent.width
                text: "Frames"
                active: setupTab.currentTab === 2
                onClicked: setupTab.currentTab = 2
            }

            ToggleButton {
                width: parent.width
                text: "Startup"
                active: setupTab.currentTab === 3
                onClicked: setupTab.currentTab = 3
            }
        }
    }

    StackLayout {
        id: tabPaneLayout
        anchors.left: tabsCard.right
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.leftMargin: 12
        currentIndex: setupTab.currentTab

        TabPane {
            id: toolsTabPane
            color: "transparent"
            padding: 0

            property var tools: window.currentSetup ? window.currentSetup.tools : []
            property var currentTool
            property var tcp: currentTool ? prettyPosition(currentTool.tcp) : []

            RowLayout {
                spacing: 12
                anchors.fill: parent

                Card {
                    id: toolsListCard
                    Layout.preferredWidth: width
                    Layout.fillHeight: true
                    width: 240
                    height: parent.height

                    PushButton {
                        id: addToolButton
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.right: parent.right
                        type: PushButton.Primary
                        text: "Add Tool"

                        onClicked: backend.addTool()
                    }

                    Component {
                        id: toolListItem

                        Rectangle {
                            property bool current: toolsTabPane.currentTool ? toolsTabPane.currentTool.id === modelData.id : false

                            color: current ? "#4682b4" : "transparent"
                            border.width: 0
                            width: toolsListCard.availableWidth
                            height: 36
                            radius: height / 2

                            Text {
                                text: modelData.name
                                anchors.fill: parent
                                leftPadding: 12
                                rightPadding: 12
                                clip: true
                                elide: Text.ElideRight
                                font.pixelSize: 14
                                horizontalAlignment: Text.AlignLeft
                                verticalAlignment: Text.AlignVCenter
                                color: parent.current ? "#ffffff" : "#000000"
                            }

                            MouseArea {
                                anchors.fill: parent

                                onClicked: {
                                    backend.selectTool(modelData.id);
                                }
                            }
                        }
                    }

                    ListView {
                        anchors.left: parent.left
                        anchors.top: addToolButton.bottom
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom
                        anchors.topMargin: 12

                        clip: true
                        spacing: 12

                        model: toolsTabPane.tools

                        delegate: toolListItem
                    }
                }

                Card {
                    id: toolParamsCard
                    Layout.preferredWidth: width
                    Layout.fillHeight: true
                    width: 360
                    height: parent.height

                    Column {
                        spacing: 12
                        anchors.left: parent.left
                        anchors.right: parent.right

                        RowLayout {
                            spacing: 12
                            anchors.left: parent.left
                            anchors.right: parent.right

                            PushButton {
                                Layout.preferredHeight: 36
                                Layout.preferredWidth: 36
                                iconSource: "qrc:///img/editor/rename"
                                type: PushButton.Primary
                                display: AbstractButton.IconOnly
                                shape: PushButton.Circle
                                onClicked: {
                                    const defaultText = toolsTabPane.currentTool ? toolsTabPane.currentTool.name : "";
                                    const names = window.currentSetup ? window.currentSetup.tools.map(t => t.name) : [];
                                    keyboard.renameTool(defaultText, names);
                                }
                            }

                            Text {
                                Layout.fillWidth: true
                                text: toolsTabPane.currentTool ? toolsTabPane.currentTool.name : "Tool"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                                elide: Text.ElideRight
                            }
                        }

                        InputNumber {
                            label: "X:"
                            value: toolsTabPane.tcp[0] || 0
                            suffix: " mm"
                            precision: 2
                            labelToLeft: true
                            rightPadding: 12
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Left
                            onSave: {
                                backend.editTool(0, result / 1000);
                            }
                        }

                        InputNumber {
                            label: "Y:"
                            value: toolsTabPane.tcp[1] || 0
                            suffix: " mm"
                            precision: 2
                            labelToLeft: true
                            rightPadding: 12
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Left
                            onSave: {
                                backend.editTool(1, result / 1000);
                            }
                        }

                        InputNumber {
                            label: "Z:"
                            value: toolsTabPane.tcp[2] || 0
                            suffix: " mm"
                            precision: 2
                            labelToLeft: true
                            rightPadding: 12
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Left
                            onSave: {
                                backend.editTool(2, result / 1000);
                            }
                        }

                        InputNumber {
                            label: "Roll:"
                            value: toolsTabPane.tcp[3] || 0
                            suffix: "°"
                            precision: 2
                            labelToLeft: true
                            rightPadding: 12
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Left
                            onSave: {
                                backend.editTool(3, result / 180 * Math.PI);
                            }
                        }

                        InputNumber {
                            label: "Pitch:"
                            value: toolsTabPane.tcp[4] || 0
                            suffix: "°"
                            precision: 2
                            labelToLeft: true
                            rightPadding: 12
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Left
                            onSave: {
                                backend.editTool(4, result / 180 * Math.PI);
                            }
                        }

                        InputNumber {
                            label: "Yaw:"
                            value: toolsTabPane.tcp[5] || 0
                            suffix: "°"
                            precision: 2
                            labelToLeft: true
                            rightPadding: 12
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Left
                            onSave: {
                                backend.editTool(5, result / 180 * Math.PI);
                            }
                        }
                    }

                    PushButton {
                        enabled: toolsTabPane.tools.length > 1
                        text: "Delete Tool"
                        type: PushButton.Danger
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom

                        onClicked: confirmDeleteToolModal.open()

                        ConfirmModal {
                            id: confirmDeleteToolModal
                            title: "Delete Tool"
                            description: "Do you really want to permanently delete this tool? It may be referenced in different programs."
                            okText: "Confirm"
                            okButtonType: PushButton.Danger
                            cancelButtonType: PushButton.Default

                            onOk: {
                                backend.deleteTool();
                                confirmDeleteToolModal.close();
                            }
                            onCancel: {
                                confirmDeleteToolModal.close();
                            }
                        }
                    }
                }

                Card {
                    id: toolEmulatorCard
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    height: parent.height
                }
            }
        }

        TabPane {
            id: payloadsTabPane
            color: "transparent"
            padding: 0

            property var payloads: window.currentSetup ? window.currentSetup.payloads : []
            property var currentPayload
            property real mass: currentPayload ? currentPayload.mass : 0.0
            property real comX: currentPayload ? currentPayload.comX * 1000 : 0.0
            property real comY: currentPayload ? currentPayload.comY * 1000 : 0.0
            property real comZ: currentPayload ? currentPayload.comZ * 1000 : 0.0

            RowLayout {
                spacing: 12
                anchors.fill: parent

                Card {
                    id: payloadsListCard
                    Layout.preferredWidth: width
                    Layout.fillHeight: true
                    width: 240
                    height: parent.height

                    PushButton {
                        id: addPayloadButton
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.right: parent.right
                        type: PushButton.Primary
                        text: "Add Payload"

                        onClicked: backend.addPayload()
                    }

                    Component {
                        id: payloadListItem

                        Rectangle {
                            property bool current: payloadsTabPane.currentPayload ? payloadsTabPane.currentPayload.id === modelData.id : false

                            color: current ? "#4682b4" : "transparent"
                            border.width: 0
                            width: payloadsListCard.availableWidth
                            height: 36
                            radius: height / 2

                            Text {
                                text: modelData.name
                                anchors.fill: parent
                                leftPadding: 12
                                rightPadding: 12
                                clip: true
                                elide: Text.ElideRight
                                font.pixelSize: 14
                                horizontalAlignment: Text.AlignLeft
                                verticalAlignment: Text.AlignVCenter
                                color: parent.current ? "#ffffff" : "#000000"
                            }

                            MouseArea {
                                anchors.fill: parent

                                onClicked: {
                                    backend.selectPayload(modelData.id);
                                }
                            }
                        }
                    }

                    ListView {
                        anchors.left: parent.left
                        anchors.top: addPayloadButton.bottom
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom
                        anchors.topMargin: 12

                        clip: true
                        spacing: 12

                        model: payloadsTabPane.payloads

                        delegate: payloadListItem
                    }
                }

                Card {
                    id: payloadParamsCard
                    Layout.preferredWidth: width
                    Layout.fillHeight: true
                    width: 360
                    height: parent.height

                    Column {
                        spacing: 12
                        anchors.left: parent.left
                        anchors.right: parent.right

                        RowLayout {
                            spacing: 12
                            anchors.left: parent.left
                            anchors.right: parent.right

                            PushButton {
                                Layout.preferredHeight: 36
                                Layout.preferredWidth: 36
                                iconSource: "qrc:///img/editor/rename"
                                type: PushButton.Primary
                                display: AbstractButton.IconOnly
                                shape: PushButton.Circle
                                onClicked: {
                                    const defaultText = payloadsTabPane.currentPayload ? payloadsTabPane.currentPayload.name : "";
                                    const names = window.currentSetup ? window.currentSetup.payloads.map(p => p.name) : [];
                                    keyboard.renamePayload(defaultText, names);
                                }
                            }

                            Text {
                                Layout.fillWidth: true
                                text: payloadsTabPane.currentPayload ? payloadsTabPane.currentPayload.name : "Payload"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                                elide: Text.ElideRight
                            }
                        }

                        InputNumber {
                            label: "Mass:"
                            value: payloadsTabPane.mass
                            suffix: " kg"
                            precision: 2
                            labelToLeft: true
                            rightPadding: 12
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Left
                            onSave: {
                                backend.editPayload("mass", result);
                            }
                        }

                        InputNumber {
                            label: "COM X:"
                            value: payloadsTabPane.comX
                            suffix: " mm"
                            precision: 2
                            labelToLeft: true
                            rightPadding: 12
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Left
                            onSave: {
                                backend.editPayload("comX", result / 1000);
                            }
                        }

                        InputNumber {
                            label: "COM Y:"
                            value: payloadsTabPane.comY
                            suffix: " mm"
                            precision: 2
                            labelToLeft: true
                            rightPadding: 12
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Left
                            onSave: {
                                backend.editPayload("comY", result / 1000);
                            }
                        }

                        InputNumber {
                            label: "COM Z:"
                            value: payloadsTabPane.comZ
                            suffix: " mm"
                            precision: 2
                            labelToLeft: true
                            rightPadding: 12
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Left
                            onSave: {
                                backend.editPayload("comZ", result / 1000);
                            }
                        }
                    }

                    Column {
                        spacing: 12
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom

                        PushButton {
                            enabled: payload && payload.id !== payloadsTabPane.currentPayload.id
                            text: "Set Payload Now"
                            type: PushButton.Primary
                            anchors.left: parent.left
                            anchors.right: parent.right

                            onClicked: {
                                backend.setPayload(payloadsTabPane.currentPayload.id);
                            }
                        }

                        PushButton {
                            enabled: payloadsTabPane.payloads.length > 1
                            text: "Delete Payload"
                            type: PushButton.Danger
                            anchors.left: parent.left
                            anchors.right: parent.right

                            onClicked: confirmDeletePayloadModal.open()

                            ConfirmModal {
                                id: confirmDeletePayloadModal
                                title: "Delete Payload"
                                description: "Do you really want to permanently delete this payload? It may be referenced in different programs."
                                okText: "Confirm"
                                okButtonType: PushButton.Danger
                                cancelButtonType: PushButton.Default

                                onOk: {
                                    backend.deletePayload();
                                    confirmDeletePayloadModal.close();
                                }
                                onCancel: {
                                    confirmDeletePayloadModal.close();
                                }
                            }
                        }
                    }
                }

                Card {
                    id: payloadEmulatorCard
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    height: parent.height
                }
            }
        }

        TabPane {
            id: framesTabPane
            color: "transparent"
            padding: 0
        }

        TabPane {
            id: startupTabPane
            color: "transparent"
            padding: 0

            property bool open: window.currentSetup ? window.currentSetup.startup.open : false
            property string programName: window.currentSetup ? window.currentSetup.startup.programName : ""
            property bool releaseBrakes: window.currentSetup ? window.currentSetup.startup.releaseBrakes : false
            property bool start: window.currentSetup ? window.currentSetup.startup.start : false

            Card {
                anchors.fill: parent

                Column {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    spacing: 12

                    RowLayout {
                        spacing: 12
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: 36

                        CheckButton {
                            Layout.fillHeight: true
                            Layout.preferredHeight: height
                            text: "Open program on startup"
                            active: startupTabPane.open
                            onClicked: {
                                backend.changeStartupParameter("open", !active);
                            }
                        }

                        Select {
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            placeholder: "Select program..."
                            enabled: startupTabPane.open
                            options: window.programsList || []
                            currentInd: window.programsList ? window.programsList.findIndex(p => p.name === startupTabPane.programName) : -1

                            onActivated: {
                                const programName = window.programsList[index].name;
                                backend.changeStartupParameter("programName", programName);
                            }
                        }
                    }

                    Alert {
                        showIcon: true
                        type: Alert.Warning
                        width: parent.width
                        message: "Important warning"
                        description: "If you select option below robot can start moving on startup. It can be dangerous. Select option below only if you are completely sure what are you doing!"
                    }

                    CheckButton {
                        text: "Release brakes on startup"
                        active: startupTabPane.releaseBrakes
                        onClicked: {
                            backend.changeStartupParameter("releaseBrakes", !active);
                        }
                    }

                    Alert {
                        showIcon: true
                        type: Alert.Warning
                        width: parent.width
                        message: "Important warning"
                        description: "If you select option below robot will start running program on startup. It can be dangerous. Select option below only if you are completely sure what are you doing!"
                    }

                    CheckButton {
                        enabled: startupTabPane.releaseBrakes && startupTabPane.open
                        text: "Start program on startup"
                        active: startupTabPane.start
                        onClicked: {
                            backend.changeStartupParameter("start", !active);
                        }
                    }
                }
            }
        }
    }
}
