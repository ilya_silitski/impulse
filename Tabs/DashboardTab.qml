import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import "../Components"
import "../Elements"

TabPane {
    id: dashboardTab

    Column {
        anchors.fill: parent
        spacing: 12

        RowLayout {
            spacing: 12
            height: (parent.height - parent.spacing) / 2
            width: parent.width

            Card {
                id: robotInfo

                Layout.fillWidth: true
                Layout.fillHeight: true

                property real voltage: window.realTimeData ? window.realTimeData.voltage || 0.0 : 0.0
                property real current: window.realTimeData ? window.realTimeData.current || 0.0 : 0.0
                property real power: voltage && current ? Math.abs(voltage * current) : 0.0

                Column {
                    anchors.fill: parent
                    spacing: 12

                    Text {
                        text: "Robot"
                        height: 36
                        width: parent.width
                        font.pixelSize: 18
                        font.weight: Font.DemiBold
                        color: "#555555"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }

                    Row {
                        spacing: 12

                        Text {
                            text: "Voltage"
                            height: 36
                            width: 60
                            font.pixelSize: 14
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignRight
                        }

                        Progress {
                            width: 180
                            anchors.verticalCenter: parent.verticalCenter
                            from: 12
                            to: 60
                            value: robotInfo.voltage
                            prefix: "12 V"
                            suffix: "60 V"
                        }

                        Text {
                            text: parseFloat(robotInfo.voltage.toFixed(1)) + " V"
                            width: 84
                            height: 36
                            font.pixelSize: 14
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignLeft
                        }
                    }

                    Row {
                        spacing: 12

                        Text {
                            text: "Current"
                            height: 36
                            width: 60
                            font.pixelSize: 14
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignRight
                        }

                        Progress {
                            width: 180
                            anchors.verticalCenter: parent.verticalCenter
                            from: 0
                            to: 20
                            value: robotInfo.current
                            prefix: "0 A"
                            suffix: "20 A"
                        }

                        Text {
                            text: parseFloat(robotInfo.current.toFixed(1)) + " A"
                            width: 84
                            height: 36
                            font.pixelSize: 14
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignLeft
                        }
                    }

                    Row {
                        spacing: 12

                        Text {
                            text: "Power"
                            height: 36
                            width: 60
                            font.pixelSize: 14
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignRight
                        }

                        Progress {
                            width: 180
                            anchors.verticalCenter: parent.verticalCenter
                            from: 0
                            to: 1000
                            value: robotInfo.power
                            prefix: "0 W"
                            suffix: "1000 W"
                        }

                        Text {
                            text: parseFloat(robotInfo.power.toFixed(0)) + " W"
                            width: 84
                            height: 36
                            font.pixelSize: 14
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignLeft
                        }
                    }
                }
            }

            GridLayout {
                Layout.fillWidth: true
                Layout.fillHeight: true
                columns: 3
                rows: 2
                columnSpacing: 12
                rowSpacing: 12

                Component {
                    id: motorInfoItem

                    Card {
                        id: motorInfo
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        property real motorTemp: window.realTimeData ? window.realTimeData.motorStatorTemp[index] || 0.0 : 0.0
                        property real controllerTemp: window.realTimeData ? window.realTimeData.motorControllerTemp[index] || 0.0 : 0.0

                        Column {
                            anchors.fill: parent
                            spacing: 12

                            Text {
                                text: "Motor " + (index + 1)
                                height: 24
                                width: parent.width
                                font.pixelSize: 14
                                font.weight: Font.DemiBold
                                color: "#555555"
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                            }

                            Row {
                                spacing: 12

                                Text {
                                    text: "Stator"
                                    height: 24
                                    width: 72
                                    font.pixelSize: 14
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignRight
                                }

                                Progress {
                                    width: 120
                                    anchors.verticalCenter: parent.verticalCenter
                                    from: 0
                                    to: 100
                                    value: motorInfo.motorTemp
                                    prefix: "0°C"
                                    suffix: "100°C"
                                }

                                Text {
                                    text: parseFloat(motorInfo.motorTemp.toFixed(2)) + "°C"
                                    width: 84
                                    height: 24
                                    font.pixelSize: 14
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignLeft
                                }
                            }

                            Row {
                                spacing: 12

                                Text {
                                    text: "Controller"
                                    height: 24
                                    width: 72
                                    font.pixelSize: 14
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignRight
                                }

                                Progress {
                                    width: 120
                                    anchors.verticalCenter: parent.verticalCenter
                                    from: 0
                                    to: 100
                                    value: motorInfo.controllerTemp
                                    prefix: "0°C"
                                    suffix: "100°C"
                                }

                                Text {
                                    text: parseFloat(motorInfo.controllerTemp.toFixed(2)) + "°C"
                                    width: 84
                                    height: 24
                                    font.pixelSize: 14
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignLeft
                                }
                            }
                        }
                    }
                }

                Repeater {
                    model: 6
                    delegate: motorInfoItem
                }
            }
        }

        Card {
            height: (parent.height - parent.spacing) / 2
            width: parent.width

            ScrollView {
                anchors.fill: parent

                TextArea {
                    readOnly: true
                    text: window.logsHistory ? window.logsHistory : ""
                    cursorPosition: window.logsHistory ? window.logsHistory.length : 0

                    background: Rectangle {
//                        anchors.fill: parent
                        border.color: "#e8e8e8"
                        color: "#f2f2f2"
                        radius: 12
                    }
                }
            }
        }
    }
}
