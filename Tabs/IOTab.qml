import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import "../Components"
import "../Elements"

TabPane {
    id: ioTab

    GridLayout {
        anchors.fill: parent
        columns: 2
        rows: 4
        columnSpacing: 12
        rowSpacing: 6

        Text {
            text: "Digital Inputs"
            Layout.preferredHeight: 18
            Layout.fillWidth: true
            font.pixelSize: 14
            font.weight: Font.DemiBold
            color: "#333333"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        Text {
            text: "Digital Outputs"
            Layout.preferredHeight: 18
            Layout.fillWidth: true
            font.pixelSize: 14
            font.weight: Font.DemiBold
            color: "#333333"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        Card {
            Layout.fillHeight: true
            Layout.fillWidth: true

            GridLayout {
                anchors.fill: parent
                columns: 3
                rows: 8
                columnSpacing: 12
                rowSpacing: 12
                flow: GridLayout.TopToBottom

                Component {
                    id: digitalInput

                    RowLayout {
                        property string name: window.currentSetup ? window.currentSetup.io.di[index].name : "[unknown]"
                        property bool active: window.realTimeData ? window.realTimeData.digitalInputs[index] === 1 : false

                        CheckButton {
                            Layout.fillHeight: true
                            Layout.preferredHeight: 30
                            padding: 4
                            text: ""
                            active: parent.active
                            enabled: false
                        }

                        Rectangle {
                            property string name: parent.name
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            border.width: 1
                            border.color: "#cccccc"
                            radius: 6

                            Text {
                                text: parent.name
                                anchors.fill: parent
                                anchors.leftMargin: 6
                                anchors.rightMargin: 6
                                font.pixelSize: 14
                                verticalAlignment: Text.AlignVCenter
                                elide: Text.ElideRight

                                MouseArea {
                                    anchors.fill: parent

                                    onClicked: {
                                        const names = window.currentSetup ? window.currentSetup.io.di.map(i => i.name) : [];
                                        keyboard.renameIO(parent.text, names, "renameDigitalInput", modelData);
                                    }
                                }
                            }
                        }
                    }
                }

                Repeater {
                    model: 24
                    delegate: digitalInput
                }
            }
        }

        Card {
            Layout.fillHeight: true
            Layout.fillWidth: true

            GridLayout {
                anchors.fill: parent
                columns: 3
                rows: 8
                columnSpacing: 12
                rowSpacing: 12
                flow: GridLayout.TopToBottom

                Component {
                    id: digitalOutput

                    RowLayout {
                        property string name: window.currentSetup ? window.currentSetup.io.do[index].name : "[unknown]"
                        property bool active: window.realTimeData ? window.realTimeData.digitalOutputs[index] === 1 : false

                        CheckButton {
                            enabled: !window.readOnlyMode
                            Layout.fillHeight: true
                            Layout.preferredHeight: 30
                            padding: 4
                            text: ""
                            active: parent.active
                            onClicked: backend.setDigitalOutput(index, !parent.active)
                        }

                        Rectangle {
                            property string name: parent.name
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            border.width: 1
                            border.color: "#cccccc"
                            radius: 6

                            Text {
                                text: parent.name
                                anchors.fill: parent
                                anchors.leftMargin: 6
                                anchors.rightMargin: 6
                                font.pixelSize: 14
                                verticalAlignment: Text.AlignVCenter
                                elide: Text.ElideRight

                                MouseArea {
                                    anchors.fill: parent

                                    onClicked: {
                                        const names = window.currentSetup ? window.currentSetup.io.do.map(o => o.name) : [];
                                        keyboard.renameIO(parent.text, names, "renameDigitalOutput", modelData);
                                    }
                                }
                            }
                        }
                    }
                }

                Repeater {
                    model: 24
                    delegate: digitalOutput
                }
            }
        }

        Text {
            text: "Analog Inputs"
            Layout.preferredHeight: 18
            Layout.fillWidth: true
            font.pixelSize: 14
            font.weight: Font.DemiBold
            color: "#333333"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        Text {
            text: "Analog Outputs"
            Layout.preferredHeight: 18
            Layout.fillWidth: true
            font.pixelSize: 14
            font.weight: Font.DemiBold
            color: "#333333"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        Card {
            Layout.fillHeight: true
            Layout.fillWidth: true

            Column {
                anchors.fill: parent
                spacing: 12

                Component {
                    id: analogInput

                    Row {
                        spacing: 12

                        property string name: window.currentSetup ? window.currentSetup.io.ai[index].name : "[unknown]"
                        property bool currentMode: window.realTimeData ? window.realTimeData.analogInputsMode[index] === 1 : false
                        property real value: window.realTimeData ? window.realTimeData.analogInputs[index] * (currentMode ? 1000 : 1) : 0.0

                        Text {
                            text: parent.name
                            height: 36
                            width: 120
                            font.pixelSize: 14
                            verticalAlignment: Text.AlignVCenter
                            elide: Text.ElideRight

                            MouseArea {
                                anchors.fill: parent

                                onClicked: {
                                    const names = window.currentSetup ? window.currentSetup.io.ai.map(i => i.name) : [];
                                    keyboard.renameIO(parent.text, names, "renameAnalogInput", modelData);
                                }
                            }
                        }

                        Progress {
                            width: 360
                            anchors.verticalCenter: parent.verticalCenter
                            from: 0
                            to: parent.currentMode ? 20 : 10
                            value: parent.value
                            prefix: parent.currentMode ? "0 mA" : "0 V"
                            suffix: parent.currentMode ? "20 mA" : "10 V"
                        }

                        Text {
                            text: parseFloat((parent.value).toFixed(2)) + (parent.currentMode ? " mA" : " V")
                            width: 96
                            height: 36
                            font.pixelSize: 14
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignLeft
                        }
                    }
                }

                Repeater {
                    model: 4
                    delegate: analogInput
                }
            }
        }

        Card {
            Layout.fillHeight: true
            Layout.fillWidth: true

            Column {
                anchors.fill: parent
                spacing: 12

                Component {
                    id: analogOutput

                    Row {
                        spacing: 12

                        property string name: window.currentSetup ? window.currentSetup.io.ao[index].name : "[unknown]"
                        property bool currentMode: window.realTimeData ? window.realTimeData.analogOutputsMode[index] === 1 : false
                        property real value: window.realTimeData ? window.realTimeData.analogOutputs[index] * (currentMode ? 1000 : 1) : 0.0

                        Text {
                            text: parent.name
                            height: 36
                            width: 120
                            font.pixelSize: 14
                            verticalAlignment: Text.AlignVCenter
                            elide: Text.ElideRight

                            MouseArea {
                                anchors.fill: parent

                                onClicked: {
                                    const names = window.currentSetup ? window.currentSetup.io.ao.map(o => o.name) : [];
                                    keyboard.renameIO(parent.text, names, "renameAnalogOutput", modelData);
                                }
                            }
                        }

                        Slide {
                            enabled: !window.readOnlyMode
                            width: 300
                            anchors.verticalCenter: parent.verticalCenter
                            from: 0
                            to: parent.currentMode ? 20 : 10
                            stepSize: 0.01
                            value: parent.value
                            prefix: parent.currentMode ? "0 mA" : "0 V"
                            suffix: parent.currentMode ? "20 mA" : "10 V"

                            onMoved: {
                                backend.setAnalogOutput(index, parent.currentMode, value);
                            }
                        }

                        InputNumber {
                            enabled: !window.readOnlyMode
                            width: 60
                            label: ""
                            value: parent.value
                            suffix: ""
                            precision: 2
                            labelWidth: 0
                            rightPadding: 12
                            leftPadding: 12
                            numpadPlacement: InputNumber.Left
                            onSave: {
                                backend.setAnalogOutput(index, parent.currentMode, result);
                            }
                        }

                        Select {
                            enabled: !window.readOnlyMode
                            width: 84
                            placeholder: "cM"
                            options: ["V", "mA"]
                            currentInd: parent.currentMode

                            onActivated: {
                                backend.setAnalogOutput(modelData, !!index, parent.value)
                            }
                        }
                    }
                }

                Repeater {
                    model: 4
                    delegate: analogOutput
                }
            }
        }
    }
}
