import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtWebEngine
import Qt5Compat.GraphicalEffects
import "../Components"
import "../Elements"
import "../Models"

TabPane {
    id: programTab

    property var currentCommand
    property bool bufferEmpty: true
    property var errors: []
    readonly property var errorsIds: errors.map(e => e.commandId)
    readonly property bool hasError: !!errors.length

    FileModal {
        id: fileDialog

        onSelected: {
            backend.openProgram(programName);
        }
    }

    Card {
        id: programToolBar
        height: 60
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: commandEditorCard.left
        anchors.rightMargin: 12

        Row {
            anchors.fill: parent
            spacing: 12

            PushButton {
                width: 96
                text: "New"

                onClicked: {
                    keyboard.newProgram();
                }
            }

            PushButton {
                width: 96
                text: "Open"

                onClicked: {
                    fileDialog.open();
                }
            }

            PushButton {
                enabled: !!window.currentProgram
                width: 96
                text: "Save"

                onClicked: {
                    backend.saveCurrentProgram();
                }
            }

            PushButton {
                enabled: false
                width: 96
                text: "Save as"

                onClicked: {
                    backend.saveCurrentProgramAs();
                }
            }

            ToolSeparator {}

            PushButton {
                width: 96
                text: "Undo"
                onClicked: {
                    const json = JSON.stringify([{a: 1}, {b: 2}, {c: 3}, {d: 4}, {e: 5}, {f: 6}]);
                    const command = "Code.count_vars('" + json + "')";
                    console.log(typeof json, json, command)
                    webView.runJavaScript(command, function(result) { console.log("Result:", result); })
                }
            }

            PushButton {
                enabled: false
                width: 96
                text: "Redo"
            }
        }
    }

    WebEngineView {
        id: webView
        anchors.left: parent.left
        anchors.top: programToolBar.bottom
        anchors.right: commandEditorCard.left
        anchors.bottom: parent.bottom
        anchors.topMargin: 12
        anchors.rightMargin: 12
        layer.enabled: true
        layer.effect: OpacityMask {
            maskSource: Item {
                width: webView.width
                height: webView.height
                Rectangle {
                    anchors.verticalCenter: parent.verticalCenter
                    width: parent.width
                    height: parent.height
                    radius: 12
                }
            }
        }

        onJavaScriptConsoleMessage: function(level, message, lineNumber, sourceID){
            if (level === WebEngineView.InfoMessageLevel) {
                try {
                    const msg = JSON.parse(message);
                    webView.runJavaScript("localStorage.getItem('last_state')", function(result) { console.log("Local storage:", result); });
                    console.log("Message from WebViewEngine: ", msg);
                } catch (error) {
                    console.error(error);
                }
            }
        }

        onJavaScriptDialogRequested: function(request) {
            console.log("defaultText", request.defaultText, "\nmessage", request.message, "\ntitle", request.title, "\ntype", request.type);
            console.log(webView.implicitHeight, webView.implicitWidth, webView.height, webView.width);

//            request.accepted = true;
        }

        url: "qrc:///blockly/index.html"
    }

    Card {
        id: commandEditorCard
        width: 540
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        property int currentTab: 0

        Row {
            id: commandEditorHeader
            spacing: 12

            ToggleButton {
                width: 120
                text: "Parameters"
                active: commandEditorCard.currentTab === 0

                onClicked: commandEditorCard.currentTab = 0
            }

            ToggleButton {
                width: 120
                text: "Points"
                active: commandEditorCard.currentTab === 1

                onClicked: commandEditorCard.currentTab = 1
            }

            ToggleButton {
                width: 120
                text: "Variables"
                active: commandEditorCard.currentTab === 2

                onClicked: commandEditorCard.currentTab = 2
            }
        }

        StackLayout {
            id: commandEditorLayout
            anchors.left: parent.left
            anchors.top: commandEditorHeader.bottom
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.topMargin: 12
            currentIndex: commandEditorCard.currentTab

            TabPane {
                id: paramsTab
                padding: 0
                color: "transparent"

                property int currentTab: {
                    if (!programTab.currentCommand)
                        return 0
                    const types = ["PROGRAM", "WAIT", "SET", "GRIPPER", "MOVE", "POINT", "COMMENT", "POPUP", "LOOP", "IF", "ELIF", "ELSE"]
                    return types.indexOf(programTab.currentCommand.type) + 1
                }

                StackLayout {
                    id: paramsLayout
                    anchors.fill: parent
                    currentIndex: paramsTab.currentTab

                    TabPane {
                        padding: 0
                        color: "transparent"

                        Text {
                            text: "No Command"
                            font.pixelSize: 24
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    TabPane {
                        id: programTabPane
                        padding: 0
                        color: "transparent"

                        property bool endless: programTab.currentCommand ? programTab.currentCommand.endless ?? false : false
                        property int repeatCount: programTab.currentCommand ? programTab.currentCommand.repeatCount ?? 1 : 1

                        Column {
                            spacing: 12
                            anchors.fill: parent

                            Text {
                                text: "Program"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                            }

                            CheckButton {
                                text: "Repeat program endlessly"
                                active: programTabPane.endless
                                onClicked: {
                                    backend.changeCommandParameter("endless", !active);
                                }
                            }

                            InputNumber {
                                label: "Repeat count:"
                                value: programTabPane.repeatCount
                                precision: 0
                                labelWidth: 96
                                anchors.left: parent.left
                                anchors.right: parent.right
                                rightPadding: 12
                                numpadPlacement: InputNumber.Left
                                enabled: !programTabPane.endless
                                onSave: {
                                    backend.changeCommandParameter("repeatCount", Math.trunc(result));
                                }
                            }
                        }
                    }

                    TabPane {
                        id: waitTabPane
                        padding: 0
                        color: "transparent"

                        property string option: programTab.currentCommand ? programTab.currentCommand.option || "" : ""
                        property bool time: option === "time"
                        property bool digital: option === "digital"
                        property bool analog: option === "analog"

                        Column {
                            spacing: 12
                            anchors.fill: parent

                            Text {
                                text: "Wait"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                            }

                            Row {
                                id: waitForTimerRow
                                anchors.left: parent.left
                                anchors.right: parent.right
                                spacing: 12

                                property real time: waitTabPane.time ? parseFloat((programTab.currentCommand.time / 1000).toFixed(3)) : 0.0

                                InputNumber {
                                    enabled: waitTabPane.time
                                    value: waitForTimerRow.time
                                    precision: 3
                                    labelWidth: 0
                                    suffix: " s"
                                    rightPadding: 12
                                    width: 120
                                    numpadPlacement: InputNumber.Left
                                    onSave: {
                                        backend.changeCommandParameter("option", "time");
                                        backend.changeCommandParameter("time", result * 1000);
                                    }
                                }
                            }

                            Row {
                                id: waitForDigitalInputRow
                                anchors.left: parent.left
                                anchors.right: parent.right
                                spacing: 12

                                property var digitalInput: waitTabPane.digital ? programTab.currentCommand.digital : null
                                property int key: digitalInput ? digitalInput.index : -1
                                property bool value: digitalInput ? digitalInput.value : false

                                PushButton {
                                    enabled: waitTabPane.digital
                                    shape: PushButton.Circle
                                    type: PushButton.Primary
                                    iconSource: "qrc:///img/editor/rename"
                                    display: AbstractButton.IconOnly

                                    onClicked: {
                                        const defaultText = window.currentSetup && waitTabPane.digital ? window.currentSetup.io.di[waitForDigitalInputRow.key].name : "";
                                        const names = window.currentSetup ? window.currentSetup.io.di.map(i => i.name) : [];
                                        keyboard.renameIO(defaultText, names, "renameDigitalInput", waitForDigitalInputRow.key);
                                    }
                                }

                                Select {
                                    width: 228
                                    placeholder: "Digital input"
                                    options: if (window.currentSetup) window.currentSetup.io.di
                                    currentInd: waitForDigitalInputRow.key

                                    onActivated: {
                                        backend.changeCommandParameter("option", "digital");
                                        waitForDigitalInputRow.digitalInput.index = index;
                                        backend.changeCommandMapParameter("digital", waitForDigitalInputRow.digitalInput);
                                    }
                                }

                                Text {
                                    height: 36
                                    width: 12
                                    font.pixelSize: 24
                                    text: "="
                                    horizontalAlignment: Text.AlignHCenter
                                    verticalAlignment: Text.AlignVCenter
                                }

                                PushButton {
                                    text: "L"
                                    shape: PushButton.Circle
                                    enabled: waitTabPane.digital

                                    property bool active: waitForDigitalInputRow.value === false

                                    outline: active
                                    type: active ? PushButton.Primary : PushButton.Default

                                    onClicked: {
                                        waitForDigitalInputRow.digitalInput.value = false;
                                        backend.changeCommandMapParameter("digital", waitForDigitalInputRow.digitalInput);
                                    }
                                }

                                PushButton {
                                    text: "H"
                                    shape: PushButton.Circle
                                    enabled: waitTabPane.digital

                                    property bool active: waitForDigitalInputRow.value === true

                                    type: active ? PushButton.Primary : PushButton.Default

                                    onClicked: {
                                        waitForDigitalInputRow.digitalInput.value = true;
                                        backend.changeCommandMapParameter("digital", waitForDigitalInputRow.digitalInput);
                                    }
                                }

                                PushButton {
                                    enabled: false
//                                    enabled: waitTabPane.digital
                                    text: "Expression"
                                    width: 108
                                }
                            }

                            Row {
                                id: waitForAnalogInputRow
                                anchors.left: parent.left
                                anchors.right: parent.right
                                spacing: 12

                                property var analogInput: waitTabPane.analog ? programTab.currentCommand.analog : null
                                property int key: analogInput ? analogInput.index : -1
                                property bool currentMode: window.realTimeData && waitTabPane.analog ? window.realTimeData.analogInputsMode[waitForAnalogInputRow.key] === 1 : false
                                property bool moreThan: analogInput ? analogInput.moreThan : false
                                property real value: analogInput ? analogInput.value : 0.0

                                PushButton {
                                    enabled: waitTabPane.analog
                                    shape: PushButton.Circle
                                    type: PushButton.Primary
                                    iconSource: "qrc:///img/editor/rename"
                                    display: AbstractButton.IconOnly

                                    onClicked: {
                                        const defaultText = window.currentSetup && waitTabPane.analog ? window.currentSetup.io.ai[waitForAnalogInputRow.key].name : "";
                                        const names = window.currentSetup ? window.currentSetup.io.ai.map(i => i.name) : [];
                                        keyboard.renameIO(defaultText, names, "renameAnalogInput", waitForAnalogInputRow.key);
                                    }
                                }

                                Select {
                                    width: 180
                                    placeholder: "Analog input"
                                    options: if (window.currentSetup) window.currentSetup.io.ai
                                    currentInd: waitForAnalogInputRow.key

                                    onActivated: {
                                        backend.changeCommandParameter("option", "analog");
                                        waitForAnalogInputRow.analogInput.index = index;
                                        backend.changeCommandMapParameter("analog", waitForAnalogInputRow.analogInput);
                                    }
                                }

                                Select {
                                    enabled: waitTabPane.analog
                                    width: 60
                                    options: ["<", ">"]
                                    currentInd: waitForAnalogInputRow.moreThan ? 1 : 0

                                    onActivated: {
                                        waitForAnalogInputRow.analogInput.moreThan = !!index;
                                        backend.changeCommandMapParameter("analog", waitForAnalogInputRow.analogInput);
                                    }
                                }

                                InputNumber {
                                    enabled: waitTabPane.analog
                                    value: waitForAnalogInputRow.value
                                    suffix: waitForAnalogInputRow.currentMode ? " mA" : " V"
                                    precision: 2
                                    labelWidth: 0
                                    rightPadding: 12
                                    width: 84
                                    numpadPlacement: InputNumber.Left
                                    onSave: {
                                        waitForAnalogInputRow.analogInput.value = result;
                                        backend.changeCommandMapParameter("analog", waitForAnalogInputRow.analogInput);
                                    }
                                }

                                PushButton {
                                    enabled: false
//                                    enabled: waitTabPane.analog
                                    text: "Expression"
                                    width: 108
                                }
                            }
                        }
                    }

                    TabPane {
                        id: setTabPane
                        padding: 0
                        color: "transparent"

                        property var operations: programTab.currentCommand ? programTab.currentCommand.operations : null
                        property var operation: operations ? operations[0] : null
                        readonly property bool digital: operation ? operation.type === "digital" : false
                        readonly property bool analog: operation ? operation.type === "analog" : false
                        readonly property bool tool: operation ? operation.type === "tool" : false
                        readonly property bool payload: operation ? operation.type === "payload" : false

                        Column {
                            spacing: 12
                            anchors.fill: parent

                            Text {
                                text: "Set"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                            }

                            Row {
                                id: digitalOutputRow
                                anchors.left: parent.left
                                anchors.right: parent.right
                                spacing: 12

                                property int key: setTabPane.digital ? setTabPane.operation.key : -1
                                property bool value: setTabPane.digital ? setTabPane.operation.value : false

                                PushButton {
                                    enabled: setTabPane.digital
                                    shape: PushButton.Circle
                                    type: PushButton.Primary
                                    iconSource: "qrc:///img/editor/rename"
                                    display: AbstractButton.IconOnly

                                    onClicked: {
                                        const defaultText = window.currentSetup && setTabPane.digital ? window.currentSetup.io.do[digitalOutputRow.key].name : "";
                                        const names = window.currentSetup ? window.currentSetup.io.do.map(o => o.name) : [];
                                        keyboard.renameIO(defaultText, names, "renameDigitalOutput", digitalOutputRow.key);
                                    }
                                }

                                Select {
                                    width: 156
                                    placeholder: "Digital ouput"
                                    options: if (window.currentSetup) window.currentSetup.io.do
                                    currentInd: digitalOutputRow.key

                                    onActivated: {
                                        const operation = {
                                            type: "digital",
                                            key: index,
                                            value: false
                                        };
                                        backend.changeCommandListParameter("operations", [operation]);
                                    }
                                }

                                Text {
                                    height: 36
                                    width: 12
                                    font.pixelSize: 24
                                    text: "="
                                    horizontalAlignment: Text.AlignHCenter
                                    verticalAlignment: Text.AlignVCenter
                                }

                                PushButton {
                                    text: "L"
                                    shape: PushButton.Circle
                                    enabled: setTabPane.digital

                                    property bool active: digitalOutputRow.value === false

                                    outline: active
                                    type: active ? PushButton.Primary : PushButton.Default

                                    onClicked: {
                                        setTabPane.operation.value = false;
                                        backend.changeCommandListParameter("operations", [setTabPane.operation]);
                                    }
                                }

                                PushButton {
                                    text: "H"
                                    shape: PushButton.Circle
                                    enabled: setTabPane.digital

                                    property bool active: digitalOutputRow.value === true

                                    type: active ? PushButton.Primary : PushButton.Default

                                    onClicked: {
                                        setTabPane.operation.value = true;
                                        backend.changeCommandListParameter("operations", [setTabPane.operation]);
                                    }
                                }

                                PushButton {
                                    enabled: false
//                                    enabled: setTabPane.digital
                                    text: "Pulse"
                                    width: 60
                                }

                                PushButton {
                                    enabled: false
//                                    enabled: setTabPane.digital
                                    text: "Expression"
                                    width: 108
                                }
                            }

                            Row {
                                id: analogOutputRow
                                anchors.left: parent.left
                                anchors.right: parent.right
                                spacing: 12

                                property int key: setTabPane.analog ? setTabPane.operation.key : -1
                                property bool currentMode: window.realTimeData && setTabPane.analog ? window.realTimeData.analogOutputsMode[analogOutputRow.key] === 1 : false
                                property real value: setTabPane.analog ? setTabPane.operation.value : 0.0

                                PushButton {
                                    enabled: setTabPane.analog
                                    shape: PushButton.Circle
                                    type: PushButton.Primary
                                    iconSource: "qrc:///img/editor/rename"
                                    display: AbstractButton.IconOnly

                                    onClicked: {
                                        const defaultText = window.currentSetup && setTabPane.analog ? window.currentSetup.io.ao[analogOutputRow.key].name : "";
                                        const names = window.currentSetup ? window.currentSetup.io.ao.map(o => o.name) : [];
                                        keyboard.renameIO(defaultText, names, "renameAnalogOutput", analogOutputRow.key);
                                    }
                                }

                                Select {
                                    width: 156
                                    placeholder: "Analog ouput"
                                    options: if (window.currentSetup) window.currentSetup.io.ao
                                    currentInd: analogOutputRow.key

                                    onActivated: {
                                        const operation = {
                                            type: "analog",
                                            key: index,
                                            value: 0.0
                                        };
                                        backend.changeCommandListParameter("operations", [operation]);
                                    }
                                }

                                Text {
                                    height: 36
                                    width: 12
                                    font.pixelSize: 24
                                    text: "="
                                    horizontalAlignment: Text.AlignHCenter
                                    verticalAlignment: Text.AlignVCenter
                                }

                                InputNumber {
                                    enabled: setTabPane.analog
                                    value: analogOutputRow.value
                                    suffix: analogOutputRow.currentMode ? " mA" : " V"
                                    precision: 2
                                    labelWidth: 0
                                    rightPadding: 12
                                    width: 156
                                    numpadPlacement: InputNumber.Left
                                    onSave: {
                                        setTabPane.operation.value = result;
                                        backend.changeCommandListParameter("operations", setTabPane.operations);
                                    }
                                }

                                PushButton {
                                    enabled: false
//                                    enabled: setTabPane.analog
                                    text: "Expression"
                                    width: 108
                                }
                            }

                            Row {
                                id: toolRow
                                anchors.left: parent.left
                                anchors.right: parent.right
                                spacing: 12

                                property string toolId: setTabPane.tool ? setTabPane.operation.toolId : ""
                                property int toolInd: window.currentSetup && setTabPane.tool ? window.currentSetup.tools.findIndex(t => t.id === toolRow.toolId) : -1
                                property var tool: window.currentSetup && setTabPane.tool && window.currentSetup.tools.find(t => t.id === toolRow.toolId)

                                PushButton {
                                    enabled: toolRow.toolId
                                    shape: PushButton.Circle
                                    type: PushButton.Primary
                                    iconSource: "qrc:///img/editor/rename"
                                    display: AbstractButton.IconOnly

                                    onClicked: {
                                        const defaultText = toolRow.tool ? toolRow.tool.name : "";
                                        const names = window.currentSetup ? window.currentSetup.tools.map(t => t.name) : [];
                                        keyboard.renameTool(defaultText, names, toolRow.toolId);
                                    }
                                }

                                Text {
                                    height: 36
                                    width: 156
                                    font.pixelSize: 18
                                    text: "Tool Center Point"
                                    horizontalAlignment: Text.AlignLeft
                                    verticalAlignment: Text.AlignVCenter
                                }

                                Text {
                                    height: 36
                                    width: 12
                                    font.pixelSize: 24
                                    text: "="
                                    horizontalAlignment: Text.AlignHCenter
                                    verticalAlignment: Text.AlignVCenter
                                }

                                Select {
                                    width: 276
                                    placeholder: "Tool Center Point"
                                    options: if (window.currentSetup) window.currentSetup.tools
                                    currentInd: toolRow.toolInd

                                    onActivated: {
                                        const operation = {
                                            type: "tool",
                                            toolId: window.currentSetup.tools[index].id
                                        };
                                        backend.changeCommandListParameter("operations", [operation]);
                                    }
                                }
                            }

                            Row {
                                id: payloadRow
                                anchors.left: parent.left
                                anchors.right: parent.right
                                spacing: 12

                                property string payloadId: setTabPane.payload ? setTabPane.operation.payloadId : ""
                                property int payloadInd: window.currentSetup && setTabPane.payload ? window.currentSetup.payloads.findIndex(t => t.id === payloadRow.payloadId) : -1
                                property var payload: window.currentSetup && setTabPane.payload && window.currentSetup.payloads.find(t => t.id === payloadRow.payloadId)

                                PushButton {
                                    enabled: payloadRow.payloadId
                                    shape: PushButton.Circle
                                    type: PushButton.Primary
                                    iconSource: "qrc:///img/editor/rename"
                                    display: AbstractButton.IconOnly

                                    onClicked: {
                                        const defaultText = payloadRow.payload ? payloadRow.payload.name : "";
                                        const names = window.currentSetup ? window.currentSetup.payloads.map(t => t.name) : [];
                                        keyboard.renamePayload(defaultText, names, payloadRow.payloadId);
                                    }
                                }

                                Text {
                                    height: 36
                                    width: 156
                                    font.pixelSize: 18
                                    text: "Payload"
                                    horizontalAlignment: Text.AlignLeft
                                    verticalAlignment: Text.AlignVCenter
                                }

                                Text {
                                    height: 36
                                    width: 12
                                    font.pixelSize: 24
                                    text: "="
                                    horizontalAlignment: Text.AlignHCenter
                                    verticalAlignment: Text.AlignVCenter
                                }

                                Select {
                                    width: 276
                                    placeholder: "Payload"
                                    options: if (window.currentSetup) window.currentSetup.payloads
                                    currentInd: payloadRow.payloadInd

                                    onActivated: {
                                        const operation = {
                                            type: "payload",
                                            payloadId: window.currentSetup.payloads[index].id
                                        };
                                        backend.changeCommandListParameter("operations", [operation]);
                                    }
                                }
                            }
                        }
                    }

                    TabPane {
                        id: gripperTabPane
                        padding: 0
                        color: "transparent"

                        property bool value: programTab.currentCommand ? !!programTab.currentCommand.value : false

                        Column {
                            spacing: 12
                            anchors.fill: parent

                            Text {
                                text: "Gripper"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                            }

                            Row {
                                spacing: 6
                                width: parent.width

                                ToggleButton {
                                    enabled: !window.readOnlyMode
                                    width: (parent.width - parent.spacing) / 2
                                    text: "Close Gripper"
                                    active: gripperTabPane.value === true

                                    onClicked: backend.changeCommandParameter("value", true);
                                }

                                ToggleButton {
                                    enabled: !window.readOnlyMode
                                    width: (parent.width - parent.spacing) / 2
                                    text: "Open Gripper"
                                    active: gripperTabPane.value === false

                                    onClicked: backend.changeCommandParameter("value", false);
                                }
                            }


                        }
                    }

                    TabPane {
                        id: moveTabPane
                        padding: 0
                        color: "transparent"

                        property string motion: programTab.currentCommand ? programTab.currentCommand.motion || "" : ""
                        property real maxTransVel: programTab.currentCommand ? programTab.currentCommand.maxTransVel || 0 : 0
                        property real maxTransAcc: programTab.currentCommand ? programTab.currentCommand.maxTransAcc || 0 : 0
                        property real maxRotVel: programTab.currentCommand ? programTab.currentCommand.maxRotVel || 0 : 0
                        property real maxRotAcc: programTab.currentCommand ? programTab.currentCommand.maxRotAcc || 0 : 0
                        property real maxJointVel: programTab.currentCommand ? programTab.currentCommand.maxJointVel || 0 : 0
                        property real maxJointAcc: programTab.currentCommand ? programTab.currentCommand.maxJointAcc || 0 : 0
                        property bool autoBlend: programTab.currentCommand ? programTab.currentCommand.autoBlend || false : false
                        property real blend: programTab.currentCommand ? programTab.currentCommand.blend || 0 : 0

                        Column {
                            spacing: 12
                            anchors.fill: parent

                            Text {
                                text: "Move"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                            }

                            Select {
                                readonly property var motionTypes: ["joint", "linear"]

                                anchors.left: parent.left
                                anchors.right: parent.right
                                placeholder: "Select motion type"
                                options: ["Joint", "Linear"]
                                currentInd: motionTypes.indexOf(moveTabPane.motion)

                                onActivated: {
                                    backend.changeCommandParameter("motion", motionTypes[index]);
                                }
                            }

                            Loader {
                                anchors.left: parent.left
                                anchors.right: parent.right

                                Component {
                                    id: jointParams

                                    Column {
                                        anchors.left: parent.left
                                        anchors.right: parent.right
                                        spacing: 12

                                        InputNumber {
                                            label: "Max velocity:"
                                            value: moveTabPane.maxJointVel * 180 / Math.PI
                                            suffix: "°/s"
                                            precision: 3
                                            labelToLeft: true
                                            rightPadding: 12
                                            anchors.left: parent.left
                                            anchors.right: parent.right
                                            numpadPlacement: InputNumber.Left
                                            onSave: {
                                                backend.changeCommandParameter("maxJointVel", result * Math.PI / 180);
                                            }
                                        }

                                        InputNumber {
                                            label: "Max acceleration:"
                                            value: moveTabPane.maxJointAcc * 180 / Math.PI
                                            suffix: "°/s^2"
                                            precision: 3
                                            labelToLeft: true
                                            rightPadding: 12
                                            anchors.left: parent.left
                                            anchors.right: parent.right
                                            numpadPlacement: InputNumber.Left
                                            onSave: {
                                                backend.changeCommandParameter("maxJointAcc", result * Math.PI / 180);
                                            }
                                        }

                                    }

                                }

                                Component {
                                    id: linearParams

                                    Row {
                                        spacing: 12
                                        anchors.left: parent.left
                                        anchors.right: parent.right

                                        Column {
                                            spacing: 12
                                            width: (parent.width - parent.spacing) / 2

                                            Text {
                                                text: "Translation"
                                                height: 18
                                                font.weight: Font.DemiBold
                                                color: "#a6000000"
                                                horizontalAlignment: Text.AlignHCenter
                                                verticalAlignment: Text.AlignVCenter
                                                anchors.left: parent.left
                                                anchors.right: parent.right
                                            }

                                            InputNumber {
                                                label: "Max vel.:"
                                                value: moveTabPane.maxTransVel
                                                suffix: " m/s"
                                                precision: 3
                                                labelToLeft: true
                                                rightPadding: 12
                                                anchors.left: parent.left
                                                anchors.right: parent.right
                                                numpadPlacement: InputNumber.Left
                                                onSave: {
                                                    backend.changeCommandParameter("maxTransVel", result);
                                                }
                                            }

                                            InputNumber {
                                                label: "Max accel.:"
                                                value: moveTabPane.maxTransAcc
                                                suffix: " m/s^2"
                                                precision: 3
                                                labelToLeft: true
                                                rightPadding: 12
                                                anchors.left: parent.left
                                                anchors.right: parent.right
                                                numpadPlacement: InputNumber.Left
                                                onSave: {
                                                    backend.changeCommandParameter("maxTransAcc", result);
                                                }
                                            }

                                        }

                                        Column {
                                            spacing: 12
                                            width: (parent.width - parent.spacing) / 2

                                            Text {
                                                text: "Rotation"
                                                height: 18
                                                font.weight: Font.DemiBold
                                                color: "#a6000000"
                                                horizontalAlignment: Text.AlignHCenter
                                                verticalAlignment: Text.AlignVCenter
                                                anchors.left: parent.left
                                                anchors.right: parent.right
                                            }

                                            InputNumber {
                                                label: "Max vel.:"
                                                value: moveTabPane.maxRotVel * 180 / Math.PI
                                                suffix: "°/s"
                                                precision: 3
                                                labelToLeft: true
                                                rightPadding: 12
                                                anchors.left: parent.left
                                                anchors.right: parent.right
                                                numpadPlacement: InputNumber.Left
                                                onSave: {
                                                    backend.changeCommandParameter("maxRotVel", result * Math.PI / 180);
                                                }
                                            }

                                            InputNumber {
                                                label: "Max accel.:"
                                                value: moveTabPane.maxRotAcc * 180 / Math.PI
                                                suffix: "°/s^2"
                                                precision: 3
                                                labelToLeft: true
                                                rightPadding: 12
                                                anchors.left: parent.left
                                                anchors.right: parent.right
                                                numpadPlacement: InputNumber.Left
                                                onSave: {
                                                    backend.changeCommandParameter("maxRotAcc", result * Math.PI / 180);
                                                }
                                            }

                                        }

                                    }

                                }

                                sourceComponent: {
                                    switch (moveTabPane.motion) {
                                    case "joint": return jointParams;
                                    case "linear": return linearParams;
                                    default: return jointParams;
                                    }
                                }
                            }

                            RowLayout {
                                spacing: 12
                                anchors.left: parent.left
                                anchors.right: parent.right

                                CheckButton {
                                    Layout.preferredHeight: 36
                                    text: "Auto blend"
                                    active: moveTabPane.autoBlend
                                    onClicked: {
                                        backend.changeCommandParameter("autoBlend", !active);
                                    }
                                }

                                InputNumber {
                                    Layout.fillWidth: true
                                    Layout.preferredHeight: 36
                                    enabled: !moveTabPane.autoBlend
                                    label: "Blend:"
                                    value: moveTabPane.blend * 1000
                                    suffix: " mm"
                                    precision: 3
                                    labelToLeft: true
                                    rightPadding: 12
                                    numpadPlacement: InputNumber.Left
                                    onSave: {
                                        backend.changeCommandParameter("blend", result / 1000);
                                    }
                                }
                            }
                        }
                    }

                    TabPane {
                        id: pointTabPane
                        padding: 0
                        color: "transparent"

                        property string pointId: programTab.currentCommand ? programTab.currentCommand.pointId || "" : ""
                        property string mode: ""
                        readonly property var referencePoints: window.currentProgram ? window.currentProgram.points : []
                        property var point: pointId ? referencePoints.find(point => point.id === pointId) : null

                        function findParent(data, key, parent) {
                            let result = null;
                            for (let index = 0; index < data.length && result === null; index++) {
                                if (data[index].id === key) {
                                    return parent;
                                }
                                if (data[index].subCommands) {
                                    result = findParent(data[index].subCommands, key, data[index]);
                                }
                            }
                            return result;
                        }

                        Column {
                            spacing: 12
                            anchors.fill: parent

                            Row {
                                spacing: 12

                                Component {
                                    id: renamePointButton

                                    PushButton {
                                        iconSource: "qrc:///img/editor/rename"
                                        type: PushButton.Primary
                                        display: AbstractButton.IconOnly
                                        shape: PushButton.Circle
                                        onClicked: {
                                            const defaultText = pointTabPane.point ? pointTabPane.point.name : "";
                                            const names = window.currentProgram ? window.currentProgram.points.map(p => p.name) : [];
                                            keyboard.renamePoint(defaultText, names, pointTabPane.point.id);
                                        }
                                    }
                                }

                                Loader {
                                    sourceComponent: if (!!pointTabPane.point) renamePointButton
                                }

                                Text {
                                    text: pointTabPane.point ? pointTabPane.point.name : "Point"
                                    font.pixelSize: 24
                                    height: 36
                                    verticalAlignment: Text.AlignVCenter
                                }
                            }

                            Select {
                                anchors.left: parent.left
                                anchors.right: parent.right
                                placeholder: "Select reference point..."
                                options: pointTabPane.referencePoints
                                currentInd: pointTabPane.referencePoints.findIndex(point => point.id === pointTabPane.pointId)

                                onActivated: {
                                    backend.changeCommandParameter("pointId", pointTabPane.referencePoints[index].id);
                                }
                            }

                            Loader {
                                anchors.left: parent.left
                                anchors.right: parent.right

                                Component {
                                    id: hasPoint

                                    Column {
                                        anchors.left: parent.left
                                        anchors.right: parent.right
                                        spacing: 12

                                        Row {
                                            anchors.left: parent.left
                                            anchors.right: parent.right
                                            spacing: 12

                                            PushButton {
                                                enabled: !window.readOnlyMode && window.robotRunning
                                                type: PushButton.Primary
                                                width: (parent.width - parent.spacing) / 2
                                                text: "Auto Move"

                                                onPressed: {
                                                    const parent = pointTabPane.findParent(window.currentProgram.body, programTab.currentCommand.id);
                                                    if (parent === null) return;
                                                    if (parent.motion === "linear") {
                                                        if (arrivedToPoint(prettyPosition(pointTabPane.point.position), motionTab.currentPosition)) {
                                                            messageModal.title = "Already at point";
                                                            messageModal.description = "Robot is already at this point.";
                                                            messageModal.type = MessageModal.Info;
                                                            messageModal.okButtonType = PushButton.Primary;
                                                            messageModal.okText = "Continue";
                                                            messageModal.open();
                                                        } else
                                                            backend.goToPosition(prettyPosition(pointTabPane.point.position));
                                                    } else if (parent.motion === "joint") {
                                                        if (arrivedToPoint(prettyPose(pointTabPane.point.pose), motionTab.currentPose)) {
                                                            messageModal.title = "Already at point";
                                                            messageModal.description = "Robot is already at this point.";
                                                            messageModal.type = MessageModal.Info;
                                                            messageModal.okButtonType = PushButton.Primary;
                                                            messageModal.okText = "Continue";
                                                            messageModal.open();
                                                        } else
                                                            backend.goToPose(prettyPose(pointTabPane.point.pose));
                                                    }
                                                }
                                                onReleased: {
                                                    backend.stop();
                                                }
                                                onCanceled: {
                                                    backend.stop();
                                                }
                                                onHoveredChanged: {
                                                    if (!hovered && down) backend.stop();
                                                }
                                            }

                                            PushButton {
                                                width: (parent.width - parent.spacing) / 2
                                                text: "Manual Move"

                                                onClicked: {
                                                    const parent = pointTabPane.findParent(window.currentProgram.body, programTab.currentCommand.id);
                                                    if (parent.motion === "linear") {
                                                        motionTab.moveToPoint("position", prettyPosition(pointTabPane.point.position), pointTabPane.point.name);
                                                    } else if (parent.motion === "joint") {
                                                        motionTab.moveToPoint("pose", prettyPose(pointTabPane.point.pose), pointTabPane.point.name)
                                                    }
                                                }
                                            }
                                        }

                                        PushButton {
                                            text: "Edit Point"
                                            width: parent.width

                                            onClicked: motionTab.editPoint()
                                        }

                                    }

                                }

                                Component {
                                    id: noPoint

                                    PushButton {
                                        type: PushButton.Primary
                                        text: "Teach point"
                                        width: parent.width

                                        onClicked: motionTab.createPoint()
                                    }

                                }

                                sourceComponent: pointTabPane.pointId && pointTabPane.point ? hasPoint : noPoint
                            }
                        }
                    }

                    TabPane {
                        id: commentTabPane
                        padding: 0
                        color: "transparent"

                        property string comment: programTab.currentCommand ? programTab.currentCommand.comment || "" : ""

                        Column {
                            spacing: 12
                            anchors.fill: parent

                            Text {
                                text: "Comment"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                            }

                            TextInput {
                                placeholderText: "Enter comment..."
                                anchors.left: parent.left
                                anchors.right: parent.right
                                text: commentTabPane.comment

                                onClicked: {
                                    keyboard.comment(commentTabPane.comment);
                                }
                            }
                        }
                    }

                    TabPane {
                        id: popupTabPane
                        padding: 0
                        color: "transparent"

                        property int popupType: programTab.currentCommand ? programTab.currentCommand.popupType || 0 : 0
                        property string message: programTab.currentCommand ? programTab.currentCommand.message || "" : ""
                        property string buttonText: programTab.currentCommand ? programTab.currentCommand.buttonText || "" : ""
                        property bool playSound: programTab.currentCommand ? programTab.currentCommand.playSound || false : false
                        property string soundName: programTab.currentCommand ? programTab.currentCommand.soundName || "" : ""
                        readonly property var sounds: [
                            {name: "Sound 1", value: "beep_1"},
                            {name: "Sound 2", value: "beep_2"},
                            {name: "Sound 3", value: "beep_3"},
                            {name: "Sound 4", value: "beep_4"},
                            {name: "Sound 5", value: "beep_5"}
                        ]

                        Column {
                            spacing: 12
                            anchors.fill: parent

                            Text {
                                text: "Popup"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                            }

                            Row {
                                anchors.left: parent.left
                                anchors.right: parent.right
                                spacing: 12

                                Text {
                                    height: 36
                                    width: 84
                                    text: "Type"
                                    color: "#555555"
                                    font.pixelSize: 14
                                    horizontalAlignment: Text.AlignLeft
                                    verticalAlignment: Text.AlignVCenter
                                }

                                Select {
                                    placeholder: "Enter message..."
                                    width: 120
                                    currentInd: popupTabPane.popupType
                                    options: ["Info", "Warning", "Error"]

                                    onActivated: {
                                        backend.changeCommandParameter("popupType", index)
                                    }
                                }
                            }

                            RowLayout {
                                anchors.left: parent.left
                                anchors.right: parent.right
                                spacing: 12
                                height: 36

                                Text {
                                    Layout.preferredWidth: 84
                                    Layout.fillHeight: true
                                    text: "Message"
                                    color: "#555555"
                                    font.pixelSize: 14
                                    horizontalAlignment: Text.AlignLeft
                                    verticalAlignment: Text.AlignVCenter
                                }

                                TextInput {
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true
                                    placeholderText: "Enter message..."
                                    text: popupTabPane.message

                                    onClicked: {
                                        keyboard.message(popupTabPane.message);
                                    }
                                }
                            }

                            RowLayout {
                                anchors.left: parent.left
                                anchors.right: parent.right
                                spacing: 12
                                height: 36

                                Text {
                                    Layout.preferredWidth: 84
                                    Layout.fillHeight: true
                                    text: "Button Text"
                                    color: "#555555"
                                    font.pixelSize: 14
                                    horizontalAlignment: Text.AlignLeft
                                    verticalAlignment: Text.AlignVCenter
                                }

                                TextInput {
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true
                                    placeholderText: "Button Text"
                                    text: popupTabPane.buttonText

                                    onClicked: {
                                        keyboard.buttonText(popupTabPane.buttonText);
                                    }
                                }
                            }

                            RowLayout {
                                anchors.left: parent.left
                                anchors.right: parent.right
                                spacing: 12
                                height: 36

                                CheckButton {
                                    Layout.preferredWidth: 120
                                    Layout.fillHeight: true
                                    text: "Play sound"
                                    active: popupTabPane.playSound
                                    onClicked: {
                                        backend.changeCommandParameter("playSound", !active);
                                    }
                                }

                                Select {
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true
                                    enabled: popupTabPane.playSound
                                    options: popupTabPane.sounds
                                    currentInd: popupTabPane.sounds.findIndex(s => s.value === popupTabPane.soundName) || 0

                                    onActivated: {
                                        backend.changeCommandParameter("soundName", popupTabPane.sounds[index].value);
                                    }
                                }

                                PushButton {
                                    Layout.preferredWidth: 120
                                    Layout.fillHeight: true
                                    enabled: popupTabPane.playSound
                                    text: "Play now"
                                    type: PushButton.Primary

                                    onClicked: {
                                        const sound = popupTabPane.sounds.find(s => s.value === popupTabPane.soundName);
                                        sound ? backend.playSound(sound.value) : backend.playSound();
                                    }
                                }
                            }
                        }
                    }

                    TabPane {
                        id: loopTabPane
                        padding: 0
                        color: "transparent"

                        property string mode: programTab.currentCommand ? programTab.currentCommand.mode || "" : ""
                        property int count: programTab.currentCommand ? programTab.currentCommand.count || 0 : 0
                        property string expression: programTab.currentCommand ? programTab.currentCommand.expression || "" : ""

                        Column {
                            spacing: 12
                            anchors.fill: parent

                            Text {
                                text: "Loop"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                            }

                            Row {
                                spacing: 12

                                CheckButton {
                                    type: CheckButton.Radio
                                    text: "Loop endlessly"
                                    active: loopTabPane.mode === "endless"
                                    onClicked: {
                                        backend.changeCommandParameter("mode", "endless");
                                    }
                                }
                            }

                            Row {
                                spacing: 12

                                CheckButton {
                                    type: CheckButton.Radio
                                    text: "Loop"
                                    active: loopTabPane.mode === "count"
                                    onClicked: {
                                        backend.changeCommandParameter("mode", "count");
                                    }
                                }

                                InputNumber {
                                    label: ""
                                    value: loopTabPane.count
                                    precision: 0
                                    labelWidth: 0
                                    suffix: loopTabPane.count === 1 ? " time" : " times"
                                    width: 120
                                    rightPadding: 12
                                    numpadPlacement: InputNumber.Left
                                    onSave: {
                                        backend.changeCommandParameter("mode", "count");
                                        backend.changeCommandParameter("count", Math.trunc(result));
                                    }
                                }
                            }

                            Row {
                                spacing: 12

                                CheckButton {
                                    type: CheckButton.Radio
                                    text: "Loop while expression is true:"
                                    active: loopTabPane.mode === "expression"
                                    onClicked: {
                                        backend.changeCommandParameter("mode", "expression");
                                    }
                                }

                                TextInput {
                                    placeholderText: "Enter expression"
                                    text: loopTabPane.expression
                                    width: 240

                                    onClicked: {
                                        keyboard.expression(loopTabPane.expression);
                                    }
                                }
                            }
                        }
                    }

                    TabPane {
                        id: ifTabPane
                        padding: 0
                        color: "transparent"

                        property string expression: programTab.currentCommand ? programTab.currentCommand.expression || "" : ""

                        Column {
                            spacing: 12
                            anchors.fill: parent

                            Text {
                                text: "If"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                            }

                            TextInput {
                                placeholderText: "Enter expression"
                                text: ifTabPane.expression
                                width: parent.width

                                onClicked: {
                                    keyboard.expression(ifTabPane.expression);
                                }
                            }
                        }
                    }

                    TabPane {
                        id: elifTabPane
                        padding: 0
                        color: "transparent"

                        property string expression: programTab.currentCommand ? programTab.currentCommand.expression || "" : ""

                        Column {
                            spacing: 12
                            anchors.fill: parent

                            Text {
                                text: "Else If"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                            }

                            TextInput {
                                placeholderText: "Enter expression"
                                text: elifTabPane.expression
                                width: parent.width

                                onClicked: {
                                    keyboard.expression(elifTabPane.expression);
                                }
                            }
                        }
                    }

                    TabPane {
                        id: elseTabPane
                        padding: 0
                        color: "transparent"

                        property string expression: programTab.currentCommand ? programTab.currentCommand.expression || "" : ""

                        Column {
                            spacing: 12
                            anchors.fill: parent

                            Text {
                                text: "Else"
                                font.pixelSize: 24
                                height: 36
                                verticalAlignment: Text.AlignVCenter
                            }
                        }
                    }
                }
            }

            TabPane {
                id: pointsTab
                padding: 0
                color: "transparent"

                Component {
                    id: pointItem

                    RowLayout {
                        height: 48
                        width: pointsTab.width

                        PushButton {
                            shape: PushButton.Circle
                            type: PushButton.Primary
                            iconSource: "qrc:///img/editor/rename"
                            display: AbstractButton.IconOnly

                            Layout.margins: 6
                            Layout.preferredHeight: height
                            Layout.preferredWidth: width

                            onClicked: {
                                const defaultText = modelData.name;
                                const names = window.currentProgram ? window.currentProgram.points.map(p => p.name) : [];
                                keyboard.renamePoint(defaultText, names, modelData.id);
                            }
                        }

                        Text {
                            text: modelData.name
                            Layout.fillWidth: true
                            Layout.margins: 6
                            font.pixelSize: 18
                            horizontalAlignment: Text.AlignLeft
                            verticalAlignment: Text.AlignVCenter
                        }

                        PushButton {
                            shape: PushButton.Circle
                            type: PushButton.Danger
                            iconSource: "qrc:///img/editor/delete"
                            display: AbstractButton.IconOnly

                            Layout.margins: 6
                            Layout.preferredHeight: height
                            Layout.preferredWidth: width

                            ConfirmModal {
                                id: confirmDeletePointModal
                                title: "Delete point"
                                description: "Do you really want to permanently delete this point? It may be referenced in program by several commands."
                                okText: "Confirm"
                                cancelButtonType: PushButton.Default
                                okButtonType: PushButton.Danger
                                onOk: {
                                    backend.deletePoint(modelData.id);
                                    confirmDeletePointModal.close();
                                }
                                onCancel: {
                                    confirmDeletePointModal.close();
                                }
                            }

                            onClicked: confirmDeletePointModal.open();
                        }
                    }
                }

                ListView {
                    anchors.fill: parent
                    clip: true
                    model: window.currentProgram ? window.currentProgram.points : 0
                    delegate: pointItem
                }
            }

            TabPane {
                id: variablesTab
                padding: 0
                color: "transparent"
            }
        }
    }
}
