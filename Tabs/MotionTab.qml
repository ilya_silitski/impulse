import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import "../Elements"
import "../Components"
import "../"

TabPane {
    id: motionTab

    property int coordinateSystem: 0
    property var tool
    property alias zeroGravityOpen: zeroGravity.open
    property alias zeroGravityActive: zeroGravity.active

    enum Operation {
        None,
        MoveToPoint,
        CreatePoint,
        EditPoint
    }

    enum Mode {
        Move,
        MovePosition,
        MovePose,
        EditPosition,
        EditPose
    }

    property int operation: MotionTab.Operation.None
    property int initialTab

    readonly property bool hasOperation: operation !== MotionTab.Operation.None

    property int mode: MotionTab.Mode.Move

    readonly property bool normal: mode === MotionTab.Mode.Move

    readonly property bool movePosition: mode === MotionTab.Mode.MovePosition
    readonly property bool movePose: mode === MotionTab.Mode.MovePose
    readonly property bool moving: movePosition || movePose

    readonly property bool editPosition: mode === MotionTab.Mode.EditPosition
    readonly property bool editPose: mode === MotionTab.Mode.EditPose
    readonly property bool editing: editPosition || editPose

    readonly property bool activePosition: movePosition || editPosition
    readonly property bool activePose: movePose || editPose

    readonly property var targets: [
        {
            name: "Home Pose",
            pose: [0, -90, 0, -90, 0, 0]
        },
        {
            name: "Start Pose",
            pose: [180, -120, 120, -90, -90, 180]
        },
        {
            name: "Zero Pose",
            pose: [0, 0, 0, 0, 0, 0]
        }
    ]

    property var currentPosition: window.realTimeData ? realTimeData.position : [0, 0, 0, 0, 0, 0]
    property var targetPosition: [0, 0, 0, 0, 0, 0]
    property var currentPose: window.realTimeData ? realTimeData.pose : [0, 0, 0, 0, 0, 0]
    property var targetPose: [0, 0, 0, 0, 0, 0]
    property int ind: 0
    property string targetName: ""

    function updateRobotScene(position, pose) {
        rootEntity.setJoints(pose);

        rootEntity.setTCP(position);
    }

    function handleSave() {
        switch (operation) {
        case MotionTab.Operation.CreatePoint:
            resetOperation();
            backend.createPoint();
            window.goToTab(initialTab);
            break;
        case MotionTab.Operation.EditPoint:
            resetOperation();
            backend.editPoint();
            window.goToTab(initialTab);
            break;
        }
    }

    function handleCancel() {
        switch (operation) {
        case MotionTab.Operation.None:
            resetOperation();
            break;
        case MotionTab.Operation.MoveToPoint:
            resetOperation();
            window.goToTab(initialTab);
            break;
        case MotionTab.Operation.CreatePoint:
            if (editing) {
                resetOperation();
                operation = MotionTab.Operation.CreatePoint;
            } else {
                resetOperation();
                window.goToTab(initialTab);
            }
            break;
        case MotionTab.Operation.EditPoint:
            if (editing) {
                resetOperation();
                operation = MotionTab.Operation.EditPoint;
            } else {
                resetOperation();
                window.goToTab(initialTab);
            }
            break;
        }
    }

    function handleMove() {
        if (activePosition) {
            backend.goToPosition(targetPosition);
        } else if (activePose) {
            backend.goToPose(targetPose);
        }
    }

    function handleStop() {
        backend.stop();
        if (activePosition) {
            if (arrivedToPoint(targetPosition, currentPosition)) handleCancel();
        } else if (activePose) {
            if (arrivedToPoint(targetPose, currentPose)) handleCancel();
        }
    }

    function moveToPoint(type, target, name) {
        if (type === "position") {
            motionTab.targetPosition = target;
            motionTab.mode = MotionTab.Mode.MovePosition;
        } else if (type === "pose") {
            motionTab.targetPose = target;
            motionTab.mode = MotionTab.Mode.MovePose;
        }
        targetName = name;
        operation = MotionTab.Operation.MoveToPoint;
        initialTab = window.currentTab;
        window.goToTab(Main.Tab.Motion);
    }

    function createPoint() {
        operation = MotionTab.Operation.CreatePoint;
        initialTab = window.currentTab;
        window.goToTab(Main.Tab.Motion);
    }

    function editPoint() {
        operation = MotionTab.Operation.EditPoint;
        initialTab = window.currentTab;
        window.goToTab(Main.Tab.Motion);
    }

    function resetOperation() {
        targetPosition = [0, 0, 0, 0, 0, 0];
        targetPose = [0, 0, 0, 0, 0, 0];
        targetName = "";
        mode = MotionTab.Mode.Move;
        operation = MotionTab.Operation.None;
    }

    ColumnLayout {
        id: leftColumn
        spacing: 12
        width: 288
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        Card {
            id: poseCard
            font.pixelSize: 14

            Layout.fillWidth: true

            function toggleEditPose () {
                targetPose = currentPose;
                mode = MotionTab.Mode.EditPose;
            }

            function changeTarget(index, value) {
                targetPose = [...targetPose.slice(0, index), value, ...targetPose.slice(index + 1)];
            }

            StackLayout {
                id: poseStackLayout
                anchors.left: parent.left
                anchors.right: parent.right
                currentIndex: activePose

                Column {
                    spacing: 12

                    Text {
                        text: "Joint Pose"
                        height: 18
                        font.weight: Font.DemiBold
                        color: "#555555"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        anchors.left: parent.left
                        anchors.right: parent.right
                    }

                    Input {
                        label: "J1:"
                        value: currentPose[0]
                        suffix: "°"
                        anchors.left: parent.left
                        anchors.right: parent.right
                        editable: !moving
                        onClicked: () => {
                            poseCard.toggleEditPose();
                            j1Input.activate();
                        }
                    }

                    Input {
                        label: "J2:"
                        value: currentPose[1]
                        suffix: "°"
                        anchors.left: parent.left
                        anchors.right: parent.right
                        editable: !moving
                        onClicked: () => {
                            poseCard.toggleEditPose();
                            j2Input.activate();
                        }
                    }

                    Input {
                        label: "J3:"
                        value: currentPose[2]
                        suffix: "°"
                        anchors.left: parent.left
                        anchors.right: parent.right
                        editable: !moving
                        onClicked: () => {
                            poseCard.toggleEditPose();
                            j3Input.activate();
                        }
                    }

                    Input {
                        label: "J4:"
                        value: currentPose[3]
                        suffix: "°"
                        anchors.left: parent.left
                        anchors.right: parent.right
                        editable: !moving
                        onClicked: () => {
                            poseCard.toggleEditPose();
                            j4Input.activate();
                        }
                    }

                    Input {
                        label: "J5:"
                        value: currentPose[4]
                        suffix: "°"
                        anchors.left: parent.left
                        anchors.right: parent.right
                        editable: !moving
                        onClicked: () => {
                            poseCard.toggleEditPose();
                            j5Input.activate();
                        }
                    }

                    Input {
                        label: "J6:"
                        value: currentPose[5]
                        suffix: "°"
                        anchors.left: parent.left
                        anchors.right: parent.right
                        editable: !moving
                        onClicked: () => {
                            poseCard.toggleEditPose();
                            j6Input.activate();
                        }
                    }
                }

                Row {
                    spacing: 12

                    Column {
                        spacing: 12
                        width: (poseCard.availableWidth - parent.spacing) / 2

                        Text {
                            text: "Current Pose"
                            height: 18
                            font.weight: Font.DemiBold
                            color: "#555555"
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            anchors.left: parent.left
                            anchors.right: parent.right
                        }

                        Input {
                            label: "J1:"
                            value: currentPose[0]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            editable: false
                        }

                        Input {
                            label: "J2:"
                            value: currentPose[1]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            editable: false
                        }

                        Input {
                            label: "J3:"
                            value: currentPose[2]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            editable: false
                        }

                        Input {
                            label: "J4:"
                            value: currentPose[3]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            editable: false
                        }

                        Input {
                            label: "J5:"
                            value: currentPose[4]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            editable: false
                        }

                        Input {
                            label: "J6:"
                            value: currentPose[5]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            editable: false
                        }
                    }

                    Column {
                        spacing: 12
                        width: (poseCard.availableWidth - parent.spacing) / 2

                        Text {
                            text: "Target Pose"
                            height: 18
                            font.weight: Font.DemiBold
                            color: "#555555"
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            anchors.left: parent.left
                            anchors.right: parent.right
                        }

                        InputNumber {
                            id: j1Input
                            label: "J1:"
                            value: targetPose[0]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Right
                            editable: editPose
                            onSave: poseCard.changeTarget(0, result)
                        }

                        InputNumber {
                            id: j2Input
                            label: "J2:"
                            value: targetPose[1]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Right
                            editable: editPose
                            onSave: poseCard.changeTarget(1, result)
                        }

                        InputNumber {
                            id: j3Input
                            label: "J3:"
                            value: targetPose[2]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Right
                            editable: editPose
                            onSave: poseCard.changeTarget(2, result)
                        }

                        InputNumber {
                            id: j4Input
                            label: "J4:"
                            value: targetPose[3]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Right
                            editable: editPose
                            onSave: poseCard.changeTarget(3, result)
                        }

                        InputNumber {
                            id: j5Input
                            label: "J5:"
                            value: targetPose[4]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Right
                            editable: editPose
                            onSave: poseCard.changeTarget(4, result)
                        }

                        InputNumber {
                            id: j6Input
                            label: "J6:"
                            value: targetPose[5]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            numpadPlacement: InputNumber.Right
                            editable: editPose
                            onSave: poseCard.changeTarget(5, result)
                        }
                    }
                }
            }
        }

        Card {
            id: moveToCard
            font.pixelSize: 18

            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }

    Card {
        id: emulatorCard
        anchors.left: leftColumn.right
        anchors.top: parent.top
        anchors.right: rightColumn.left
        anchors.bottom: parent.bottom
        anchors.leftMargin: 12
        anchors.rightMargin: 12
        padding: 0

        RobotScene {
            id: rootEntity

            inTCP: !!motionTab.coordinateSystem
        }

        Row {
            anchors.top: parent.top
            anchors.topMargin: 12
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 12

            ZeroGravityControl {
                id: zeroGravity
                disabled: window.readOnlyMode || !window.robotRunning

                onOpenChanged: {
                    backend.setZeroGravity(open);
                }

                onStarted: {
                    backend.startZeroGravity();
                }

                onStopped: {
                    backend.stopZeroGravity();
                }
            }

            PushButton {
//                enabled: !window.readOnlyMode && window.robotRunning
                enabled: true
                text: "Align"
                width: 84
                size: PushButton.Large
                type: PushButton.Primary

                onPressed: {
//                    backend.align();
                }
                onReleased: {
//                    backend.stop();
                }
                onCanceled: {
//                    backend.stop();
                }
                onHoveredChanged: {
//                    if (!hovered && down) backend.stop();
                }
            }
        }

        Column {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.margins: 12
            spacing: 12

            ToggleButton {
                size: PushButton.Large
                display: AbstractButton.IconOnly
                iconSource: "qrc:///img/move"
                shape: PushButton.Circle
                active: !rootEntity.isRotate

                onClicked: rootEntity.changeMode(false)
            }

            ToggleButton {
                size: PushButton.Large
                display: AbstractButton.IconOnly
                iconSource: "qrc:///img/turn"
                shape: PushButton.Circle
                active: rootEntity.isRotate

                onClicked: rootEntity.changeMode(true)
            }
        }

        ZoomSlider {
            length: 360
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: 12
        }

        JoggingControl {
            enabled: !window.readOnlyMode && window.robotRunning
            radius: 240
        }
    }

    ColumnLayout {
        id: rightColumn
        spacing: 12
        width: 288
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        Card {
            id: positionCard
            font.pixelSize: 14

            Layout.fillWidth: true

            function toggleEditPosition () {
                targetPosition = currentPosition;
                mode = MotionTab.Mode.EditPosition;
            }

            function changeTarget(index, value) {
                targetPosition = [...targetPosition.slice(0, index), value, ...targetPosition.slice(index + 1)];
            }

            Column {
                spacing: 12
                anchors.left: parent.left
                anchors.right: parent.right

                StackLayout {
                    id: positionStackLayout
                    anchors.left: parent.left
                    anchors.right: parent.right
                    currentIndex: activePosition

                    Column {
                        spacing: 12

                        Text {
                            text: "TCP Position"
                            height: 18
                            font.weight: Font.DemiBold
                            color: "#555555"
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            anchors.left: parent.left
                            anchors.right: parent.right
                        }

                        Input {
                            label: "X:"
                            value: currentPosition[0]
                            suffix: " mm"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            editable: !moving
                            onClicked: {
                                positionCard.toggleEditPosition();
                                xInput.activate();
                            }
                        }

                        Input {
                            label: "Y:"
                            value: currentPosition[1]
                            suffix: " mm"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            editable: !moving
                            onClicked: () => {
                                positionCard.toggleEditPosition();
                                yInput.activate();
                            }
                        }

                        Input {
                            label: "Z:"
                            value: currentPosition[2]
                            suffix: " mm"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            editable: !moving
                            onClicked: () => {
                                positionCard.toggleEditPosition();
                                zInput.activate();
                            }
                        }

                        Input {
                            label: "RX:"
                            value: currentPosition[3]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            editable: !moving
                            onClicked: () => {
                                positionCard.toggleEditPosition();
                                rxInput.activate();
                            }
                        }

                        Input {
                            label: "RY:"
                            value: currentPosition[4]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            editable: !moving
                            onClicked: () => {
                                positionCard.toggleEditPosition();
                                ryInput.activate();
                            }
                        }

                        Input {
                            label: "RZ:"
                            value: currentPosition[5]
                            suffix: "°"
                            anchors.left: parent.left
                            anchors.right: parent.right
                            editable: !moving
                            onClicked: () => {
                                positionCard.toggleEditPosition();
                                rzInput.activate();
                            }
                        }
                    }

                    Row {
                        spacing: 12

                        Column {
                            spacing: 12
                            width: (positionCard.availableWidth - parent.spacing) / 2

                            Text {
                                text: "Current Position"
                                height: 18
                                font.weight: Font.DemiBold
                                color: "#555555"
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                anchors.left: parent.left
                                anchors.right: parent.right
                            }

                            Input {
                                label: "X:"
                                value: currentPosition[0]
                                suffix: " mm"
                                anchors.left: parent.left
                                anchors.right: parent.right
                                editable: false
                            }

                            Input {
                                label: "Y:"
                                value: currentPosition[1]
                                suffix: " mm"
                                anchors.left: parent.left
                                anchors.right: parent.right
                                editable: false
                            }

                            Input {
                                label: "Z:"
                                value: currentPosition[2]
                                suffix: " mm"
                                anchors.left: parent.left
                                anchors.right: parent.right
                                editable: false
                            }

                            Input {
                                label: "RX:"
                                value: currentPosition[3]
                                suffix: "°"
                                anchors.left: parent.left
                                anchors.right: parent.right
                                editable: false
                            }

                            Input {
                                label: "RY:"
                                value: currentPosition[4]
                                suffix: "°"
                                anchors.left: parent.left
                                anchors.right: parent.right
                                editable: false
                            }

                            Input {
                                label: "RZ:"
                                value: currentPosition[5]
                                suffix: "°"
                                anchors.left: parent.left
                                anchors.right: parent.right
                                editable: false
                            }
                        }

                        Column {
                            spacing: 12
                            width: (positionCard.availableWidth - parent.spacing) / 2

                            Text {
                                text: "Target Position"
                                height: 18
                                font.weight: Font.DemiBold
                                color: "#555555"
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                anchors.left: parent.left
                                anchors.right: parent.right
                            }

                            InputNumber {
                                id: xInput
                                label: "X:"
                                value: targetPosition[0]
                                suffix: " mm"
                                anchors.left: parent.left
                                anchors.right: parent.right
                                numpadPlacement: InputNumber.Left
                                editable: editPosition
                                onSave: positionCard.changeTarget(0, result)
                            }

                            InputNumber {
                                id: yInput
                                label: "Y:"
                                value: targetPosition[1]
                                suffix: " mm"
                                anchors.left: parent.left
                                anchors.right: parent.right
                                numpadPlacement: InputNumber.Left
                                editable: editPosition
                                onSave: positionCard.changeTarget(1, result)
                            }

                            InputNumber {
                                id: zInput
                                label: "Z:"
                                value: targetPosition[2]
                                suffix: " mm"
                                anchors.left: parent.left
                                anchors.right: parent.right
                                numpadPlacement: InputNumber.Left
                                editable: editPosition
                                onSave: positionCard.changeTarget(2, result)
                            }

                            InputNumber {
                                id: rxInput
                                label: "RX:"
                                value: targetPosition[3]
                                suffix: "°"
                                anchors.left: parent.left
                                anchors.right: parent.right
                                numpadPlacement: InputNumber.Left
                                editable: editPosition
                                onSave: positionCard.changeTarget(3, result)
                            }

                            InputNumber {
                                id: ryInput
                                label: "RY:"
                                value: targetPosition[4]
                                suffix: "°"
                                anchors.left: parent.left
                                anchors.right: parent.right
                                numpadPlacement: InputNumber.Left
                                editable: editPosition
                                onSave: positionCard.changeTarget(4, result)
                            }

                            InputNumber {
                                id: rzInput
                                label: "RZ:"
                                value: targetPosition[5]
                                suffix: "°"
                                anchors.left: parent.left
                                anchors.right: parent.right
                                numpadPlacement: InputNumber.Left
                                editable: editPosition
                                onSave: positionCard.changeTarget(5, result)
                            }
                        }
                    }
                }

                Text {
                    text: "Tool"
                    height: 18
                    font.weight: Font.DemiBold
                    color: "#555555"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors.left: parent.left
                    anchors.right: parent.right
                }

                Select {
                    enabled: !activePose && !window.readOnlyMode
                    anchors.left: parent.left
                    anchors.right: parent.right
                    placeholder: "Select tool"
                    // if no 'currentTool' yet set 'options' to []; if 'currentTool' is default tool set 'options' to tools without option '<Default TCP>';
                    // if 'currentInd' is tool from setup adding option '<Default TCP>' to reset tool to default;
                    options: currentTool ? currentToolId === -1 ? tools : ["<Default TCP>", ...tools] : []
                    // if no 'currentTool' yet set 'currentInd' to -1; if 'currentTool' is default tool set 'currentInd' to 'currentToolId' (which is -1);
                    // if 'currentInd' is tool from setup incrementing 'currentInd' by 1 to skip option '<Default TCP>';
                    currentInd: currentTool ? currentToolId === -1 ? currentToolId : currentToolId + 1 : -1

                    property var tools: window.currentSetup ? window.currentSetup.tools : []
                    property var currentTool: motionTab.tool
                    property int currentToolId: motionTab.tool ? tools.findIndex(t => t.id === currentTool.id) : -1

                    onActivated: {
                        if (currentToolId === -1) {
                            // if 'currentToolId' equals -1 that means default tool is current tool
                            // tool selected from tools from setup without change on index of selected tool
                            backend.setTool(window.currentSetup.tools[index].id);
                        } else {
                            // if 'currentToolId' NOT equals -1 that means tool from setup is current tool
                            // default tool selected if index of selected tool is les than 1 (equals 0 - option '<Default TCP>')
                            // otherwise tool selected from tools from setup with decremented by 1 index of selected tool
                            if (index < 1) backend.setTool("0");
                            else backend.setTool(window.currentSetup.tools[index - 1].id);
                        }
                    }
                }

                Text {
                    text: "Coordinate System"
                    height: 18
                    font.weight: Font.DemiBold
                    color: "#555555"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors.left: parent.left
                    anchors.right: parent.right
                }

                Select {
                    enabled: !activePose && !window.readOnlyMode
                    anchors.left: parent.left
                    anchors.right: parent.right
                    currentInd: motionTab.coordinateSystem
                    options: ["Base", "Tool"]
                    onActivated: {
                        backend.setCoordinateSystem(!!index);
                    }
                }
            }
        }

        Card {
            id: actionsCard

            Layout.fillHeight: true
            Layout.fillWidth: true

            Column {
                spacing: 12
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.right: parent.right
            }

            Text {
                visible: targetName
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.right: parent.right
                height: 36
                font.pixelSize: 18
                font.weight: Font.DemiBold
                color: "#333333"
                elide: Text.ElideRight
                text: targetName
                verticalAlignment: Text.AlignVCenter
            }

            Select {
                visible: normal && !targetName
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.right: parent.right
                currentInd: -1
                options: targets
                onActivated: {
                    targetPose = targets[index].pose;
                    targetName = targets[index].name;
                    mode = MotionTab.Mode.MovePose;
                    currentInd = -1;
                }
            }

            Column {
                spacing: 12
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                visible: hasOperation || moving || editing

                Loader {
                    anchors.left: parent.left
                    anchors.right: parent.right

                    Component {
                        id: moveButton

                        PushButton {
                            enabled: !window.readOnlyMode && window.robotRunning
                            type: PushButton.Primary
                            text: "Auto Move"
                            anchors.left: parent.left
                            anchors.right: parent.right

                            onPressed: {
                                handleMove();
                            }
                            onReleased: {
                                handleStop();
                            }
                            onCanceled: {
                                handleStop();
                            }
                            onHoveredChanged: {
                                if (!hovered && down) handleStop();
                            }
                        }
                    }

                    Component {
                        id: saveButton

                        PushButton {
                            text: "Save"
                            type: PushButton.Success
                            anchors.left: parent.left
                            anchors.right: parent.right

                            onClicked: {
                                handleSave();
                            }
                        }
                    }

                    sourceComponent: editing || moving ? moveButton : saveButton
                }

                PushButton {
                    text: "Cancel"
                    type: PushButton.Danger
                    anchors.left: parent.left
                    anchors.right: parent.right

                    onClicked: {
                        handleCancel();
                    }
                }
            }
        }
    }
}
