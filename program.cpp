#include "program.h"

Program::Program()
{
    Q_INIT_RESOURCE(qml);

    int rc;

    ssh = ssh_new();
    if (ssh == NULL)
        qDebug() << "SSH is not connected!";

    ssh_options_set(ssh, SSH_OPTIONS_USER, "programmer");
    ssh_options_set(ssh, SSH_OPTIONS_HOST, "localhost");

    rc = ssh_connect(ssh);

    if (rc != SSH_OK) {
        fprintf(stderr, "Error connecting to localhost: %s\n", ssh_get_error(ssh));
        exit(-1);
    }

    rc = ssh_userauth_publickey_auto(ssh, NULL, NULL);
    if (rc != SSH_AUTH_SUCCESS)
    {
        fprintf(stderr, "Error authenticating with public key: %s\n", ssh_get_error(ssh));
        ssh_disconnect(ssh);
        ssh_free(ssh);
        exit(-1);
    }

    sftp = sftp_new(ssh);
    if (sftp == NULL)
    {
      fprintf(stderr, "Error allocating SFTP session: %s\n", ssh_get_error(ssh));
      exit(-1);
    }

    rc = sftp_init(sftp);
    if (rc != SSH_OK)
    {
      fprintf(stderr, "Error initializing SFTP session: code %d.\n", sftp_get_error(sftp));
      sftp_free(sftp);
      exit(-1);
    }
}

Program::~Program()
{
    sftp_free(sftp);
    ssh_disconnect(ssh);
    ssh_free(ssh);
}

QVariantList Program::get_files_list(QString path)
{
    sftp_dir dir;
    sftp_attributes attributes;
    int result;
    QVariantList list;

    dir = sftp_opendir(sftp, (abs_path + path).toLocal8Bit().data());
    if (!dir)
    {
        fprintf(stderr, "Directory not opened: %s\n", ssh_get_error(ssh));
        return list;
    }

    while ((attributes = sftp_readdir(sftp, dir)) != NULL)
    {
        QVariantMap file = {
            {"name", QString(attributes->name)},
            {"size", (quint64)attributes->size},
            {"type", attributes->type},
            {"modified", (quint32)attributes->mtime},
            {"created", (quint64)attributes->createtime},
        };
        list.push_back(file);
        sftp_attributes_free(attributes);
    }

    if (!sftp_dir_eof(dir))
    {
        fprintf(stderr, "Can't list directory: %s\n", ssh_get_error(ssh));
        sftp_closedir(dir);
        return list;
    }

    result = sftp_closedir(dir);
    if (result != SSH_OK)
    {
        fprintf(stderr, "Can't close directory: %s\n", ssh_get_error(ssh));
        return list;
    }

    return list;
}

int Program::add_new_file(QString path)
{
    int access_type = O_CREAT;
    sftp_file file;
    const char *empty_file = "";
    int length = strlen(empty_file);
    int result, nwritten;

    file = sftp_open(sftp, (abs_path + path).toLocal8Bit().data(), access_type, S_IRWXU);
    if (file == NULL)
    {
        fprintf(stderr, "Can't open file for writing: %s\n", ssh_get_error(ssh));
        return SSH_ERROR;
    }

    nwritten = sftp_write(file, empty_file, length);
    if (nwritten != length)
    {
        fprintf(stderr, "Can't write data to file: %s\n", ssh_get_error(ssh));
        sftp_close(file);
        return SSH_ERROR;
    }

    result = sftp_close(file);
    if (result != SSH_OK)
    {
        fprintf(stderr, "Can't close the written file: %s\n", ssh_get_error(ssh));
        return result;
    }

    return SSH_OK;
}

QString Program::open_file(QString path)
{
    return read_file_content(path);
}

int Program::save_file(QString path, QString content)
{
    return write_file_content(content, path);
}

int Program::save_file_as(QString newPath, QString content)
{
    return write_file_content(content, newPath);
}

int Program::save_as(QString oldPath, QString newPath)
{
    QString content = read_file_content(oldPath);
    return write_file_content(content, newPath);
}

int Program::rename_file(QString oldPath, QString newPath)
{
    int result = sftp_rename(sftp, oldPath.toLocal8Bit().data(), newPath.toLocal8Bit().data());
    if (result != SSH_OK)
    {
        fprintf(stderr, "Can't rename file: %s\n", ssh_get_error(ssh));
        return result;
    }
    return SSH_OK;
}

int Program::delete_file(QString path)
{
    int result = sftp_unlink(sftp, path.toLocal8Bit().data());
    if (result != SSH_OK)
    {
        fprintf(stderr, "Can't delete file: %s\n", ssh_get_error(ssh));
        return result;
    }
    return SSH_OK;
}

int Program::add_new_dir(QString path)
{
    int result = sftp_mkdir(sftp, (abs_path + path).toLocal8Bit().data(), S_IRWXU);
    if (result != SSH_OK)
    {
        if (sftp_get_error(sftp) != SSH_FX_FILE_ALREADY_EXISTS)
        {
            fprintf(stderr, "Can't create directory: %s\n", ssh_get_error(ssh));
            return result;
        }
    }
    return SSH_OK;
}

int Program::rename_dir(QString oldPath, QString newPath)
{
    return rename_file(oldPath, newPath);
}

int Program::delete_dir(QString path)
{
    int result = sftp_rmdir(sftp, (abs_path + path).toLocal8Bit().data());
    if (result != SSH_OK)
    {
        if (sftp_get_error(sftp) != SSH_FX_FILE_ALREADY_EXISTS)
        {
            fprintf(stderr, "Can't delete directory: %s\n", ssh_get_error(ssh));
            return result;
        }
    }
    return SSH_OK;
}

QJsonDocument Program::text_to_json(QString text)
{
    return QJsonDocument::fromJson(text.toLocal8Bit());
}

QString Program::json_to_text(QJsonDocument json)
{
    return QString(json.toJson(QJsonDocument::Compact));
}

QString Program::read_file_content(QString path)
{
    int access_type;
    sftp_file file;
    char buffer[MAX_XFER_BUF_SIZE];
    int nbytes, result;
    QString content;

    access_type = O_RDONLY;
    file = sftp_open(sftp, (abs_path + path).toLocal8Bit().data(), access_type, 0);
    if (file == NULL) {
        fprintf(stderr, "Can't open file for reading: %s\n", ssh_get_error(ssh));
        return content;
    }

    for (;;) {
        nbytes = sftp_read(file, buffer, sizeof(buffer));
        if (nbytes == 0) {
            break; // EOF
        } else if (nbytes < 0) {
            fprintf(stderr, "Error while reading file: %s\n", ssh_get_error(ssh));
            sftp_close(file);
            return content;
        }

        content.append(buffer);
    }

    result = sftp_close(file);
    if (result != SSH_OK) {
        fprintf(stderr, "Can't close the read file: %s\n", ssh_get_error(ssh));
        return content;
    }

    return content;
}

int Program::write_file_content(QString content, QString path)
{
    int access_type = O_WRONLY;
    sftp_file file;
    int length = content.toLocal8Bit().length();
    int result, nwritten;

    file = sftp_open(sftp, (abs_path + path).toLocal8Bit().data(), access_type, S_IRWXU);
    if (file == NULL)
    {
        fprintf(stderr, "Can't open file for writing: %s\n", ssh_get_error(ssh));
        return SSH_ERROR;
    }

    nwritten = sftp_write(file, content.toLocal8Bit().data(), length);
    if (nwritten != length)
    {
        fprintf(stderr, "Can't write data to file: %s\n", ssh_get_error(ssh));
        sftp_close(file);
        return SSH_ERROR;
    }

    result = sftp_close(file);
    if (result != SSH_OK)
    {
        fprintf(stderr, "Can't close the written file: %s\n", ssh_get_error(ssh));
        return result;
    }

    return SSH_OK;
}



//void Program::open_setup_file()
//{
//    if (!setup_dir.exists("default.json")) return init_setup_file();

//    QFile file(setup_dir.filePath("default.json"));

//    if(file.open(QIODevice::ReadOnly))
//    {
//        currentSetup = QJsonDocument::fromJson(file.readAll()).object();

//        if (currentSetup["tools"].toArray().count() > 0) currentTool = currentSetup["tools"].toArray()[0].toObject();

//        if (currentSetup["payloads"].toArray().count() > 0) currentPayload = currentSetup["payloads"].toArray()[0].toObject();

//        file.close();
//    }
//    else return emit message("error", "Open error", "Setup file cannot be opened in read mode");
//}

//QJsonObject Program::get_curr_setup()
//{
//    return currentSetup;
//}

//QJsonObject Program::get_curr_tool()
//{
//    return currentTool;
//}

//QJsonObject Program::get_tool(QString id)
//{
//    if (id == "0") return get_default_tool();
//    QJsonArray tools = currentSetup["tools"].toArray();
//    for (int i = 0; i < tools.count(); i++)
//    {
//        QJsonObject tool = tools[i].toObject();
//        if (tool["id"].toString() == id) return tool;
//    }
//    return get_default_tool();
//}

//QJsonObject Program::get_curr_payload()
//{
//    return currentPayload;
//}

//QJsonObject Program::get_payload(QString id)
//{
//    if (id == "0") return get_default_payload();
//    QJsonArray payloads = currentSetup["payloads"].toArray();
//    for (int i = 0; i < payloads.count(); i++)
//    {
//        QJsonObject payload = payloads[i].toObject();
//        if (payload["id"].toString() == id) return payload;
//    }
//    return get_default_payload();
//}

//QJsonObject Program::get_curr_prog()
//{
//    return currentProgram;
//}

//QString Program::get_curr_prog_name()
//{
//    return currentProgramName;
//}

//QJsonObject Program::get_curr_comm()
//{
//    return currentCommand;
//}

//bool Program::is_buff_empty()
//{
//    return buffer.isEmpty();
//}

//void Program::add_new_prog(QString name)
//{
//    QFile defaultProgram(":/defaults/program.json");

//    if (defaultProgram.open(QIODevice::ReadOnly))
//    {
//        currentProgram = QJsonDocument::fromJson(defaultProgram.readAll()).object();

//        defaultProgram.close();
//    }
//    else return emit message("error", "Open error", "Default Program file cannot be opened in read mode");

//    currentCommand = currentProgram["body"].toArray()[0].toObject();

//    currentProgramName = name;

//    save_prog_file(currentProgramName);
//}

//void Program::open_prog(QString fileName)
//{
//    if (program_dir.exists(fileName + ".json"))
//    {
//        QFile file(program_dir.filePath(fileName + ".json"));

//        if(file.open(QIODevice::ReadOnly))
//        {
//            currentProgram = QJsonDocument::fromJson(file.readAll()).object();
//            currentProgramName = fileName;
//            currentCommand = currentProgram["body"].toArray()[0].toObject();

//            file.close();
//        }
//        else return emit message("error", "Open error", "Selected Program file cannot be opened in read mode");
//    }
//    else return emit message("error", "Open error", "Selected Program file does not exist");
//}

//void Program::save_prog()
//{
//    save_prog_file(currentProgramName);
//}

//void Program::save_prog_as(QString newName)
//{
//    save_prog_file(newName);
//    currentProgramName = newName;
//}

//void Program::rename_prog(QString newName)
//{
//    program_dir.rename(currentProgramName + ".json", newName + ".json");
//}

//void Program::select_comm(QString id)
//{
//    QJsonValue foundValue = find_com(currentProgram["body"].toArray(), id);
//    if (foundValue.isNull()) return;
//    currentCommand = foundValue.toObject();
//}

//void Program::expand_comm(QString id, bool expand)
//{
//    QJsonValue foundValue = find_com(currentProgram["body"].toArray(), id);
//    if (foundValue.isNull()) return;
//    QJsonObject currentCommand = foundValue.toObject();

//    currentProgram["body"] = change_com_param(currentProgram["body"].toArray(), currentCommand, "expanded", expand);
//}

//void Program::add_comm(QString type)
//{
//    QJsonObject newCommand = get_com(type);

//    bool success = false;

//    currentProgram["body"] = add_com(currentProgram["body"].toArray(), currentCommand, newCommand, &success);

//    // TODO: prevent insertion to parent on success = false in case of ELIF and ELSE
//    if (success) currentCommand = newCommand;
//    else
//    {
//        currentProgram["body"] = add_com_to_par(currentProgram["body"].toArray(), currentCommand, newCommand);

//        currentCommand = newCommand;
//    }
//}

//void Program::change_comm_param(QString key, QVariant value)
//{
//    currentProgram["body"] = change_com_param(currentProgram["body"].toArray(), currentCommand, key, value);

//    select_comm(currentCommand["id"].toString());
//}

//void Program::move_comm(bool up)
//{
//    currentProgram["body"] = move_com(currentProgram["body"].toArray(), currentCommand, up);
//}

//void Program::cut_comm()
//{
//    buffer = currentCommand;

//    currentProgram["body"] = remove_com(currentProgram["body"].toArray(), currentCommand);
//}

//void Program::copy_comm()
//{
//    buffer = currentCommand;
//}

//void Program::paste_comm()
//{
//    QJsonObject bufferCommand = buffer;
//    QJsonArray commands = {bufferCommand};
//    commands = com_unq_id(commands);
//    bufferCommand = commands[0].toObject();

//    bool success = false;

//    currentProgram["body"] = insert_com(currentProgram["body"].toArray(), currentCommand, bufferCommand, &success);

//    if (success) currentCommand = bufferCommand;
//    else
//    {
//        currentProgram["body"] = add_com_to_par(currentProgram["body"].toArray(), currentCommand, bufferCommand);

//        currentCommand = bufferCommand;
//    }
//}

//void Program::ignore_comm(bool ignore)
//{
//    currentProgram["body"] = change_com_param(currentProgram["body"].toArray(), currentCommand, "ignored", ignore);

//    select_comm(currentCommand["id"].toString());
//}

//void Program::remove_comm()
//{
//    currentProgram["body"] = remove_com(currentProgram["body"].toArray(), currentCommand);
//}

//void Program::create_pnt(QVariantList position, QVariantList pose)
//{
//    QJsonArray points = currentProgram["points"].toArray();

//    QString name = "";
//    QStringList names;
//    for (int i = 0; i < points.count(); i++) names.append(points[i].toObject()["name"].toString());
//    for (int i = 1; ; i++) {
//        if (names.contains(QString::asprintf("New_point_%d", i))) continue;
//        name = QString::asprintf("New_point_%d", i);
//        break;
//    }
//    QJsonObject point = {
//        {"id", get_id()},
//        {"name", name},
//        {"position", QJsonArray::fromVariantList(position)},
//        {"pose", QJsonArray::fromVariantList(pose)}
//    };
//    points.append(point);
//    currentProgram["points"] = points;

//    change_comm_param("pointId", point["id"].toString());
//}

//void Program::edit_pnt(QVariantList position, QVariantList pose)
//{
//    QJsonArray points = currentProgram["points"].toArray();

//    for(int i = 0; points.count(); i++)
//    {
//        QJsonObject point = points[i].toObject();
//        if (point["id"].toString() == currentCommand["pointId"].toString())
//        {
//            point["position"] = QJsonArray::fromVariantList(position);
//            point["pose"] = QJsonArray::fromVariantList(pose);
//            points[i] = point;
//            break;
//        }
//    }

//    currentProgram["points"] = points;
//}

//void Program::rename_pnt(QString pointId, QString newName)
//{
//    QJsonArray points = currentProgram["points"].toArray();

//    for(int i = 0; points.count(); i++)
//    {
//        QJsonObject point = points[i].toObject();
//        if (point["id"].toString() == pointId)
//        {
//            point["name"] = newName;
//            points[i] = point;
//            break;
//        }
//    }

//    currentProgram["points"] = points;
//}

//void Program::delete_pnt(QString pointId)
//{
//    QJsonArray points = currentProgram["points"].toArray();

//    for(int i = 0; points.count(); i++)
//    {
//        QJsonObject point = points[i].toObject();
//        if (point["id"].toString() == pointId)
//        {
//            points.removeAt(i);
//            break;
//        }
//    }

//    currentProgram["points"] = points;
//}

//void Program::add_var()
//{
//    QJsonArray variables = currentProgram["variables"].toArray();

//    QString name = "";
//    QStringList names;
//    for (int i = 0; i < variables.count(); i++) names.append(variables[i].toObject()["name"].toString());
//    for (int i = 1; ; i++) {
//        if (names.contains(QString::asprintf("New_vat_%d", i))) continue;
//        name = QString::asprintf("New_var_%d", i);
//        break;
//    }
//    QJsonObject variable = {
//        {"id", get_id()},
//        {"name", name},
//        {"value", QJsonValue()}
//    };
//    variables.append(variable);
//    currentProgram["variables"] = variables;
//}

//void Program::create_var()
//{
//    QJsonArray variables = currentProgram["variables"].toArray();

//    QString name = "";
//    QStringList names;
//    for (int i = 0; i < variables.count(); i++) names.append(variables[i].toObject()["name"].toString());
//    for (int i = 1; ; i++) {
//        if (names.contains(QString::asprintf("New_vat_%d", i))) continue;
//        name = QString::asprintf("New_var_%d", i);
//        break;
//    }
//    QJsonObject variable = {
//        {"id", get_id()},
//        {"name", name},
//        {"value", QJsonValue()}
//    };
//    variables.append(variable);
//    currentProgram["variables"] = variables;

//    QJsonObject operation = {
//        {"type", "variable"},
//        {"id", variable["id"].toString()},
//        {"value", ""}
//    };
//    QJsonArray operations = currentCommand["operations"].toArray();
//    if (operations.count() == 1)
//    {
//        operations[0] = operation;
//    }
//    change_comm_param("operations", operations);
//}

//void Program::edit_var(QString varId, QString value)
//{
//    QJsonArray variables = currentProgram["variables"].toArray();

//    for(int i = 0; variables.count(); i++)
//    {
//        QJsonObject variable = variables[i].toObject();
//        if (variable["id"].toString() == varId)
//        {
//            variable["value"] = QJsonValue::fromVariant(value);
//            variables[i] = variable;
//            break;
//        }
//    }

//    currentProgram["variables"] = variables;
//}

//void Program::rename_io(QString type, int index, QString newName)
//{
//    QJsonObject io = currentSetup["io"].toObject();
//    QJsonArray arr = io[type].toArray();
//    QJsonObject obj = arr[index].toObject();
//    obj["name"] = newName;
//    arr[index] = obj;
//    io[type] = arr;
//    currentSetup["io"] = io;

//    save_setup_file("default");
//}

//void Program::add_tool()
//{
//    QJsonArray tools = currentSetup["tools"].toArray();
//    QString name = "New_tool_0";
//    QStringList names;
//    for (int i = 0; i < tools.count(); i++) names.append(tools[i].toObject()["name"].toString());
//    for (int i = 1; ; i++) {
//        if (names.contains(QString::asprintf("New_tool_%d", i))) continue;
//        name = QString::asprintf("New_tool_%d", i);
//        break;
//    }
//    QJsonObject new_tool = get_default_tool();
//    new_tool["id"] = QString::number(QDateTime::currentMSecsSinceEpoch(), 36);
//    new_tool["name"] = name;
//    tools.append(new_tool);
//    currentSetup["tools"] = tools;

//    save_setup_file("default");

//    currentTool = new_tool;
//}

//void Program::select_tool(QString id)
//{
//    QJsonArray tools = currentSetup["tools"].toArray();
//    for (int i = 0; i < tools.count(); i++)
//    {
//        QJsonObject tool = tools[i].toObject();
//        if (tool["id"].toString() == id)
//        {
//            currentTool = tool;
//            break;
//        }
//    }
//}

//void Program::rename_tool(QString newName)
//{
//    rename_tool(newName, currentTool["id"].toString());
//}

//void Program::rename_tool(QString newName, QString toolId)
//{
//    QJsonArray tools = currentSetup["tools"].toArray();
//    for (int i = 0; i < tools.count(); i++)
//    {
//        QJsonObject tool = tools[i].toObject();
//        if (tool["id"].toString() == toolId)
//        {
//            tool["name"] = newName;
//            tools[i] = tool;
//            if (currentTool["id"].toString() == toolId) currentTool = tool;
//            break;
//        }
//    }
//    currentSetup["tools"] = tools;

//    save_setup_file("default");
//}

//void Program::edit_tool(int index, double value)
//{
//    QJsonArray tools = currentSetup["tools"].toArray();
//    for (int i = 0; i < tools.count(); i++)
//    {
//        QJsonObject tool = tools[i].toObject();
//        if (tool["id"].toString() == currentTool["id"].toString())
//        {
//            QJsonArray tcp = tool["tcp"].toArray();
//            tcp[index] = value;
//            tool["tcp"] = tcp;
//            tools[i] = tool;
//            currentTool = tool;
//            break;
//        }
//    }
//    currentSetup["tools"] = tools;

//    save_setup_file("default");
//}

//void Program::delete_tool()
//{
//    QJsonArray tools = currentSetup["tools"].toArray();
//    for (int i = 0; i < tools.count(); i++)
//    {
//        QJsonObject tool = tools[i].toObject();
//        if (tool["id"].toString() == currentTool["id"].toString())
//        {
//            if (tools[i - 1].isObject()) currentTool = tools[i - 1].toObject();
//            else if (tools[i + 1].isObject()) currentTool = tools[i + 1].toObject();
//            tools.removeAt(i);
//            break;
//        }
//    }
//    currentSetup["tools"] = tools;

//    save_setup_file("default");
//}

//void Program::add_payload()
//{
//    QJsonArray payloads = currentSetup["payloads"].toArray();
//    QString name = "New_payload_0";
//    QStringList names;
//    for (int i = 0; i < payloads.count(); i++) names.append(payloads[i].toObject()["name"].toString());
//    for (int i = 1; ; i++) {
//        if (names.contains(QString::asprintf("New_payload_%d", i))) continue;
//        name = QString::asprintf("New_payload_%d", i);
//        break;
//    }
//    QJsonObject new_payload = get_default_payload();
//    new_payload["id"] = QString::number(QDateTime::currentMSecsSinceEpoch(), 36);
//    new_payload["name"] = name;
//    payloads.append(new_payload);
//    currentSetup["payloads"] = payloads;

//    save_setup_file("default");

//    currentPayload = new_payload;
//}

//void Program::select_payload(QString id)
//{
//    QJsonArray payloads = currentSetup["payloads"].toArray();
//    for (int i = 0; i < payloads.count(); i++)
//    {
//        QJsonObject payload = payloads[i].toObject();
//        if (payload["id"].toString() == id)
//        {
//            currentPayload = payload;
//            break;
//        }
//    }
//}

//void Program::rename_payload(QString newName)
//{
//    rename_payload(newName, currentPayload["id"].toString());
//}

//void Program::rename_payload(QString newName, QString payloadId)
//{
//    QJsonArray payloads = currentSetup["payloads"].toArray();
//    for (int i = 0; i < payloads.count(); i++)
//    {
//        QJsonObject payload = payloads[i].toObject();
//        if (payload["id"].toString() == payloadId)
//        {
//            payload["name"] = newName;
//            payloads[i] = payload;
//            if (currentPayload["id"].toString() == payloadId) currentPayload = payload;
//            break;
//        }
//    }
//    currentSetup["payloads"] = payloads;

//    save_setup_file("default");
//}

//void Program::edit_payload(QString key, double value)
//{
//    QJsonArray payloads = currentSetup["payloads"].toArray();
//    for (int i = 0; i < payloads.count(); i++)
//    {
//        QJsonObject payload = payloads[i].toObject();
//        if (payload["id"].toString() == currentPayload["id"].toString())
//        {
//            payload[key] = value;
//            payloads[i] = payload;
//            currentPayload = payload;
//            break;
//        }
//    }
//    currentSetup["payloads"] = payloads;

//    save_setup_file("default");
//}

//void Program::delete_payload()
//{
//    QJsonArray payloads = currentSetup["payloads"].toArray();
//    for (int i = 0; i < payloads.count(); i++)
//    {
//        QJsonObject payload = payloads[i].toObject();
//        if (payload["id"].toString() == currentPayload["id"].toString())
//        {
//            if (payloads[i - 1].isObject()) currentPayload = payloads[i - 1].toObject();
//            else if (payloads[i + 1].isObject()) currentPayload = payloads[i + 1].toObject();
//            payloads.removeAt(i);
//            break;
//        }
//    }
//    currentSetup["payloads"] = payloads;

//    save_setup_file("default");
//}

//void Program::change_strp_param(QString key, QVariant value)
//{
//    QJsonObject startup = currentSetup["startup"].toObject();

//    startup[key] = QJsonValue::fromVariant(value);

//    currentSetup["startup"] = startup;

//    save_setup_file("default");
//}

//void Program::save_prog_file(QString fileName)
//{
//    QJsonDocument document(currentProgram);

//    QFile file(program_dir.filePath(fileName + ".json"));

//    if (file.open(QIODevice::WriteOnly | QFile::Text | QFile::Truncate))
//    {
//        file.write(document.toJson(QJsonDocument::Indented));
//        file.close();
//    }
//    else return emit message("error", "Open error", "Program file cannot be opened in write mode");
//}

//void Program::save_setup_file(QString fileName)
//{
//    QJsonDocument document(currentSetup);

//    QFile file(setup_dir.filePath(fileName + ".json"));

//    if (file.open(QIODevice::WriteOnly | QFile::Text | QFile::Truncate))
//    {
//        file.write(document.toJson(QJsonDocument::Compact));
//        file.close();
//    }
//    else return emit message("error", "Open error", "Setup file cannot be opened in write mode");
//}

//void Program::init_setup_file()
//{
//    QFile defaultSetup(":/defaults/setup.json");

//    if (defaultSetup.open(QIODevice::ReadOnly))
//    {
//        currentSetup = QJsonDocument::fromJson(defaultSetup.readAll()).object();

//        defaultSetup.close();
//    }
//    else return emit message("error", "Open error", "Default Setup file cannot be opened in read mode");

//    save_setup_file("default");
//}

//QDir Program::get_stores_dir()
//{
//    QDir cwd = QDir::current();

//    if (cwd.cd("/opt/impulse"))
//        if (cwd.mkpath("/opt/impulse/stores/")) cwd.cd("/opt/impulse/stores/");
//        else qDebug() << "Unable to create stores path";
//    else
//        if (cwd.absolutePath().endsWith("impulse/build"))
//        {
//            if (cwd.mkpath("../stores/")) cwd.cd("../stores/");
//            else qDebug() << "Unable to create stores path";
//        }

//    qDebug() << "stores dir" << cwd;

//    return cwd;
//}

//Program::CommandType Program::get_com_type_enum(QString type)
//{
//    int enumIndex = Program::staticMetaObject.indexOfEnumerator("CommandType");
//    QMetaEnum metaEnum = Program::staticMetaObject.enumerator(enumIndex);
//    return CommandType(metaEnum.keyToValue(type.toLocal8Bit()));
//}

//QString Program::get_id()
//{
//    QString id = QString::number(currentProgram["currentId"].toInt());
//    currentProgram["currentId"] = currentProgram["currentId"].toInt() + 1;
//    return id;
//}

//QJsonObject Program::get_com(QString type)
//{
//    QJsonObject command;
//    CommandType commandType = get_com_type_enum(type);
//    switch (commandType) {
//    case PROGRAM: break;
//    case SET:
//    {
//        QJsonObject operation = {
//            {"type", "digital"}, // digital analog variable tcp payload ?register?
//            {"key", 0},
//            {"value", false},
//            {"expression", ""},
//            {"useValue", true},
//        };
//        QJsonArray operations = { operation };
//        command = {
//            {"type", type},
//            {"operations", operations},
//        };
//        break;
//    }
//    case GRIPPER:
//    {
//        command = {
//            {"type", type},
//            {"value", false}
//        };
//        break;
//    }
//    case WAIT:
//    {
//        QJsonObject digital = {
//            {"index", 0},
//            {"value", false},
//        };
//        QJsonObject analog = {
//            {"index", 0},
//            {"moreThan", true},
//            {"value", 0.0},
//        };
//        command = {
//            {"type", type},
//            {"option", "time"},
//            {"time", 0.0},
//            {"digital", digital},
//            {"analog", analog},
//        };
//        break;
//    }
//    case MOVE:
//    {
//        command = {
//            {"type", type},
//            {"motion", "joint"},
//            {"maxTransVel", 0.25},
//            {"maxTransAcc", 1.2},
//            {"maxRotVel", 1.047197},
//            {"maxRotAcc", 1.396263},
//            {"maxJointVel", 1.047197},
//            {"maxJointAcc", 1.396263},
//            {"autoBlend", false},
//            {"blend", 0.0},
//            {"subCommands", QJsonArray()},
//            {"expanded", true},
//        };
//        break;
//    }
//    case POINT:
//    {
//        command = {
//            {"type", type},
//            {"pointId", ""},
//        };
//        break;
//    }
//    case COMMENT:
//    {
//        command = {
//            {"type", type},
//            {"comment", ""},
//        };
//        break;
//    }
//    case POPUP:
//    {
//        command = {
//            {"type", type},
//            {"popupType", 0},
//            {"message", ""},
//            {"buttonText", "Ok"},
//            {"playSound", false},
//            {"soundName", "beep_1"},
//        };
//        break;
//    }
//    case LOOP:
//    {
//        command = {
//            {"type", type},
//            {"mode", "endless"},
//            {"count", 0},
//            {"expression", ""},
//            {"subCommands", QJsonArray()},
//            {"expanded", true},
//        };
//        break;
//    }
//    case IF:
//    {
//        command = {
//            {"type", type},
//            {"expression", ""},
//            {"subCommands", QJsonArray()},
//            {"expanded", true},
//        };
//        break;
//    }
//    case ELIF:
//    {
//        command = {
//            {"type", type},
//            {"expression", ""},
//            {"subCommands", QJsonArray()},
//            {"expanded", true},
//        };
//        break;
//    }
//    case ELSE:
//    {
//        command = {
//            {"type", type},
//            {"subCommands", QJsonArray()},
//            {"expanded", true},
//        };
//        break;
//    }
//    }
//    command["id"] = get_id();
//    command["ignored"] = false;
//    return command;
//}

//QJsonValue Program::find_com(QJsonArray arr, QString id) const
//{
//    QJsonValue result;
//    for(QJsonValue val : arr)
//    {
//        if (val.toObject()["id"].toString() == id)
//        {
//            return val;
//        }
//        if (val.toObject()["subCommands"].isArray())
//        {
//            result = find_com(val.toObject()["subCommands"].toArray(), id);
//        }
//        if (!result.isNull()) break;
//    }
//    return result;
//}

//QJsonArray Program::add_com(QJsonArray arr, QJsonObject currentCommand, QJsonObject newCommand, bool *success)
//{
//    for (int i = 0; i < arr.count(); i++)
//    {
//        QJsonObject obj = arr[i].toObject();
//        if (obj["id"].toString() == currentCommand["id"].toString())
//        {
//            CommandType currentCommandType = get_com_type_enum(currentCommand["type"].toString());
//            CommandType newCommandType = get_com_type_enum(newCommand["type"].toString());

//            if (newCommandType == ELIF || newCommandType == ELSE) {
//                if (currentCommandType == IF || currentCommandType == ELIF) {
//                    // insert ELIF or ELSE after IF or ELIF
//                    arr[i] = obj;
//                    arr.insert(i + 1, newCommand);
//                    *success = true;
//                    return arr;
//                }
//                else
//                {
//                    arr[i] = obj;
//                    *success = false;
//                    return arr;
//                }
//            }
//            if (currentCommandType == PROGRAM || currentCommandType == LOOP || currentCommandType == IF || currentCommandType == ELIF || currentCommandType == ELSE)
//            {
//                if (newCommandType == POINT)
//                {
//                    // prepend POINT type newCommand to new MOVE type moveCommand's subCommands
//                    // and prepend moveCommand to subCommands of PROGRAM, LOOP, IF, ELIF, ELSE type currentCommand
//                    QJsonObject moveCommand = get_com("MOVE");
//                    QJsonArray moveCommandChildren = moveCommand["subCommands"].toArray();
//                    moveCommandChildren.prepend(newCommand);
//                    moveCommand["subCommands"] = moveCommandChildren;

//                    QJsonArray objArr = obj["subCommands"].toArray();
//                    objArr.prepend(moveCommand);
//                    obj["subCommands"] = objArr;
//                    arr[i] = obj;
//                    *success = true;
//                    return arr;
//                }
//                else
//                {
//                    // prepend newCommand to subCommands of PROGRAM, LOOP, IF, ELIF, ELSE type currentCommand
//                    QJsonArray objArr = obj["subCommands"].toArray();
//                    objArr.prepend(newCommand);
//                    obj["subCommands"] = objArr;
//                    arr[i] = obj;
//                    *success = true;
//                    return arr;
//                }
//            }
//            else
//            {
//                if (currentCommandType == MOVE)
//                {
//                    if (newCommandType == POINT)
//                    {
//                        // prepend POINT type newCommand to subCommands of MOVE type currentCommand
//                        QJsonArray objArr = obj["subCommands"].toArray();
//                        objArr.prepend(newCommand);
//                        obj["subCommands"] = objArr;
//                        obj["expanded"] = true;
//                        arr[i] = obj;
//                        *success = true;
//                        return arr;
//                    }
//                    else
//                    {
//                        // insert newCommand after MOVE type currentCommand
//                        arr[i] = obj;
//                        arr.insert(i + 1, newCommand);
//                        *success = true;
//                        return arr;
//                    }
//                }
//                else if (currentCommandType == POINT)
//                {
//                    if (newCommandType == POINT)
//                    {
//                        // insert POINT type newCommand after POINT type currentCommand
//                        arr[i] = obj;
//                        arr.insert(i + 1, newCommand);
//                        *success = true;
//                        return arr;
//                    }
//                    else
//                    {
//                        // unable to insert non-POINT type newCommand after POINT type currentCommand
//                        // emit message("warning", "Warning", "Only commands of type POINT can be added into command MOVE.");
//                        arr[i] = obj;
//                        *success = false;
//                        return arr;
//                    }
//                }
//                else
//                {
//                    if (newCommandType == POINT)
//                    {
//                        // prepend POINT type newCommand to new MOVE type moveCommand's subCommands
//                        // and insert moveCommand after currentCommand
//                        QJsonObject moveCommand = get_com("MOVE");
//                        QJsonArray moveCommandChildren = moveCommand["subCommands"].toArray();
//                        moveCommandChildren.prepend(newCommand);
//                        moveCommand["subCommands"] = moveCommandChildren;

//                        arr[i] = obj;
//                        arr.insert(i + 1, moveCommand);
//                        *success = true;
//                        return arr;
//                    }
//                    else
//                    {
//                        // insert newCommand after currentCommand
//                        arr[i] = obj;
//                        arr.insert(i + 1, newCommand);
//                        *success = true;
//                        return arr;
//                    }
//                }
//            }
//        }
//        if (obj["subCommands"].isArray())
//        {
//            QJsonArray subComArr = obj["subCommands"].toArray();
//            obj["subCommands"] = add_com(subComArr, currentCommand, newCommand, success);
//        }
//        else
//        {
//            // have to remove subCommands due to its creation while checking its presence
//            obj.remove("subCommands");
//        }
//        arr[i] = obj;
//    }
//    return arr;
//}

//QJsonArray Program::add_com_to_par(QJsonArray arr, QJsonObject currentCommand, QJsonObject newCommand)
//{
//    for (int i = 0; i < arr.count(); i++)
//    {
//        QJsonObject par = arr[i].toObject();
//        if (par["subCommands"].isArray())
//        {
//            QJsonArray parArr = par["subCommands"].toArray();
//            for (int j = 0; j < parArr.count(); j++)
//            {
//                if (parArr[j].toObject()["id"].toString() == currentCommand["id"].toString())
//                {
//                    arr[i] = par;
//                    arr.insert(i + 1, newCommand);
//                    return arr;
//                }
//            }
//            par["subCommands"] = add_com_to_par(parArr, currentCommand, newCommand);
//        }
//        else
//        {
//            // have to remove subCommands due to its creation while checking its presence
//            par.remove("subCommands");
//        }
//        arr[i] = par;
//    }
//    return arr;
//}

//QJsonArray Program::insert_com(QJsonArray arr, QJsonObject currentCommand, QJsonObject command, bool *success, bool insert_after)
//{
//    for (int i = 0; i < arr.count(); i++)
//    {
//        QJsonObject obj = arr[i].toObject();
//        if (obj["id"].toString() == currentCommand["id"].toString())
//        {
//            CommandType currentCommandType = get_com_type_enum(currentCommand["type"].toString());
//            CommandType commandType = get_com_type_enum(command["type"].toString());

//            if (commandType == ELIF || commandType == ELSE) {
//                if (currentCommandType == IF || currentCommandType == ELIF) {
//                    // insert ELIF or ELSE after IF or ELIF
//                    arr[i] = obj;
//                    arr.insert(i + 1, command);
//                    *success = true;
//                    return arr;
//                }
//                else
//                {
//                    arr[i] = obj;
//                    *success = false;
//                    return arr;
//                }
//            }
//            if (currentCommandType == PROGRAM || currentCommandType == LOOP || currentCommandType == IF || currentCommandType == ELIF || currentCommandType == ELSE)
//            {
//                if (commandType == POINT)
//                {
//                    // prepend POINT type newCommand to new MOVE type moveCommand's subCommands
//                    // and prepend moveCommand to subCommands of PROGRAM, LOOP, IF, ELIF, ELSE type currentCommand
//                    QJsonObject moveCommand = get_com("MOVE");
//                    QJsonArray moveCommandChildren = moveCommand["subCommands"].toArray();
//                    moveCommandChildren.prepend(command);
//                    moveCommand["subCommands"] = moveCommandChildren;

//                    QJsonArray objArr = obj["subCommands"].toArray();
//                    objArr.prepend(moveCommand);
//                    obj["subCommands"] = objArr;
//                    arr[i] = obj;
//                    *success = true;
//                    return arr;
//                }
//                else
//                {
//                    // prepend newCommand to subCommands of PROGRAM, LOOP, IF, ELIF, ELSE type currentCommand
//                    QJsonArray objArr = obj["subCommands"].toArray();
//                    objArr.prepend(command);
//                    obj["subCommands"] = objArr;
//                    arr[i] = obj;
//                    *success = true;
//                    return arr;
//                }
//            }
//            else
//            {
//                if (currentCommandType == MOVE)
//                {
//                    if (commandType == POINT)
//                    {
//                        // prepend POINT type newCommand to subCommands of MOVE type currentCommand
//                        QJsonArray objArr = obj["subCommands"].toArray();
//                        objArr.prepend(command);
//                        obj["subCommands"] = objArr;
//                        obj["expanded"] = true;
//                        arr[i] = obj;
//                        *success = true;
//                        return arr;
//                    }
//                    else
//                    {
//                        // insert newCommand after MOVE type currentCommand
//                        arr[i] = obj;
//                        arr.insert(i + 1, command);
//                        *success = true;
//                        return arr;
//                    }
//                }
//                else if (currentCommandType == POINT)
//                {
//                    if (commandType == POINT)
//                    {
//                        // insert POINT type newCommand after POINT type currentCommand
//                        arr[i] = obj;
//                        arr.insert(i + 1, command);
//                        *success = true;
//                        return arr;
//                    }
//                    else
//                    {
//                        // unable to insert non-POINT type newCommand after POINT type currentCommand
//                        // emit message("warning", "Warning", "Only commands of type POINT can be added into command MOVE.");
//                        arr[i] = obj;
//                        *success = false;
//                        return arr;
//                    }
//                }
//                else
//                {
//                    if (commandType == POINT)
//                    {
//                        // prepend POINT type newCommand to new MOVE type moveCommand's subCommands
//                        // and insert moveCommand after currentCommand
//                        QJsonObject moveCommand = get_com("MOVE");
//                        QJsonArray moveCommandChildren = moveCommand["subCommands"].toArray();
//                        moveCommandChildren.prepend(command);
//                        moveCommand["subCommands"] = moveCommandChildren;

//                        arr[i] = obj;
//                        arr.insert(i + 1, moveCommand);
//                        *success = true;
//                        return arr;
//                    }
//                    else
//                    {
//                        // insert newCommand after currentCommand
//                        arr[i] = obj;
//                        arr.insert(i + 1, command);
//                        *success = true;
//                        return arr;
//                    }
//                }
//            }
//        }
//        if (obj["subCommands"].isArray())
//        {
//            QJsonArray subComArr = obj["subCommands"].toArray();
//            obj["subCommands"] = insert_com(subComArr, currentCommand, command, success, insert_after);
//        }
//        else
//        {
//            // have to remove subCommands due to its creation while checking its presence
//            obj.remove("subCommands");
//        }
//        arr[i] = obj;
//    }
//    return arr;
//}

//QJsonArray Program::insert_com_to_par(QJsonArray arr, QJsonObject currentCommand, QJsonObject command)
//{
//    for (int i = 0; i < arr.count(); i++)
//    {
//        QJsonObject par = arr[i].toObject();
//        if (par["subCommands"].isArray())
//        {
//            QJsonArray parArr = par["subCommands"].toArray();
//            for (int j = 0; j < parArr.count(); j++)
//            {
//                if (parArr[j].toObject()["id"].toString() == currentCommand["id"].toString())
//                {
//                    arr[i] = par;
//                    arr.insert(i + 1, command);
//                    return arr;
//                }
//            }
//            par["subCommands"] = add_com_to_par(parArr, currentCommand, command);
//        }
//        else
//        {
//            // have to remove subCommands due to its creation while checking its presence
//            par.remove("subCommands");
//        }
//        arr[i] = par;
//    }
//    return arr;
//}

//QJsonArray Program::move_com(QJsonArray arr, QJsonObject currentCommand, bool moveUp)
//{
//    for (int i = 0; i < arr.count(); i++)
//    {
//        QJsonObject obj = arr[i].toObject();
//        if (obj["id"].toString() == currentCommand["id"].toString())
//        {
//            if (moveUp)
//            {
//                QJsonValue temp = arr[i - 1];
//                if (temp.isObject())
//                {
//                    arr[i - 1] = arr[i];
//                    arr[i] = temp;
//                }
//                return arr;
//            }
//            else
//            {
//                QJsonValue temp = arr[i + 1];
//                if (temp.isObject())
//                {
//                    arr[i + 1] = arr[i];
//                    arr[i] = temp;
//                }
//                return arr;
//            }
//        }
//        if (obj["subCommands"].isArray())
//        {
//            QJsonArray subComArr = obj["subCommands"].toArray();
//            obj["subCommands"] = move_com(subComArr, currentCommand, moveUp);
//        }
//        else
//        {
//            // have to remove subCommands due to its creation while checking its presence
//            obj.remove("subCommands");
//        }
//        arr[i] = obj;
//    }
//    return arr;
//}

//QJsonArray Program::remove_com(QJsonArray arr, QJsonObject currentCommand, QJsonObject parentCommand)
//{
//    for (int i = 0; i < arr.count(); i++)
//    {
//        QJsonObject obj = arr[i].toObject();
//        if (obj["id"].toString() == currentCommand["id"].toString())
//        {
//            if (arr[i - 1].isObject()) Program::currentCommand = arr[i - 1].toObject();
//            else if (arr[i + 1].isObject()) Program::currentCommand = arr[i + 1].toObject();
//            else Program::currentCommand = parentCommand;

//            arr.removeAt(i);
//            return arr;
//        }
//        if (obj["subCommands"].isArray())
//        {
//            QJsonArray subComArr = obj["subCommands"].toArray();
//            obj["subCommands"] = remove_com(subComArr, currentCommand, obj);
//        }
//        else
//        {
//            // have to remove subCommands due to its creation while checking its presence
//            obj.remove("subCommands");
//        }
//        arr[i] = obj;
//    }
//    return arr;
//}

//QJsonArray Program::change_com_param(QJsonArray arr, QJsonObject currentCommand, QString key, QVariant value)
//{
//    for (int i = 0; i < arr.count(); i++)
//    {
//        QJsonObject obj = arr[i].toObject();
//        if (obj["id"].toString() == currentCommand["id"].toString())
//        {
//            obj[key] = QJsonValue::fromVariant(value);
//            arr[i] = obj;
//            return arr;
//        }
//        if (obj["subCommands"].isArray())
//        {
//            QJsonArray subComArr = obj["subCommands"].toArray();
//            obj["subCommands"] = change_com_param(subComArr, currentCommand, key, value);
//        }
//        else
//        {
//            // have to remove subCommands due to its creation while checking its presence
//            obj.remove("subCommands");
//        }
//        arr[i] = obj;
//    }
//    return arr;
//}

//QJsonArray Program::com_unq_id(QJsonArray arr)
//{
//    for (int i = 0; i < arr.count(); i++)
//    {
//        QJsonObject obj = arr[i].toObject();
//        obj["id"] = get_id();
//        if (obj["subCommands"].isArray())
//        {
//            QJsonArray subComArr = obj["subCommands"].toArray();
//            obj["subCommands"] = com_unq_id(subComArr);
//        }
//        else
//        {
//            // have to remove subCommands due to its creation while checking its presence
//            obj.remove("subCommands");
//        }
//        arr[i] = obj;
//    }
//    return arr;
//}

//QJsonObject Program::get_default_tool()
//{
//    QJsonArray tcp = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
//    QJsonObject tool = {
//        {"id", "0"},
//        {"name", "default"},
//        {"tcp", tcp}
//    };
//    return tool;
//}

//QJsonObject Program::get_default_payload()
//{
//    QJsonObject payload = {
//        {"id", "0"},
//        {"name", "default"},
//        {"mass", 0.0},
//        {"comX", 0.0},
//        {"comY", 0.0},
//        {"comZ", 0.0}
//    };
//    return payload;
//}
