#include "robot.h"

Robot::Robot()
{
    rtd_sock = new QTcpSocket;
    connect(rtd_sock, SIGNAL(connected()), this, SIGNAL(rtd_sock_connected()));
    connect(rtd_sock, SIGNAL(readyRead()), this, SLOT(rtd_sock_ready_read()));
    connect(rtd_sock, SIGNAL(errorOccurred(QAbstractSocket::SocketError)), this, SIGNAL(rtd_sock_error(QAbstractSocket::SocketError)));
    connect(rtd_sock, SIGNAL(disconnected()), this, SIGNAL(rtd_sock_disconnected()));

    ctrl_sock = new QTcpSocket;
    connect(ctrl_sock, SIGNAL(connected()), this, SIGNAL(ctrl_sock_connected()));
    connect(ctrl_sock, SIGNAL(readyRead()), this, SLOT(ctrl_sock_ready_read()));
    connect(ctrl_sock, SIGNAL(errorOccurred(QAbstractSocket::SocketError)), this, SIGNAL(ctrl_sock_error(QAbstractSocket::SocketError)));
    connect(ctrl_sock, SIGNAL(disconnected()), this, SIGNAL(ctrl_sock_disconnected()));

    log_sock = new QTcpSocket;
    connect(log_sock, SIGNAL(connected()), this, SIGNAL(log_sock_connected()));
    connect(log_sock, SIGNAL(readyRead()), this, SLOT(log_sock_ready_read()));
    connect(log_sock, SIGNAL(errorOccurred(QAbstractSocket::SocketError)), this, SIGNAL(log_sock_error(QAbstractSocket::SocketError)));
    connect(log_sock, SIGNAL(disconnected()), this, SIGNAL(log_sock_disconnected()));

    jog_timer = new QTimer(this);
    jogging_timer = new QTimer(this);
    zg_timer = new QTimer(this);

    connect(jog_timer, SIGNAL(timeout()), this, SLOT(handle_jog()));
    connect(jogging_timer, SIGNAL(timeout()), this, SLOT(handle_jogging()));
    connect(zg_timer, SIGNAL(timeout()), this, SLOT(handle_zg()));
}

Robot::~Robot()
{
    delete rtd_sock;
    delete ctrl_sock;
    delete jog_timer;
    delete jogging_timer;
    delete zg_timer;
    delete log_sock;
}

void Robot::set_run(bool is_run)
{
    run = is_run;
}

void Robot::set_wait_di(int ind, bool val)
{
    wait_mode = DI;
    di_wait.ind = ind;
    di_wait.val = val;
}

void Robot::set_wait_ai(int ind, bool mt, double val)
{
    wait_mode = AI;
    ai_wait.ind = ind;
    ai_wait.mt = mt;
    ai_wait.val = val;
}

bool Robot::is_brks_rlsd()
{
    return rtd.state == CTRLR_STATE_RUN;
}

// ==================
// === RTD SOCKET ===
// ==================

void Robot::connect_rtd_sock()
{
    rtd_sock->connectToHost(HOST, RTD_PORT);
}

void Robot::rtd_sock_ready_read()
{

    int len = 0;

    while (true) {

        len = rtd_sock->read(((char *)&rtd_rcv_buf) + rtd_rcv_ptr, sizeof(rtd) - rtd_rcv_ptr);

        if(len <= 0) return;

        rtd_rcv_ptr += len;

        if(rtd_rcv_ptr == sizeof(rtd))
        {
            ctrlr_rt_data_t prev = rtd;

            memcpy(&rtd, rtd_rcv_buf, sizeof(rtd));

            compare_rtd(prev, rtd);

            rtd_rcv_ptr = 0;

            send_rtd();
        }

    }

}

void Robot::compare_rtd(ctrlr_rt_data_t prev, ctrlr_rt_data_t next)
{
    if (rtd_init)
    {
        if (prev.motion_mode == CTRLR_MM_ZG && next.motion_mode == CTRLR_MM_HOLD)
        {
            zg_timer->stop();
            zg_en = false;
        }
        if (prev.motion_mode == CTRLR_MM_JOG && next.motion_mode == CTRLR_MM_HOLD)
        {
            if (is_jogging)
            {
                jogging_timer->stop();
                is_jogging = false;
                jogging_interrupted = true;
                jogging_vec[0] = 0;
                jogging_vec[1] = 0;
                jogging_vec[2] = 0;
                jogging_vec[3] = 0;
                jogging_vec[4] = 0;
                jogging_vec[5] = 0;
            }
            else if (jog_axis != -1)
            {
                jog_timer->stop();
                jog_axis = -1;
                jog_value = 0;
            }
        }
        if (prev.wpi.buff_fill != next.wpi.buff_fill && next.wpi.buff_fill == 0 && run)
        {
            emit mtn_stp();
            if (next.motion_mode == CTRLR_MM_HOLD)
            {
                emit prog_intr();
            }
        }
        if (wait_mode == DI)
        {
            if (GET_BIT(next.io.dig_in, di_wait.ind) == di_wait.val)
            {
                wait_mode = NI;
                emit di_rcv();
            }
        }
        if (wait_mode == AI)
        {
            if (ai_wait.mt)
            {
                if (next.io.an_in_value[ai_wait.ind] > (next.io.an_in_curr_mode[ai_wait.ind] ? ai_wait.val * 1e-3 : ai_wait.val))
                {
                    wait_mode = NI;
                    emit ai_rcv();
                }
            }
            else
            {
                if (next.io.an_in_value[ai_wait.ind] < (next.io.an_in_curr_mode[ai_wait.ind] ? ai_wait.val * 1e-3 : ai_wait.val))
                {
                    wait_mode = NI;
                    emit ai_rcv();
                }
            }
        }
        if (!di_equal(prev, next))
        {
            QVariantList di;
            COPY_IO_TO_LIST(next.io.dig_in, &di, SIZE(next.io.dig_in));
            emit di_upd(di);
        }
        if (!ai_equal(prev, next))
        {
            QVariantList ai_v;
            QVariantList ai_m;
            COPY_VECTOR_TO_LIST(next.io.an_in_value, &ai_v, SIZE(next.io.an_in_value));
            COPY_VECTOR_TO_LIST(next.io.an_in_curr_mode, &ai_m, SIZE(next.io.an_in_curr_mode));
            emit ai_upd(ai_v, ai_m);
        }
        if (prev.wpi.buff_fill > next.wpi.buff_fill && run)
        {
            emit pnt_psd(next.wpi.buff_fill);
        }
        if (prev.wpi.cmd_cntr != next.wpi.cmd_cntr && next.wpi.cmd_cntr == cmd_cntr)
        {
            emit cmd_acc();
        }
        if (prev.state != CTRLR_STATE_RUN && next.state == CTRLR_STATE_RUN)
        {
            emit brks_rlsd();
        }
    }
    else
    {
        rtd_init = true;

        cmd_cntr = next.wpi.cmd_cntr;

        if (next.state == CTRLR_STATE_RUN)
        {
            emit brks_rlsd();
        }
    }
    emit io_upd(io_to_map(next));
}

void Robot::send_rtd()
{
    QVariantMap data;
    QVariantList position;
    QVariantList pose;
    QVariantList motorStatorTemp;
    QVariantList motorControllerTemp;
    QVariantList digitalInputs;
    QVariantList digitalOutputs;
    QVariantList analogInputs;
    QVariantList analogInputsMode;
    QVariantList analogOutputs;
    QVariantList analogOutputsMode;

    COPY_VECTOR_TO_LIST(rtd.act_x, &position, SIZE(rtd.act_x));
    COPY_VECTOR_TO_LIST(rtd.act_q, &pose, SIZE(rtd.act_q));
    COPY_VECTOR_TO_LIST(rtd.temp_m, &motorStatorTemp, SIZE(rtd.temp_m));
    COPY_VECTOR_TO_LIST(rtd.temp_e, &motorControllerTemp, SIZE(rtd.temp_e));

    COPY_IO_TO_LIST(rtd.io.dig_in, &digitalInputs, SIZE(rtd.io.dig_in));
    COPY_IO_TO_LIST(rtd.io.dig_out, &digitalOutputs, SIZE(rtd.io.dig_out));
    COPY_VECTOR_TO_LIST(rtd.io.an_in_value, &analogInputs, SIZE(rtd.io.an_in_value));
    COPY_VECTOR_TO_LIST(rtd.io.an_in_curr_mode, &analogInputsMode, SIZE(rtd.io.an_in_curr_mode));
    COPY_VECTOR_TO_LIST(rtd.io.an_out_value, &analogOutputs, SIZE(rtd.io.an_out_value));
    COPY_VECTOR_TO_LIST(rtd.io.an_out_curr_mode, &analogOutputsMode, SIZE(rtd.io.an_out_curr_mode));

    data.insert("state", rtd.state);
    data.insert("motionMode", rtd.motion_mode);
    data.insert("position", position);
    data.insert("pose", pose);
    data.insert("bufferFill", rtd.wpi.buff_fill);
    data.insert("commandsCounter", rtd.wpi.cmd_cntr);
    data.insert("motorStatorTemp", motorStatorTemp);
    data.insert("motorControllerTemp", motorControllerTemp);
    data.insert("current", rtd.arm_current);
    data.insert("voltage", rtd.arm_voltage);
    data.insert("digitalInputs", digitalInputs);
    data.insert("digitalOutputs", digitalOutputs);
    data.insert("analogInputs", analogInputs);
    data.insert("analogInputsMode", analogInputsMode);
    data.insert("analogOutputs", analogOutputs);
    data.insert("analogOutputsMode", analogOutputsMode);

    emit rtd_upd(data);
}

void Robot::disconnect_rtd_sock()
{
    rtd_sock->close();
}

// ===================
// === CTRL SOCKET ===
// ===================

void Robot::connect_ctrl_sock()
{
    ctrl_sock->connectToHost(HOST, CTRL_PORT);
}

void Robot::stop()
{
    ctrlr_coms_stop_t c;
    c.type = CTRLR_COMS_STOP;

    send_cmd(&c, sizeof(c));
}

void Robot::set_pwr_mode(ctrlr_coms_power_cmd_t cmd)
{
    ctrlr_coms_power_t c = {
        .type = CTRLR_COMS_POWER,
        .cmd = cmd,
    };

    send_cmd(&c, sizeof(c));
}

void Robot::spd_acc_scale(double scale_v, double scale_a) {
    spd_scale = scale_v;
    acc_scale = scale_a;

    ctrlr_coms_move_scale_t c = {
        .type = CTRLR_COMS_MOVE_SCALE,
        .scale_v = scale_v,
        .scale_a = scale_a,
    };

    send_cmd(&c, sizeof(c));
}

void Robot::add_wp_l(double x[6], double l[4], double rblend) {
    ctrlr_coms_move_add_wp_t c = {
        .type = CTRLR_COMS_MOVE_ADD_WP,
        .wp = {
            .type = MOVE_WP_TYPE_LINEAR_CART,
            .des_q = {0.0},
            .des_x = {0.0},
            .force = {0.0},
            .force_en = {0},
            .force_in_tcp = 0,
            .vmax_t = l[0],
            .vmax_r = l[1],
            .amax_t = l[2],
            .amax_r = l[3],
            .vmax_j = 0,
            .amax_j = 0,
            .rblend = rblend,
            .pseg = 0,
        }
    };

    COPY_VECTOR(x, c.wp.des_x, 6);

    cmd_cntr++;

    send_cmd(&c, sizeof(c));
}

void Robot::add_wp_j(double q[6], double l[2], double rblend) {
    ctrlr_coms_move_add_wp_t c = {
        .type = CTRLR_COMS_MOVE_ADD_WP,
        .wp = {
            .type = MOVE_WP_TYPE_JOINT,
            .des_q = {0.0},
            .des_x = {0.0},
            .force = {0.0},
            .force_en = {0},
            .force_in_tcp = 0,
            .vmax_t = 0,
            .vmax_r = 0,
            .amax_t = 0,
            .amax_r = 0,
            .vmax_j = l[0],
            .amax_j = l[1],
            .rblend = rblend,
            .pseg = 0,
        }
    };

    COPY_VECTOR(q, c.wp.des_q, 6);

    cmd_cntr++;

    send_cmd(&c, sizeof(c));
}

void Robot::run_wps() {
    if (run) emit pnt_psd(rtd.wpi.buff_fill);

    ctrlr_coms_move_run_t c = {
        .type = CTRLR_COMS_MOVE_RUN,
    };

    spd_acc_scale(spd_scale, acc_scale);
    send_cmd(&c, sizeof(c));
}

void Robot::start_jog(int axis, int value) {
    if (is_jogging) return;

    jog_axis = axis;
    jog_value = value;

    handle_jog();

    jog_timer->start(JOG_DELAY);
}

void Robot::stop_jog()
{
    if (is_jogging) return;

    jog_value = 0;

    jog_timer->stop();

    handle_jog();
}

void Robot::jogging(double x, double y, double z, double rx, double ry, double rz)
{
    if (jog_axis != -1) return;

    jogging_vec[0] = x;
    jogging_vec[1] = y;
    jogging_vec[2] = z;
    jogging_vec[3] = rx;
    jogging_vec[4] = ry;
    jogging_vec[5] = rz;

    if (!jogging_interrupted) handle_jogging();

    if (x == 0 && y == 0 && z == 0 && rx == 0 && ry == 0 && rz== 0)
    {
        // stop jogging
        jogging_timer->stop();
        is_jogging = false;
        jogging_interrupted = false;
    }
    else if (!is_jogging && !jogging_interrupted)
    {
        // handle start jogging
        is_jogging = true;
        jogging_timer->start(JOG_DELAY);
    }
}

void Robot::start_zg()
{
    zg_en = true;

    if (rtd.motion_mode == CTRLR_MM_JOG) stop();

    handle_zg();

    zg_timer->start(ZG_DELAY);
}

void Robot::stop_zg()
{
    zg_en = false;

    zg_timer->stop();

    handle_zg();
}

void Robot::align()
{
    double rpy[3] = {rtd.act_x[3], rtd.act_x[4], rtd.act_x[5]};
    double res[3] = {rtd.act_x[3], rtd.act_x[4], rtd.act_x[5]};
    double rot[3][3];
    double rott[3][3];
    double z[3];
    double n[3];
    double q[4];
    double rotq[4];
    double newq[4];
    double k[3] = {0, 0, 1};

    rpy_to_quat(rpy, q);
    rpy_to_r(rpy, rot);
    matrix_transpose_3x3(rot, rott);
    matrix_mult_vector_3x3(rott, k, z);
    vector_cross3(z, k, n);
    double norm = 1.0 / vector_norm(n, 3);
    vector_scale(n, norm, n, 3);

    double dot = vector_dot(z, k, 3);
    double angle = acos(dot);

    if(angle > M_PI / 4.0 && angle < 3.0 * M_PI / 4.0)
    {

        if (angle < M_PI / 2.0)
        {
            angle = M_PI / 2.0 - angle;
            double qr[4]= {
              cos(angle / 2.0), sin(angle / 2.0) * n[0],
              sin(angle / 2.0) * n[1], sin(angle / 2.0) * n[2]
              };
            vector_copy(qr, rotq, 4);
        }
        else
        {
            angle = M_PI / 2.0 - angle;
            double qr[4]= {
              cos(angle / 2.0), sin(angle / 2.0) * n[0],
              sin(angle / 2.0) * n[1], sin(angle / 2.0) * n[2]
              };
            vector_copy(qr, rotq, 4);
        }
    }
    else
    {
        if (angle < M_PI / 2.0)
        {
            double qr[4]= {
              cos(angle / 2.0), -sin(angle / 2.0) * n[0],
              -sin(angle / 2.0) * n[1], -sin(angle / 2.0) * n[2]
              };
            vector_copy(qr, rotq, 4);
        }
        else
        {
            angle = M_PI - angle;
            double qr[4]= {
              cos(angle / 2.0), sin(angle / 2.0) * n[0],
              sin(angle / 2.0) * n[1], sin(angle / 2.0) * n[2]
              };
            vector_copy(qr, rotq, 4);
        }
    }
    quat_mult(q, rotq, newq);
    quat_to_rpy(newq, res);
    double tar_x[6] = {rtd.act_x[0], rtd.act_x[1], rtd.act_x[2], res[0], res[1], res[2]};
    double l[4] = {0.25, 1.2, 1.047197, 1.396263};
    add_wp_l(tar_x, l, 0);
    run_wps();
}

void Robot::set_do(int index, bool value)
{
    ctrlr_coms_set_outputs_t c = {
        .type = CTRLR_COMS_SET_OUTPUTS,
        .dig_out_mask = {0},
        .dig_out = {0},
        .an_out_mask = {0},
        .an_out_curr_mode = {0},
        .an_out_value = {0.0},
    };
    int ind = index / 8, depth = index % 8;
    c.dig_out_mask[ind] = 1 << depth;
    c.dig_out[ind] = value << depth;

    send_cmd(&c, sizeof(c));
}

void Robot::set_ao(int index, bool currentMode, double value)
{
    value = currentMode ? std::max(0.0, std::min(value, 20.0)) : std::max(0.0, std::min(value, 10.0));

    ctrlr_coms_set_outputs_t c = {
        .type = CTRLR_COMS_SET_OUTPUTS,
        .dig_out_mask = {0},
        .dig_out = {0},
        .an_out_mask = {0},
        .an_out_curr_mode = {0},
        .an_out_value = {0.0},
    };
    c.an_out_mask[index] = 1;
    c.an_out_curr_mode[index] = currentMode;
    c.an_out_value[index] = currentMode ? value * 1e-3 : value;

    send_cmd(&c, sizeof(c));
}

void Robot::get_settings()
{
    ctrlr_coms_get_settings_t c = {
        .type = CTRLR_COMS_GET_SETTINGS,
    };

    send_cmd(&c, sizeof(c));

    await_settings();
}

uint8_t Robot::get_crdnt_sys()
{
    return settings.jog.in_tcp;
}

void Robot::set_crdnt_sys(bool in_tcp)
{
    ctrlr_coms_settings_t c = settings;
    c.type = CTRLR_COMS_SETTINGS;
    c.jog.in_tcp = in_tcp;

    send_cmd(&c, sizeof(c));

    get_settings();
}

void Robot::set_tcp(double tcp[])
{
    ctrlr_coms_settings_t c = settings;
    c.type = CTRLR_COMS_SETTINGS;
    c.tool.x = tcp[0];
    c.tool.y = tcp[1];
    c.tool.z = tcp[2];
    c.tool.roll = tcp[3];
    c.tool.pitch = tcp[4];
    c.tool.yaw = tcp[5];

    send_cmd(&c, sizeof(c));

    get_settings();
}

void Robot::set_payload(double mass, double com_x, double com_y, double com_z)
{
    ctrlr_coms_settings_t c = settings;
    c.type = CTRLR_COMS_SETTINGS;
    c.payload.mass = mass;
    c.payload.com_x = com_x;
    c.payload.com_y = com_y;
    c.payload.com_z = com_z;

    send_cmd(&c, sizeof(c));

    get_settings();
}

QString Robot::get_log_hist()
{
    return log_history;
}

bool Robot::send_cmd(void *payload, uint32_t size)
{
    bool returnValue = false;

    if(ctrl_sock->isOpen())
    {
      if(ctrl_sock->write((char *)&size, sizeof(size)) == sizeof(size))
      {
          returnValue = ctrl_sock->write((char *)payload, size) == size;
      }
    }

    if(!returnValue && ctrl_sock->isOpen())
    {
      disconnect_ctrl_sock();
    }

    return returnValue;
}

void Robot::parse_ctrl_data()
{
    ctrlr_coms_type_t t;

    memcpy(&t, ctrl_rcv_buf, sizeof(t));

    switch (t)
    {
        case CTRLR_COMS_GET_SETTINGS:
            memcpy(&settings, ctrl_rcv_buf, sizeof(ctrlr_coms_settings_t));
            emit settings_received();
            break;
        default:
            qDebug("unknown\n");
            break;
    }
}

void Robot::send_settings()
{
    QVariantMap settingsMap;
    QVariantMap payload;
    QVariantMap tool;
    QVariantMap jogMap;
    QVariantMap fc;

    payload.insert("mass", settings.payload.mass);
    payload.insert("comX", settings.payload.com_x);
    payload.insert("comY", settings.payload.com_y);
    payload.insert("comZ", settings.payload.com_z);

    settingsMap.insert("payload", payload);

    tool.insert("x", settings.tool.x);
    tool.insert("y", settings.tool.y);
    tool.insert("z", settings.tool.z);
    tool.insert("roll", settings.tool.roll);
    tool.insert("pitch", settings.tool.pitch);
    tool.insert("yaw", settings.tool.yaw);

    settingsMap.insert("tool", tool);

    QVariantList force_en;
    QVariantList force;
    QVariantList spd_max_jog;
    QVariantList accel;
    QVariantList decel;

    COPY_VECTOR_TO_LIST(settings.jog.force_en, &force_en, 6);
    COPY_VECTOR_TO_LIST(settings.jog.force, &force, 6);
    COPY_VECTOR_TO_LIST(settings.jog.spd_max, &spd_max_jog, 6);
    COPY_VECTOR_TO_LIST(settings.jog.accel, &accel, 6);
    COPY_VECTOR_TO_LIST(settings.jog.decel, &decel, 6);

    jogMap.insert("inTCP", settings.jog.in_tcp);
    jogMap.insert("forceEnabled", force_en);
    jogMap.insert("force", force);
    jogMap.insert("maxSpeed", spd_max_jog);
    jogMap.insert("acceleration", accel);
    jogMap.insert("deceleration", decel);

    settingsMap.insert("jog", jogMap);

    QVariantList damp;
    QVariantList virt_mass;
    QVariantList spd_max_fc;

    COPY_VECTOR_TO_LIST(settings.fc.damp, &damp, 6);
    COPY_VECTOR_TO_LIST(settings.fc.virt_mass, &virt_mass, 6);
    COPY_VECTOR_TO_LIST(settings.fc.spd_max, &spd_max_fc, 6);

    fc.insert("damping", damp);
    fc.insert("virtualMass", virt_mass);
    fc.insert("maxSpeed", spd_max_fc);

    settingsMap.insert("forceControl", fc);

    emit settings_upd(settingsMap);
}

void Robot::await_settings(int msec)
{
    QEventLoop loop;
    connect(this, SIGNAL(settings_received()), &loop, SLOT(quit()));
    QTimer::singleShot(msec, Qt::PreciseTimer, &loop, SLOT(quit()));
    loop.exec();
}

void Robot::handle_jog()
{
    if (jog_axis == -1)
    {
        if (jog_timer->isActive()) jog_timer->stop();

        return;
    }

    ctrlr_coms_jog_t c = {
        .type = CTRLR_COMS_JOG,
        .mode = CTRLR_COMS_JOG_MODE_VELOCITY,
        .force_en = {0},
        .force_max = {0.0},
        .force_const = {0.0},
        .stiff = {0.0},
        .var = {0.0},
    };

    double var[6] = {0.0};

    var[jog_axis] = jog_value * spd_scale;

    COPY_VECTOR(var, c.var, 6);

    if (!send_cmd(&c, sizeof(c)) || jog_value == 0)
    {
        jog_timer->stop();

        jog_axis = -1;
    }
}

void Robot::handle_jogging()
{
    if (!is_jogging)
    {
        if (jogging_timer->isActive()) jogging_timer->stop();

        return;
    }

    ctrlr_coms_jog_t c = {
        .type = CTRLR_COMS_JOG,
        .mode = CTRLR_COMS_JOG_MODE_VELOCITY,
        .force_en = {0},
        .force_max = {0.0},
        .force_const = {0.0},
        .stiff = {0.0},
        .var = {
            jogging_vec[0] * spd_scale,
            jogging_vec[1] * spd_scale,
            jogging_vec[2] * spd_scale,
            jogging_vec[3] * spd_scale,
            jogging_vec[4] * spd_scale,
            jogging_vec[5] * spd_scale,
        },
    };

    if (!send_cmd(&c, sizeof(c)) || !is_jogging)
    {
        jogging_timer->stop();
    }
}

void Robot::handle_zg()
{
    if (!zg_en && zg_timer->isActive()) zg_timer->stop();

    ctrlr_coms_zg_t c = {
        .type = CTRLR_COMS_ZG,
        .en = zg_en,
    };

    if (!send_cmd(&c, sizeof(c)) || !zg_en)
    {
        zg_timer->stop();

        zg_en = false;
    }
}

void Robot::ctrl_sock_ready_read()
{
    int l;

    while(true)
    {
        switch(ctrl_rcv_state)
        {
            case RCV_IDLE:
                ctrl_rcv_ptr = 0;
                ctrl_rcv_state = RCV_SZ;
                break;
            case RCV_SZ:
                l = ctrl_sock->read(((char *)&ctrl_rcv_sz) + ctrl_rcv_ptr, sizeof(ctrl_rcv_sz) - ctrl_rcv_ptr);
                if(l <= 0) return;
                ctrl_rcv_ptr += l;
                if(ctrl_rcv_ptr == sizeof(ctrl_rcv_sz))
                {
                    ctrl_rcv_state = RCV_PLOAD;
                    ctrl_rcv_ptr = 0;
                }
                break;
            case RCV_PLOAD:
                l = ctrl_sock->read(((char *)&ctrl_rcv_buf) + ctrl_rcv_ptr, ctrl_rcv_sz - ctrl_rcv_ptr);
                if(l <= 0) return;
                ctrl_rcv_ptr += l;
                if(ctrl_rcv_ptr == (int)ctrl_rcv_sz)
                {
                    ctrl_rcv_state = RCV_IDLE;
                    parse_ctrl_data();
                }
                break;
        }
    }
}

void Robot::disconnect_ctrl_sock()
{
    ctrl_sock->close();
}

// ==================
// === LOG SOCKET ===
// ==================

void Robot::connect_log_sock()
{
    log_sock->connectToHost(HOST, LOG_PORT);
}

void Robot::log_sock_ready_read()
{

    int len = 0;
    char buf[4096];

    len = log_sock->read(buf, sizeof(buf));

    if(len <= 0) return;

    if (log_history.isEmpty())
    {
        log_history = QString::fromLocal8Bit(buf, len);
    }
    else
    {
        log_history.append(QString::fromLocal8Bit(buf, len));
    }

    emit log_upd(log_history);

}

void Robot::disconnect_log_sock()
{
    log_sock->close();
}

bool di_equal(ctrlr_rt_data_t prev, ctrlr_rt_data_t next)
{
    for(int i = 0; i < CTRLR_MAX_DIG_IN; i++) if (prev.io.dig_in[i] != next.io.dig_in[i]) return false;
    return true;
}

bool ai_equal(ctrlr_rt_data_t prev, ctrlr_rt_data_t next)
{
    for(int i = 0; i < CTRLR_MAX_AN_IN; i++) if (prev.io.an_in_value[i] != next.io.an_in_value[i]) return false;
    return true;
}

QVariantMap io_to_map(ctrlr_rt_data_t rtd)
{
    QVariantList dI, dO, aI, aO;
    COPY_IO_TO_LIST(rtd.io.dig_in, &dI, SIZE(rtd.io.dig_in));
    COPY_IO_TO_LIST(rtd.io.dig_out, &dO, SIZE(rtd.io.dig_out));
    COPY_VECTOR_TO_LIST(rtd.io.an_in_value, &aI, 4);
    COPY_VECTOR_TO_LIST(rtd.io.an_out_value, &aO, 4);
    QVariantMap result = {
        {"di", dI},
        {"do", dO},
        {"ai", aI},
        {"ao", aO},
    };
    return result;
}
