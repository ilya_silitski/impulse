#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QThread>
#include <QTimer>
#include <QProcess>
#include <qqml.h>
#include "program.h"
#include "utils.h"
#include "exr_proto.h"
#include "tcs.h"
#include "chk.h"

#define EMU_DATA_FREQ 60.0
#define GEN_DATA_FREQ 10.0

class Backend : public QObject
{

    Q_OBJECT
    QML_ELEMENT

public:
    Backend();
    ~Backend();

    enum State {
        Idle,
        Off,
        Stby,
        On,
        Run,
        Reserved0,
        Failure,
        ForceExit
    };
    Q_ENUM(State)

    enum MotionMode {
        Hold,
        ZeroGravity,
        Jog,
        Move
    };
    Q_ENUM(MotionMode)

    Q_INVOKABLE void startup();

    Q_INVOKABLE void stop();
    Q_INVOKABLE void turnRobotOff();
    Q_INVOKABLE void turnRobotOn();
    Q_INVOKABLE void releaseBrakes();
    Q_INVOKABLE void changeSpeed(int speed);
    Q_INVOKABLE void setCoordinateSystem(bool inTcp);
    Q_INVOKABLE void setTool(QString toolId);
    Q_INVOKABLE void setPayload(QString payloadId);
    Q_INVOKABLE void goToPose(QVariantList target);
    Q_INVOKABLE void goToPosition(QVariantList target);
    Q_INVOKABLE void startJogging(QString axis, bool direction);
    Q_INVOKABLE void stopJogging();
    Q_INVOKABLE void jogging(double x, double y, double z, double rx, double ry, double rz);
    Q_INVOKABLE void setZeroGravity(bool open);
    Q_INVOKABLE void startZeroGravity();
    Q_INVOKABLE void stopZeroGravity();
    Q_INVOKABLE void align();
    Q_INVOKABLE void setDigitalOutput(int index, bool value);
    Q_INVOKABLE void setAnalogOutput(int index, bool mode, double value);

    Q_INVOKABLE void runProgram();
    Q_INVOKABLE void pauseProgram();
    Q_INVOKABLE void stepProgram();
    Q_INVOKABLE void continueProgram();
    Q_INVOKABLE void stopProgram();

    Q_INVOKABLE void openFile(QString path);
    Q_INVOKABLE void createNewFile(QString path);
    Q_INVOKABLE void saveFile(QString path, QString content);
    Q_INVOKABLE void saveFileAs(QString newPath, QString content);
    Q_INVOKABLE void saveAs(QString oldPath, QString newPath);
    Q_INVOKABLE void renameFile(QString oldPath, QString newPath);
    Q_INVOKABLE void deleteFile(QString path);

    Q_INVOKABLE void createNewDir(QString path);
    Q_INVOKABLE void renameDir(QString oldPath, QString newPath);
    Q_INVOKABLE void deleteDir(QString path);

private slots:
    void chunk_received(tcs_conn_state_t conn_state, tcs_type_t type, tcs_size_t payload_size, void *payload);

    void emu_data_upd();
    void gen_data_upd();

signals:
    void emulatorDataUpdated(QVariantMap data);
    void generalDataUpdated(QVariantMap data);

    void logsUpdated(QString logHistory);

    void programRunning();
    void programPaused();
    void programFinished(int s, int v);
    void programStopped();
    void showPopup(int popupType, QString message, QString buttonText, bool playSound, QString soundName);

private:

    QVariantMap rtd;
    QTimer *emu_data_timer;
    QTimer *gen_data_timer;
    QString logs_hist;

    Program *programController;

    TCS *tcs;

    bool zeroGravityOpen = false;
    qint64 pauseStart = 0;

    void runProgramOnBreaksReleased();

    void sendCurrentSetup();
    void sendCurrentTool();
    void sendCurrentPayload();

    void sendProgramsList(QString path);
    void sendCurrentProgram();
    void sendCurrentProgramName();
    void sendCurrentCommand();
    void sendBufferEmpty();

    void sendCoordinateSystem();
    void sendTool();
    void sendPayload();
    void prog_running();
    void prog_paused();
    void prog_finished(int32_t s, int32_t v);
    void prog_stopped();
};

#endif // BACKEND_H
