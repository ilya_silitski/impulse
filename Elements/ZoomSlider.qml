import QtQuick
import QtQuick.Controls

Slider {
    id: zoomSlider
    height: length
    width: 48
    orientation: Qt.Vertical
    stepSize: 0.01
    value: 0.5

    property int length: 300
    property int handleRadius: 36
    property int strokeWidth: 8

    background: Rectangle {
        anchors.fill: parent
        color: "#ffffff"
        opacity: 0.65

        Rectangle {
            x: zoomSlider.leftPadding + zoomSlider.availableWidth / 2 - width / 2
            y: zoomSlider.topPadding + zoomSlider.handleRadius / 2 - zoomSlider.strokeWidth / 2
            height: zoomSlider.availableHeight - zoomSlider.handleRadius + zoomSlider.strokeWidth
            width: zoomSlider.strokeWidth
            radius: zoomSlider.strokeWidth / 2
            color: "#e0e0e0"
        }
    }

    handle: Rectangle {
        x: zoomSlider.leftPadding + zoomSlider.availableWidth / 2 - width / 2
        y: zoomSlider.topPadding + zoomSlider.visualPosition * (zoomSlider.availableHeight - height)
        width: zoomSlider.handleRadius
        height: zoomSlider.handleRadius
        radius: zoomSlider.handleRadius / 2
        color: zoomSlider.pressed ? "#f0f0f0" : "#f6f6f6"
        border.color: "#bdbebf"
    }

    onMoved: rootEntity.zoom(zoomSlider.value);
}
