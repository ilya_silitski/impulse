import QtQuick
import QtQuick.Controls

CheckBox {
    id: button
    text: "CheckButton"
    height: 36
    padding: 6

    enum Type {
        Checkbox,
        Radio
    }

    property bool active: false
    property int type: CheckButton.Checkbox

    readonly property color textColor: {
        if (!button.enabled) return "#cccccc"
        return button.down ? Qt.darker("#555555", 1.15) : "#555555"
    }

    readonly property color mainColor: {
        if (!button.enabled) return "#cccccc"
        if (button.active) {
            return button.down ? Qt.darker("#4682b4", 1.15) : "#4682b4"
        } else {
            return button.down ? Qt.darker("#888888", 1.15) : "#888888"
        }
    }

    indicator: Rectangle {
        height: parent.availableHeight
        width: height
        x: button.leftPadding
        y: (parent.height - height) / 2
        radius: type === CheckButton.Checkbox ? 6 : height / 2
        border.color: mainColor
        border.width: 2
        color: "transparent"

        Rectangle {
            height: parent.height - 12
            width: height
            x: 6
            y: 6
            radius: type === CheckButton.Checkbox ? 3 : height / 2
            color: mainColor
            visible: button.active
        }
    }

    contentItem: Text {
        text: button.text
        font: button.font
        color: textColor
        verticalAlignment: Text.AlignVCenter
        leftPadding: button.indicator.width + button.spacing
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:3}
}
##^##*/
