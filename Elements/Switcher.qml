import QtQuick
import QtQuick.Controls

Switch {
    id: switcher
    text: "Text"
    height: 36
    font.pixelSize: 18

    property color checkedColor: "#4682b4"
    property color textColor: "#555555"

    indicator: Rectangle {
        width: parent.height * 2
        height: parent.height
        radius: height / 2
        color: switcher.checked ? switcher.checkedColor : "#ffffff"
        border.color: switcher.checked ? switcher.checkedColor : "#e8e8e8"

        Rectangle {
            x: switcher.checked ? parent.width - width : 0
            width: switcher.height
            height: switcher.height
            radius: height / 2
            color: switcher.down ? "#d9d9d9" : "#e8e8e8"
            border.color: "#cccccc"

            Behavior on x { NumberAnimation { duration: 100 } }
        }
    }

    contentItem: Text {
        text: switcher.text
        font: switcher.font
        color: switcher.textColor
        verticalAlignment: Text.AlignVCenter
        leftPadding: switcher.indicator.width + switcher.spacing
    }
}
