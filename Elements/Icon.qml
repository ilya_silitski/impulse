import QtQuick
import QtQuick.Controls

Button {
    property string source
    property color color: "#000000"
    property int size: 64

    width: size; height: size
    padding: 0
    enabled: false

    icon.source: source
    icon.width: size
    icon.height: size
    icon.color: color

    background: Rectangle {
        anchors.fill: parent
        color: "transparent"
        border.width: 0
    }
}
