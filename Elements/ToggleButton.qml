import QtQuick
import QtQuick.Controls

PushButton {
    id: button
    checkable: false
    property bool active: false

    type: button.active ? PushButton.Primary : PushButton.Default
}
