import QtQuick
import QtQuick.Controls

Frame {
    property color color: "#e8e8e8"

    background: Rectangle {
        border.width: 0
        color: parent.color
    }
}
