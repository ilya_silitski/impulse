import QtQuick
import QtQuick.Controls
import "../Components"

Input {
    id: input
    active: numpad.visible
    onClicked: activate()

    function activate() {
        numpad.setDefaultValue(input.value.toFixed(precision) || "ERROR");
        numpad.open();
    }

    enum Placement {
        Left,
        Right
    }

    property int numpadPlacement: InputNumber.Right

    signal save(var result)

    Numpad {
        id: numpad
        x: numpadPlacement === InputNumber.Right ? input.width + 12 : -(numpad.width + 12)
        y: input.height / 2 - numpad.height / 2
        onResult: input.save(result);
    }
}
