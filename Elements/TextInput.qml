import QtQuick
import QtQuick.Controls

TextField {
    id: input
    height: 36
    placeholderText: "Enter text..."
    padding: 6
    leftPadding: label === "" ? 12 : labelWidth + 2
    readOnly: true

    property string label: ""
    property int labelWidth: 28
    property bool active: false

    signal clicked

    background: Rectangle {
        anchors.fill: parent
        color: "#ffffff"
        border.color: active ? "#40a9ff" : "#d9d9d9"
        border.width: 1
        radius: parent.height / 2

        Text {
            width: labelWidth
            text: input.label
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: input.font.pixelSize
            font.weight: Font.Medium
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: input.clicked();
    }
}
