import QtQuick
import QtQuick.Controls

Pane {
    id: alert
    padding: 12
    width: 480

    enum Type {
        Info,
        Warning,
        Error,
        Success
    }

    property int type: Alert.Type.Info
    property bool showIcon: false
    property string message: ""
    property string description: ""

    background: Rectangle {
        anchors.fill: parent
        radius: 12
        color: {
            switch (alert.type) {
            case Alert.Type.Info: return "#e6f7ff";
            case Alert.Type.Warning: return "#fffbe6";
            case Alert.Type.Error: return "#fff1f0";
            case Alert.Type.Success: return "#f6ffed";
            default: return "#e6f7ff";
            }
        }
        border.color: {
            switch (alert.type) {
            case Alert.Type.Info: return "#91d5ff";
            case Alert.Type.Warning: return "#ffe58f";
            case Alert.Type.Error: return "#ffa39e";
            case Alert.Type.Success: return "#b7eb8f";
            default: return "#91d5ff";
            }
        }
    }

    Row {
        spacing: 12
        anchors.left: parent.left
        anchors.right: parent.right

        Component {
            id: iconItem

            Icon {
                height: alert.description ? 36 : 24
                width: height
                source: {
                    switch (alert.type) {
                    case Alert.Type.Info: return "qrc:///img/info";
                    case Alert.Type.Warning: return "qrc:///img/warning";
                    case Alert.Type.Error: return "qrc:///img/error";
                    case Alert.Type.Success: return "qrc:///img/success";
                    default: return "qrc:///img/info";
                    }
                }

                color: {
                    switch (alert.type) {
                    case Alert.Type.Info: return "#1890ff";
                    case Alert.Type.Warning: return "#faad14";
                    case Alert.Type.Error: return "#f5222d";
                    case Alert.Type.Success: return "#52c41a";
                    default: return "#1890ff";
                    }
                }
            }
        }

        Loader {
            sourceComponent: if (alert.showIcon) iconItem
        }

        Column {
            width: alert.showIcon ? (parent.width - (alert.description ? 36 : 24) - parent.spacing) : parent.width

            Text {
                height: alert.description ? 36 : 24
                width: parent.width
                elide: Text.ElideRight
                color: "#333333"
                font.pixelSize: 18
                verticalAlignment: Text.AlignVCenter
                text: alert.message
            }

            Component {
                id: descriptionItem

                Text {
                    width: parent.width
                    color: "#555555"
                    font.pixelSize: 14
                    text: alert.description
                    wrapMode: Text.WordWrap
                }
            }

            Loader {
                width: parent.width
                sourceComponent: if (alert.description) descriptionItem
            }
        }
    }
}
