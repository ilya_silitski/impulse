import QtQuick
import QtQuick.Controls

ProgressBar {
    id: progressBar
    height: 24
    width: 240

    enum LineCap {
        Round,
        Square
    }

    property int linecap: Progress.Round
    property string prefix
    property string suffix

    background: Rectangle {
        anchors.verticalCenter: progressBar.verticalCenter
        width: progressBar.width
        height: progressBar.height / 2
        radius: height / 2
        color: "#e8e8e8"
    }

    contentItem: Item {
        width: progressBar.width
        height: progressBar.height

        Rectangle {
            anchors.verticalCenter: parent.verticalCenter
            width: progressBar.visualPosition * parent.width
            height: parent.height / 2
            radius: progressBar.linecap === Progress.Round ? (height / 2) : 0
            color: "#4682b4"
        }
    }

    Text {
        anchors.left: parent.left
        anchors.top: parent.bottom
        text: prefix
        font.pixelSize: 10
        height: 12
        width: 24
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    Text {
        anchors.right: parent.right
        anchors.top: parent.bottom
        text: suffix
        font.pixelSize: 10
        height: 12
        width: 24
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
}
