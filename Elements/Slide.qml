import QtQuick
import QtQuick.Controls

Slider {
    id: slider
    value: 1
    height: 24
    width: 240
    padding: 0

    property string prefix
    property string suffix

    background: Rectangle {
        x: slider.leftPadding + slider.height / 4
        y: slider.topPadding + (slider.availableHeight - height) / 2
        width: slider.availableWidth - slider.height / 2
        height: slider.height / 2
        radius: height / 2
        color: "#e8e8e8"

        Rectangle {
            width: slider.visualPosition * parent.width
            height: parent.height
            color: slider.enabled ? "#4682b4" : "#cccccc"
            radius: parent.radius
        }
    }

    handle: Rectangle {
        x: slider.leftPadding + slider.visualPosition * (slider.availableWidth - width)
        y: slider.topPadding + (slider.availableHeight - height) / 2
        height: slider.height
        width: slider.height
        radius: height / 2
        color: slider.pressed ? "#d9d9d9" : "#e8e8e8"
        border.color: "#d9d9d9"
    }

    Text {
        anchors.left: parent.left
        anchors.top: parent.bottom
        text: prefix
        font.pixelSize: 10
        height: 12
        width: 24
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    Text {
        anchors.right: parent.right
        anchors.top: parent.bottom
        text: suffix
        font.pixelSize: 10
        height: 12
        width: 24
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
}
