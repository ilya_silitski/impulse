import QtQuick
import QtQuick.Controls

TextField {
    id: input
    height: 36
    horizontalAlignment: Text.AlignRight
    placeholderText: "N/A"
    padding: 6
    text: value.toFixed(precision) + suffix
    leftPadding: labelWidth + 2
    readOnly: true

    property string label
    property int labelWidth: 28
    property real value
    property string suffix
    property bool editable: true
    property bool active: false
    property int precision: 1
    property bool labelToLeft: false

    signal clicked

    background: Rectangle {
        anchors.fill: parent
        color: input.enabled ? "#ffffff" : "#f8f8f8"
        border.color: active ? "#40a9ff" : "#e8e8e8"
        border.width: editable ? 1 : 0
        radius: parent.height / 2

        Text {
            width: labelWidth
            text: input.label
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.leftMargin: input.labelToLeft ? 12 : 0
            horizontalAlignment: input.labelToLeft ? Text.AlignLeft : Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            color: input.color
            font.pixelSize: input.font.pixelSize
            font.weight: Font.Medium
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: if(editable) input.clicked()
    }
}
