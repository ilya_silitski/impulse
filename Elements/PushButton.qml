import QtQuick
import QtQuick.Controls

Button {
    id: button

    height: {
        switch (button.size) {
        case PushButton.Size.Small: return 24;
        case PushButton.Size.Regular: return 36;
        case PushButton.Size.Large: return 48;
        }
    }
    width: button.shape === PushButton.Shape.Circle ? button.height : button.contentItem.implicitWidth + button.leftPadding + rightPadding

    leftPadding: button.shape === PushButton.Shape.Circle ? 0 : 12
    topPadding: button.shape === PushButton.Shape.Circle ? 0 : 6
    rightPadding: button.shape === PushButton.Shape.Circle ? 0 : 12
    bottomPadding: button.shape === PushButton.Shape.Circle ? 0 : 6


    font.pixelSize: {
        switch (button.size) {
        case PushButton.Size.Small: return 12;
        case PushButton.Size.Regular: return 14;
        case PushButton.Size.Large: return 18;
        }
    }

    display: AbstractButton.TextOnly

    enum Type {
        Default,
        Primary,
        Success,
        Danger,
        Warning
    }

    enum Size {
        Small,
        Regular,
        Large
    }

    enum Shape {
        Round,
        Circle
    }

    property int size: PushButton.Size.Regular
    property int type: PushButton.Type.Default
    property int shape: PushButton.Shape.Round
    property bool outline: false
    property string iconSource

    readonly property color textColor: {
        if (!button.enabled) return "#bfbfbf";
        switch(button.type) {
        case PushButton.Type.Default: return button.outline ? "555555" : "#555555";
        case PushButton.Type.Primary: return button.outline ? "#4682b4" : "#ffffff";
        case PushButton.Type.Success: return button.outline ? "#52c41a" : "#ffffff";
        case PushButton.Type.Danger: return button.outline ? "#ff3333" : "#ffffff";
        case PushButton.Type.Warning: return button.outline ? "#ffc107" : "#555555";
        }
    }

    readonly property color backgroundColor: {
        if (button.outline) return "transparent";
        if (!button.enabled) return "#f5f5f5";
        let backColor = "#000000";
        switch(button.type) {
        case PushButton.Type.Default: backColor = "#ffffff"; break;
        case PushButton.Type.Warning: backColor = "#ffc107"; break;
        case PushButton.Type.Primary: backColor = "#4682b4"; break;
        case PushButton.Type.Success: backColor = "#52c41a"; break;
        case PushButton.Type.Danger: backColor = "#ff3333"; break;
        }
        return button.down ? Qt.darker(backColor, 1.15) : backColor;
    }

    readonly property color borderColor: {
        if (!button.enabled) return "#e8e8e8";
        if (button.type === PushButton.Type.Default) return "#e8e8e8";
        if (button.outline) {
            switch(button.type) {
            case PushButton.Type.Primary: return "#4682b4";
            case PushButton.Type.Success: return "#52c41a";
            case PushButton.Type.Danger: return "#ff3333";
            case PushButton.Type.Warning: return "#ffc107";
            }
        }
        return "transparent";
    }

    Component {
        id: textItem
        Text {
            text: button.text
            font: button.font
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: button.textColor
        }
    }

    Component {
        id: iconItem
        Icon {
            source: button.iconSource
            color: button.textColor
            size: button.height / 2
        }
    }

    Component {
        id: iconTextItem
        Row {
            spacing: 6
            Loader { sourceComponent: iconItem }
            Loader { sourceComponent: textItem }
        }
    }

    contentItem: Loader {
        sourceComponent: {
            switch(button.display) {
            case AbstractButton.IconOnly: return iconItem;
            case AbstractButton.TextOnly: return textItem;
            case AbstractButton.TextBesideIcon: return iconTextItem;
            }
        }
    }

    background: Rectangle {
        id: backgroundRect
        height: button.height
        radius: button.height / 2
        color: button.backgroundColor
        border.width: 1
        border.color: button.borderColor
    }
}
