import QtQuick

Item {
    id: button
    height: outerRadius
    width: outerRadius
    transformOrigin: Item.Bottom
    rotation: placement * 90

    property string text: "X+"
    property int outerRadius: 120
    property int innerRadius: 60
    property int placement: SectorJoggingButton.Placement.Top
    property color color: "#ff0000"

    signal pressed
    signal released

    enum Placement {
        Top,
        Right,
        Bottom,
        Left
    }

    onEnabledChanged: canvas.requestPaint();

    Canvas {
        id: canvas
        height: button.outerRadius
        width: button.outerRadius
        transformOrigin: Item.BottomRight
        anchors.right: parent.horizontalCenter
        anchors.bottom: parent.bottom
        rotation: 45
        opacity: mouseArea.pushed ? 0.85 : 0.65

        Behavior on opacity {
            PropertyAnimation { duration: 100 }
        }

        onPaint: {
            const ctx = canvas.getContext("2d");
            ctx.beginPath();
            ctx.moveTo(button.outerRadius - button.innerRadius, button.outerRadius);
            ctx.lineTo(0, button.outerRadius);
            ctx.arc(button.outerRadius, button.outerRadius, button.outerRadius, Math.PI, -Math.PI / 2, false);
            ctx.lineTo(button.outerRadius, button.outerRadius - button.innerRadius);
            ctx.arc(button.outerRadius, button.outerRadius, button.innerRadius, -Math.PI / 2, Math.PI, true);
            ctx.fillStyle = button.enabled ? color : "#cccccc";
            ctx.fill();
            ctx.strokeStyle = "#ffffff";
            ctx.lineWidth = 2;
            ctx.stroke();
        }

        MouseArea {
            id: mouseArea
            enabled: button.enabled
            anchors.fill: parent
            property bool pushed: false
            function within() {
                const x0 = canvas.width;
                const y0 = canvas.height;
                const x = mouseX;
                const y = mouseY;
                const distance = Math.sqrt((x0 - x) ** 2 + (y0 - y) ** 2);
                return distance > button.innerRadius && distance < button.outerRadius && x < x0 && y < y0;
            }
            function handlePressed() {
                button.pressed();
                pushed = true;
            }
            function handleReleased() {
                button.released();
                pushed = false;
            }
            onPressed: if (within()) handlePressed();
            onReleased: if (within()) handleReleased();
            onCanceled: if (within()) handleReleased();
            onPositionChanged: if (pushed && !within()) handleReleased();
        }
    }

    Text {
        anchors.fill: parent
        anchors.bottomMargin: innerRadius
        rotation: -button.placement * 90
        text: button.text
        font.pixelSize: 24
        font.weight: Font.DemiBold
        color: "#ffffff"
        height: 36
        width: 60
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
}


