import QtQuick

Item {
    id: button
    height: radius
    width: radius * 2
    transformOrigin: Item.Bottom
    rotation: placement * 90

    property string text: "Z+"
    property int radius: 60
    property int placement: SemicircleJoggingButton.Placement.Top
    property color color: "#0000ff"

    signal pressed
    signal released

    enum Placement {
        Top,
        Right,
        Bottom,
        Left
    }

    onEnabledChanged: canvas.requestPaint();

    Canvas {
        id: canvas
        anchors.fill: parent
        opacity: mouseArea.pushed ? 0.85 : 0.65

        Behavior on opacity {
            PropertyAnimation { duration: 100 }
        }

        onPaint: {
            const ctx = canvas.getContext("2d");
            ctx.beginPath();
            ctx.moveTo(0, button.radius);
            ctx.arc(button.radius, button.radius, button.radius, Math.PI, 0, false);
            ctx.lineTo(0, button.radius);
            ctx.fillStyle = button.enabled ? color : "#cccccc";
            ctx.fill();
            ctx.strokeStyle = "#ffffff";
            ctx.lineWidth = 2;
            ctx.stroke();
        }

        MouseArea {
            id: mouseArea
            enabled: button.enabled
            anchors.fill: parent
            property bool pushed: false
            function within() {
                const x0 = button.radius;
                const y0 = button.radius;
                const x = mouseX;
                const y = mouseY;
                const distance = Math.sqrt((x0 - x) ** 2 + (y0 - y) ** 2);
                return distance < button.radius && y < y0;
            }
            function handlePressed() {
                button.pressed();
                pushed = true;
            }
            function handleReleased() {
                button.released();
                pushed = false;
            }
            onPressed: if (within()) handlePressed();
            onReleased: if (within()) handleReleased();
            onCanceled: if (within()) handleReleased();
            onPositionChanged: if (pushed && !within()) handleReleased();
        }
    }

    Text {
        anchors.fill: parent
        text: button.text
        rotation: -(button.placement * 90)
        font.pixelSize: 24
        font.weight: Font.DemiBold
        color: "#ffffff"
        height: 36
        width: 60
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
}
