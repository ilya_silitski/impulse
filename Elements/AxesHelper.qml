import QtQuick
import QtQuick3D
import QtQuick3D.Helpers

Node {
    id: axesNode

    property real lineWidth: 1
    property real lineLength: 100
    property real arrowWidth: 7.5
    property real arrowLength: 15
    property bool showArrow: true
    property bool showLine: true
    property alias enableXYGrid: defaultHelper.enableXYGrid
    property alias enableXZGrid: defaultHelper.enableXZGrid
    property alias enableYZGrid: defaultHelper.enableYZGrid

    AxisHelper {
        id: defaultHelper
        enableAxisLines: false

        enableXYGrid: false
        enableXZGrid: false
        enableYZGrid: false

        scale: Qt.vector3d(0.1, 0.1, 0.1)
    }

    DefaultMaterial {
        id: axisXMaterial
        diffuseColor: "#ff0000"
    }

    Model {
        id: axisXLine

        visible: showLine

        source: "#Cube"

        materials: axisXMaterial

        position.x: ((lineLength / 2) - (lineWidth / 2))
        scale.x: (lineLength / 100)
        scale.y: (lineWidth / 100)
        scale.z: (lineWidth / 100)
    }

    Model {
        id: axisXArrow

        visible: showArrow

        source: "#Cone"

        materials: axisXMaterial

        position.x: ((lineLength) - (lineWidth / 2))
        scale.x: (lineWidth / 100 * arrowWidth)
        scale.y: (lineWidth / 100 * arrowLength)
        scale.z: (lineWidth / 100 * arrowWidth)
        eulerRotation.z: -90
    }

    DefaultMaterial {
        id: axisYMaterial
        diffuseColor: "#00ff00"
    }

    Model {
        id: axisYLine

        visible: showLine

        source: "#Cube"

        materials: axisYMaterial

        position.y: ((lineLength / 2) - (lineWidth / 2))
        scale.x: (lineWidth / 100)
        scale.y: (lineLength / 100)
        scale.z: (lineWidth / 100)
    }

    Model {
        id: axisYArrow

        visible: showArrow

        source: "#Cone"

        materials: axisYMaterial

        position.y: ((lineLength) - (lineWidth / 2))
        scale.x: (lineWidth / 100 * arrowWidth)
        scale.y: (lineWidth / 100 * arrowLength)
        scale.z: (lineWidth / 100 * arrowWidth)
    }

    DefaultMaterial {
        id: axisZMaterial
        diffuseColor: "#0000ff"
    }

    Model {
        id: axisZLine

        visible: showLine

        source: "#Cube"

        materials: axisZMaterial

        position.z: ((lineLength / 2) - (lineWidth / 2))
        scale.x: (lineWidth / 100)
        scale.y: (lineWidth / 100)
        scale.z: (lineLength / 100)
    }

    Model {
        id: axisZArrow

        visible: showArrow

        source: "#Cone"

        materials: axisZMaterial

        position.z: ((lineLength) - (lineWidth / 2))
        scale.x: (lineWidth / 100 * arrowWidth)
        scale.y: (lineWidth / 100 * arrowLength)
        scale.z: (lineWidth / 100 * arrowWidth)
        eulerRotation.x: 90
    }
}
