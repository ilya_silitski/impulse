import QtQuick
import QtQuick.Controls

ComboBox {
    id: select
    height: 36
    font.pixelSize: 14
    model: options
    rightPadding: 12

    property var options
    property int currentInd: -1
    property string placeholder: "Select target"

    contentItem: Text {
        leftPadding: 12
        topPadding: 6
        rightPadding: 24
        bottomPadding: 6
        height: 24
        text: currentInd === -1 ? placeholder : (options[currentInd] ? (options[currentInd].name || options[currentInd]) : placeholder)
        font: select.font
        color: currentInd === -1 || !select.enabled ? "#bfbfbf" : "#555555"
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    indicator: Icon {
        source: "qrc:///img/left"
        x: select.width - width - select.rightPadding
        y: select.topPadding + (select.availableHeight - height) / 2
        width: 18
        height: 18
        rotation: select.popup.visible ? 90 : -90
        opacity: 0.25

        Behavior on rotation { NumberAnimation { duration: 200 } }
    }

    background: Rectangle {
        anchors.fill: parent
        color: select.enabled ? "#ffffff" : "#f8f8f8"
        border.color: select.popup.visible ? "#40a9ff" : "#e8e8e8"
        radius: parent.height / 2
    }

    popup: Popup {
        y: select.height + 6
        width: select.width
        height: Math.min(implicitHeight, 198)
        padding: 0

        contentItem: ListView {
            clip: true
            implicitHeight: contentHeight
            model: select.popup.visible ? select.delegateModel : null
            currentIndex: select.highlightedIndex
        }

        background: Rectangle {
            id: backgroundRect
            border.width: 1
            border.color: "#e8e8e8"
            radius: 18
        }
    }

    delegate: ItemDelegate {
        height: 36
        width: select.width
        leftPadding: 12
        topPadding: 6
        rightPadding: 12
        bottomPadding: 6
        highlighted: currentInd === index
        contentItem: Text {
            height: 24
            text: modelData.name || modelData
            color: "#555555"
            font.pixelSize: 14
            font.weight: parent.highlighted ? Font.DemiBold : Font.Normal
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
        }
        background: Rectangle {
            anchors.fill: parent
            color: parent.highlighted ? "#e6f7ff" : "transparent"
            border.width: 0
        }
    }
}
