#include "wristbutton.h"

WristButton::WristButton(int index, int delay)
{
    pressAndHoldDelay = delay;
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timeout()));
    buttonIndex = index;
}

WristButton::~WristButton()
{
    delete timer;
}

void WristButton::inputsUpdated(QVariantList inputs)
{
    bool newPressed = inputs[buttonIndex].toInt() == 1;
    if (pressed != newPressed)
    {
        if (newPressed)
        {
            timer->setSingleShot(true);
            timer->setInterval(pressAndHoldDelay);
            timer->start();
            emit press();
        }
        else
        {
            if (timer->isActive())
            {
                timer->stop();
                emit shortPress();
            }
            emit release();
        }
        pressed = newPressed;
    }
}

void WristButton::timeout()
{
    if (pressed) emit longPress();
}
