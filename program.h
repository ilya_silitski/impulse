#ifndef PROGRAM_H
#define PROGRAM_H

#include <QObject>
#include <QDir>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMetaEnum>
#include <libssh/libssh.h>
#include <libssh/sftp.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAX_XFER_BUF_SIZE 16384


class Program : public QObject
{
    Q_OBJECT

public:
    Program();
    ~Program();

    // get list of all files on specified path
    // path should NOT start with '/' and have to end with '/'
    QVariantList get_files_list(QString path);
    // create new file
    int add_new_file(QString path);
    // open existing file
    QString open_file(QString path);
    // save file content
    int save_file(QString path, QString content);
    // save file content as
    int save_file_as(QString newPath, QString content);
    // save file as
    int save_as(QString oldPath, QString newPath);
    // rename file
    int rename_file(QString oldPath, QString newPath);
    // delete file
    int delete_file(QString path);
    // create new dir
    int add_new_dir(QString path);
    // rename existing dir
    int rename_dir(QString oldPath, QString newPath);
    // delete dir
    int delete_dir(QString path);

    QJsonDocument text_to_json(QString text);
    QString json_to_text(QJsonDocument json);

signals:
    void message(QString type, QString title, QString message);

private:

    ssh_session ssh;
    sftp_session sftp;

    QString abs_path = "/home/programmer/programs/";

    QString read_file_content(QString path);
    int write_file_content(QString content, QString path);

};

#endif // PROGRAM_H
