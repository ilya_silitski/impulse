#include "backend.h"

Backend::Backend()
{
    qDebug() << chk_sum;

    programController = new Program();

    connect(programController, SIGNAL(message(QString,QString,QString)), this, SIGNAL(programMessage(QString,QString,QString)));

    tcs = new TCS("127.0.0.1", 23032);

    connect(tcs, SIGNAL(chunk_received(tcs_conn_state_t,tcs_type_t,tcs_size_t,void*)), this, SLOT(chunk_received(tcs_conn_state_t,tcs_type_t,tcs_size_t,void*)));

    if(!tcs->start())
    {
        qDebug() << "Client Start Failed\n" << tcs->get_connection_error();
    }

//    emu_data_timer = new QTimer(this);
    gen_data_timer = new QTimer(this);

//    connect(emu_data_timer, SIGNAL(timeout()), this, SLOT(emu_data_upd()));
    connect(gen_data_timer, SIGNAL(timeout()), this, SLOT(gen_data_upd()));

//    emu_data_timer->start(1000.0 / EMU_DATA_FREQ);
    gen_data_timer->start(1000.0 / GEN_DATA_FREQ);
}

Backend::~Backend()
{
//    emu_data_timer->stop();
    gen_data_timer->stop();
    tcs->terminate();

//    delete emu_data_timer;
    delete gen_data_timer;
    delete programController;
    delete tcs;
}

void Backend::chunk_received(tcs_conn_state_t conn_state, tcs_type_t type, tcs_size_t payload_size, void *payload)
{
    switch(conn_state)
    {
    case TCS_CST_CONNECTED:
        qDebug() << "TCS_CST_CONNECTED";
        break;
    case TCS_CST_DISCONNECTED:
        qDebug() << "TCS_CST_DISCONNECTED";
        break;
    case TCS_CST_CHUNK_RECEIVED:
        switch(type)
        {
        case EXR_STDOUT:
        case EXR_STDERR:
            qDebug() << (char *)payload;
            break;
        case EXR_GET_VARS:
        {
            char vars[4096];
            memcpy(vars, payload, payload_size);
            qDebug() << vars;
        }
            break;
        case EXR_GET_STATE:
        {
            exr_state_t *s = (exr_state_t *)payload;
//            char *fn =  (char *)payload + sizeof(exr_state_t);

//            if(strcmp(fn, ProgramFile->value()) != 0)
//            {
//                ProgramFile->value(fn);
//                ProgramEditor->loadfile(ProgramFile->value());
//            }
            if(s->paused)
            {
                prog_paused();
            }
            else
            {
                prog_running();
            }
        }
            break;
        case EXR_NOT_RUNNING:
//            vars[0] = '\0';
//            prog_ln = -1;

//            DbgFlowControls->deactivate();
//            DbgRunControls->activate();

            break;
        case EXR_CONNECTED:
            prog_running();
            break;

        case EXR_TERMINATED:
        {
            exr_trm_stat_t *stat = (exr_trm_stat_t *)payload;
            switch(stat->type)
            {
            case EXT_TRM_EXITCODE:
                prog_finished(0, stat->value);
                break;
            case EXT_TRM_SIGNAL:
                if (stat->value == 9)
                {
                    prog_stopped();
                }
                else
                {
                    prog_finished(1, stat->value);
                }
                break;
            default:
                prog_finished(2, stat->value);
                break;
            }
        }
            break;
        default:
            qDebug() << "Unknown chunk with type:" << type;
            break;
        }
        break;
    default:
        break;
    }
}

void Backend::prog_running()
{
    emit programRunning();
}

void Backend::prog_paused()
{
    emit programPaused();
}

void Backend::prog_finished(int32_t s, int32_t v)
{
    qDebug() << "prog_finished s: " << s << "; v: " << v;
    emit programFinished(s, v);
}

void Backend::prog_stopped()
{
    emit programStopped();
}

void Backend::startup()
{
    tcs->send_command(EXR_GET_STATE, 0, 0);

    return;

//    emit logsUpdated(logs_hist);

//    QJsonObject setup(programController->text_to_json(programController->open_file("default.crs")).object());

//    QJsonObject startup = setup["startup"].toObject();
}

void Backend::stop()
{
    qDebug() << "stop";
}

void Backend::turnRobotOff()
{
    qDebug() << "turnRobotOff";
}

void Backend::turnRobotOn()
{
    qDebug() << "turnRobotOn";
}

void Backend::releaseBrakes()
{
    qDebug() << "releaseBrakes";
}

void Backend::changeSpeed(int speed)
{
    qDebug() << "changeSpeed" << speed;
}

void Backend::setCoordinateSystem(bool inTcp)
{
    qDebug() << "setCoordinateSystem" << inTcp;
}

void Backend::setTool(QString toolId)
{
    qDebug() << "setCoordinateSystem" << toolId;
}

void Backend::setPayload(QString payloadId)
{
    qDebug() << "setCoordinateSystem" << payloadId;
}

void Backend::goToPose(QVariantList target)
{
    qDebug() << "goToPose" << target;
}

void Backend::goToPosition(QVariantList target)
{
    qDebug() << "goToPosition" << target;
}

void Backend::startJogging(QString axis, bool direction)
{
    qDebug() << "startJogging" << axis << direction;
}

void Backend::stopJogging()
{
    qDebug() << "stopJogging";
}

void Backend::jogging(double x, double y, double z, double rx, double ry, double rz)
{
    qDebug() << "jogging" << x << y << z << rx << ry << rz;
}

void Backend::setZeroGravity(bool open)
{
    zeroGravityOpen = open;
}

void Backend::startZeroGravity()
{
    qDebug() << "startZeroGravity";
}

void Backend::stopZeroGravity()
{
    qDebug() << "stopZeroGravity";
}

void Backend::align()
{
    qDebug() << "align";
}

void Backend::setDigitalOutput(int index, bool value)
{
    qDebug() << "setDigitalOutput" << index << value;
}

void Backend::setAnalogOutput(int index, bool mode, double value)
{
    qDebug() << "setAnalogOutput" << index << mode << value;
}

void Backend::openFile(QString path)
{
    programController->open_file(path);
}

void Backend::createNewFile(QString path)
{
    programController->add_new_file(path);
}

void Backend::saveFile(QString path, QString content)
{
    programController->save_file(path, content);
}

void Backend::saveFileAs(QString newPath, QString content)
{
    programController->save_file_as(newPath, content);
}

void Backend::saveAs(QString oldPath, QString newPath)
{
    programController->save_as(oldPath, newPath);
}

void Backend::renameFile(QString oldPath, QString newPath)
{
    programController->rename_file(oldPath, newPath);
}

void Backend::deleteFile(QString path)
{
    programController->delete_file(path);
}

void Backend::createNewDir(QString path)
{
    programController->add_new_dir(path);
}

void Backend::renameDir(QString oldPath, QString newPath)
{
    programController->rename_dir(oldPath, newPath);
}

void Backend::deleteDir(QString path)
{
    programController->delete_dir(path);
}

void Backend::runProgramOnBreaksReleased()
{
    qDebug() << "run_prog_on_brks_rlsd";
}

void Backend::runProgram()
{
    const char *prg = "/home/ilya/work/python-to-c-embedding/clt-srv/test.py";
    tcs->send_command(EXR_RUN, (void *)prg, strlen(prg));
}

void Backend::pauseProgram()
{
    tcs->send_command(EXR_PAUSE, 0, 0);
}

void Backend::stepProgram()
{
    tcs->send_command(EXR_STEP, 0, 0);
}

void Backend::continueProgram()
{
    tcs->send_command(EXR_CONT, 0, 0);
}

void Backend::stopProgram()
{
    tcs->send_command(EXR_STOP, 0, 0);
}

void Backend::emu_data_upd()
{
    if (rtd.isEmpty()) return;
    QVariantMap data = {
        {"position", rtd["position"]},
        {"pose", PRETTY_POSE(rtd["pose"].toList())},
    };
    emit emulatorDataUpdated(data);
}

void Backend::gen_data_upd()
{
    tcs->send_command(EXR_GET_STATE, 0, 0);
    return;
    if (rtd.isEmpty()) return;
    QVariantMap data(rtd);
    data["position"] = PRETTY_POSITION(data["position"].toList());
    data["pose"] = PRETTY_POSE(data["pose"].toList());
    emit generalDataUpdated(data);
}
