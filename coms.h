#ifndef COMS_H
#define COMS_H
//maximum number of bytes for digital inputs
#define CTRLR_MAX_DIG_IN		4
//maximum number of bytes for digital outputs
#define CTRLR_MAX_DIG_OUT		4

#define CTRLR_MAX_AN_IN			4
#define CTRLR_MAX_AN_OUT		4

#include <stdint.h>

typedef enum
{
    CTRLR_COMS_STOP,
    CTRLR_COMS_MOVE_ADD_WP,
    CTRLR_COMS_MOVE_RUN,
    CTRLR_COMS_MOVE_SCALE,
    CTRLR_COMS_SETTINGS,
    CTRLR_COMS_POWER,
    CTRLR_COMS_GET_SETTINGS,
    CTRLR_COMS_GET_MODEL,
    CTRLR_COMS_JOG,
    CTRLR_COMS_NOP,
    CTRLR_COMS_SET_MODEL,
    CTRLR_COMS_SET_ENVIRONMENT,
    CTRLR_COMS_GET_MODEL_VIEW,
    CTRLR_COMS_GET_ENVIRONMENT,
    CTRLR_COMS_SET_OUTPUTS,
    CTRLR_COMS_ZG,
} ctrlr_coms_type_t;

typedef enum
{
    CTRLR_COMS_POWER_CMD_OFF,
    CTRLR_COMS_POWER_CMD_STBY,
    CTRLR_COMS_POWER_CMD_ON,
    CTRLR_COMS_POWER_CMD_RUN,
    CTRLR_COMS_POWER_CMD_STOP, //same is 'CTRLR_COMS_STOP'
} ctrlr_coms_power_cmd_t;

typedef enum
{
    CTRLR_STATE_IDLE = 0,
    CTRLR_STATE_OFF,
    CTRLR_STATE_STBY,
    CTRLR_STATE_ON,
    CTRLR_STATE_RUN,
    CTRLR_STATE_RESERVED0,
    CTRLR_STATE_FAILURE,
    CTRLR_STATE_FORCE_EXIT
} ctrlr_state_t;

typedef enum
{
    CTRLR_MM_HOLD,
    CTRLR_MM_ZG,
    CTRLR_MM_JOG,
    CTRLR_MM_MOVE,
} ctrlr_motion_mode_t;

typedef struct
{
    ctrlr_coms_type_t type;
} ctrlr_coms_stop_t;

typedef struct
{
    ctrlr_coms_type_t type;
    ctrlr_coms_power_cmd_t cmd;
} ctrlr_coms_power_t;

typedef enum
{
    MOVE_WP_TYPE_JOINT,
    MOVE_WP_TYPE_LINEAR_CART,
    MOVE_WP_TYPE_LINEAR_POSE
} move_waypoint_type_t;

typedef struct
{
    //move type
    move_waypoint_type_t type;
    //joint space
    double des_q[6];
    //cart space
    double des_x[6];
    //cart forces
    double force[6];
    uint8_t force_en[6];
    uint8_t force_in_tcp;
    //cart profile limits
    double vmax_t;
    double vmax_r;
    double amax_t;
    double amax_r;
    //joint profile limits
    double vmax_j;
    double amax_j;
    //blending radius
    double rblend;
    // pointer to the segment distance ring buffer element
    int pseg;
} move_waypoint_t;

typedef struct
{
    ctrlr_coms_type_t type;
    move_waypoint_t wp;
} ctrlr_coms_move_add_wp_t;

typedef struct
{
    ctrlr_coms_type_t type;
} ctrlr_coms_move_run_t;

typedef struct
{
    ctrlr_coms_type_t type;
    double scale_v;
    double scale_a;
} ctrlr_coms_move_scale_t;

typedef struct
{
    ctrlr_coms_type_t type;
} ctrlr_coms_get_settings_t;

typedef struct
{
    ctrlr_coms_type_t type;
} ctrlr_coms_get_model_t;

typedef enum
{
    CTRLR_COMS_JOG_MODE_OFF,
    CTRLR_COMS_JOG_MODE_FORCE,
    CTRLR_COMS_JOG_MODE_VELOCITY,
    CTRLR_COMS_JOG_MODE_POSITION
} ctrlr_coms_jog_mode_t;

typedef struct
{
    ctrlr_coms_type_t type;
    ctrlr_coms_jog_mode_t mode;
    uint8_t force_en[6];
    double force_max[6];
    double force_const[6];
    double stiff[6];
    double var[6];
} ctrlr_coms_jog_t;

typedef struct
{
    ctrlr_coms_type_t type;
    uint8_t en;
} ctrlr_coms_zg_t;

typedef struct
{
    ctrlr_coms_type_t type;
} ctrlr_coms_nop_t;

typedef struct
{
    ctrlr_coms_type_t type;
    struct
    {
        double mass;
        double com_x;
        double com_y;
        double com_z;
    } payload;
    struct
    {
        double x;
        double y;
        double z;
        double roll;
        double pitch;
        double yaw;
    } tool;
    struct
    {
        uint8_t in_tcp;
        uint8_t force_en[6];
        double force[6];
        double spd_max[6];
        double accel[6];
        double decel[6];
    } jog;
    struct
    {
        double damp[6];
        double virt_mass[6];
        double spd_max[6];
    } fc;
} ctrlr_coms_settings_t;

typedef struct
{
    ctrlr_coms_type_t type;
    //per bit mask
    uint8_t dig_out_mask[CTRLR_MAX_DIG_OUT];
    uint8_t dig_out[CTRLR_MAX_DIG_OUT];

    //per byte mask
    uint8_t an_out_mask[CTRLR_MAX_AN_OUT];
    uint8_t an_out_curr_mode[CTRLR_MAX_AN_OUT];
    double an_out_value[CTRLR_MAX_AN_OUT];
} ctrlr_coms_set_outputs_t;

#endif // COMS_H
