'use_strict';

function defineRobotObject() {
    Blockly.Python.definitions_['import_robot'] = 'from api import RobotAPI';
    Blockly.Python.definitions_['robot_object'] = "r = RobotAPI(ip=\"localhost\")\nr.init_robot()\nr.set_speed_scaling(1.0)\nr.set_accel_scaling(1.0)";
}




Blockly.Blocks['robot_wp_q'] = {
    init: function() {
        this.setInputsInline(true);
        this.appendDummyInput('J0').appendField(new Blockly.FieldNumber(0), '0');
        this.appendDummyInput('J1').appendField(new Blockly.FieldNumber(-90), '1');
        this.appendDummyInput('J2').appendField(new Blockly.FieldNumber(0), '2');
        this.appendDummyInput('J3').appendField(new Blockly.FieldNumber(-90), '3');
        this.appendDummyInput('J4').appendField(new Blockly.FieldNumber(0), '4');
        this.appendDummyInput('J5').appendField(new Blockly.FieldNumber(0), '5');
        this.setColour(0);
        this.setOutput(true, "wp_q");
    }
};

Blockly.Blocks['robot_wp_x'] = {
    init: function() {
        this.setInputsInline(true);
        this.appendDummyInput('X').appendField(new Blockly.FieldNumber(0), '0');
        this.appendDummyInput('Y').appendField(new Blockly.FieldNumber(-0.25), '1');
        this.appendDummyInput('Z').appendField(new Blockly.FieldNumber(1.0), '2');
        this.appendDummyInput('R').appendField(new Blockly.FieldNumber(-90), '3');
        this.appendDummyInput('P').appendField(new Blockly.FieldNumber(0), '4');
        this.appendDummyInput('Y').appendField(new Blockly.FieldNumber(-180), '5');
        this.setColour(250);
        this.setOutput(true, "wp_x");
    }
};

Blockly.Python['robot_wp_q'] = function(block) {
    var cmd = '';
    for (let step = '0'; step < '6'; step++) {
        cmd += block.getFieldValue(step.toString()) || 0.0;
        if (step.toString() === '5') return [cmd, Blockly.Python.ORDER_NONE];
        cmd += ', ';
    }
};

Blockly.Python['robot_wp_x'] = function(block) {
    var cmd = '';
    for (let step = '0'; step < '6'; step++) {
        cmd += block.getFieldValue(step.toString()) || 0.0;
        if (step.toString() === '5') return [cmd, Blockly.Python.ORDER_NONE];
        cmd += ', ';
    }
};


Blockly.Blocks['robot_get_q'] = {
    init: function() {
        this.setInputsInline(true);
        this.appendDummyInput().appendField('Get J');
        this.setColour(0);
        this.setOutput(true, "wp_q");
    }
};

Blockly.Blocks['robot_get_x'] = {
    init: function() {
        this.setInputsInline(true);
        this.appendDummyInput().appendField('Get L');
        this.setColour(250);
        this.setOutput(true, "wp_x");
    }
};

Blockly.Python['robot_get_q'] = function(block) {
    Blockly.Python.definitions_['import_math'] = 'import math';
    var fn = Blockly.Python.provideFunction_('pose_rad_to_deg',
                                             ['def pose_rad_to_deg(lst):',
                                             '    act_x = list(lst)',
                                             '    for i in range(3, 6):',
                                             '        act_x[i] = math.degrees(act_q[i])',
                                             '    return act_x']);
    return [fn + '(r.ctrl.data[act_q])', Blockly.Python.ORDER_NONE];
};

Blockly.Python['robot_get_x'] = function(block) {
    Blockly.Python.definitions_['import_math'] = 'import math';
    var fn = Blockly.Python.provideFunction_('pos_rad_to_deg',
                                             ['def pos_rad_to_deg(lst):',
                                             '    act_x = list(lst)',
                                             '    for i in range(3, 6):',
                                             '        act_x[i] = math.degrees(act_q[i])',
                                             '    return act_x']);
    return [fn + '(r.ctrl.data[act_x])', Blockly.Python.ORDER_NONE];
};

Blockly.Blocks['robot_add_wp_q_simple'] = {
    init: function() {
        this.setInputsInline(true);
        this.appendValueInput("WP")
        .setCheck("wp_q")
        .appendField("+WP J");
        this.setColour(0);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip('Appends Joint Waypoint');
    }
};

Blockly.Blocks['robot_add_wp_x_simple'] = {
    init: function() {
        this.setInputsInline(true);
        this.appendValueInput("WP")
        .setCheck("wp_x")
        .appendField("+WP L");
        this.setColour(250);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip('Appends Linear Waypoint');
    }
};

Blockly.Python['robot_add_wp_q_simple'] = function(block) {
    defineRobotObject();
    var J = Blockly.Python.valueToCode(block, 'WP', Blockly.Python.ORDER_NONE) || '0,-90,0,-90,0,0';
    if(J.includes(','))
        return 'r.add_wp_deg(t=0, des_q=['+J+'])\n';
    else
        return 'r.add_wp_deg(t=0, des_q='+J+')\n';
};

Blockly.Python['robot_add_wp_x_simple'] = function(block) {
    defineRobotObject();
    var J = Blockly.Python.valueToCode(block, 'WP', Blockly.Python.ORDER_NONE) || '0,-90,0,-90,0,0';
    if(J.includes(','))
        return 'r.add_wp_deg(t=1, des_x=['+J+'])\n';
    else
        return 'r.add_wp_deg(t=1, des_x='+J+')\n';
};


Blockly.Blocks['robot_add_wp_q'] = {
    init: function() {
        this.setInputsInline(true);
        this.appendValueInput("J0").setCheck("Number").appendField("+WP J0");
        this.appendValueInput("J1").setCheck("Number").appendField("J1");
        this.appendValueInput("J2").setCheck("Number").appendField("J2");
        this.appendValueInput("J3").setCheck("Number").appendField("J3");
        this.appendValueInput("J4").setCheck("Number").appendField("J4");
        this.appendValueInput("J5").setCheck("Number").appendField("J5");
        this.setColour(0);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip('Appends Joint Waypoint');
    }
};

Blockly.Blocks['robot_add_wp_x'] = {
    init: function() {
        this.setInputsInline(true);
        this.appendValueInput("J0").setCheck("Number").appendField("+WP X");
        this.appendValueInput("J1").setCheck("Number").appendField("Y");
        this.appendValueInput("J2").setCheck("Number").appendField("Z");
        this.appendValueInput("J3").setCheck("Number").appendField("R");
        this.appendValueInput("J4").setCheck("Number").appendField("P");
        this.appendValueInput("J5").setCheck("Number").appendField("Y");
        this.setColour(250);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip('Appends Joint Waypoint');
    }
};

Blockly.Python['robot_add_wp_q'] = function(block) {
    defineRobotObject();
    var j0 = Blockly.Python.valueToCode(block, 'J0', Blockly.Python.ORDER_ATOMIC) || 0.0;
    var j1 = Blockly.Python.valueToCode(block, 'J1', Blockly.Python.ORDER_ATOMIC) || -90.0;
    var j2 = Blockly.Python.valueToCode(block, 'J2', Blockly.Python.ORDER_ATOMIC) || 0.0;
    var j3 = Blockly.Python.valueToCode(block, 'J3', Blockly.Python.ORDER_ATOMIC) || -90.0;
    var j4 = Blockly.Python.valueToCode(block, 'J4', Blockly.Python.ORDER_ATOMIC) || 0.0;
    var j5 = Blockly.Python.valueToCode(block, 'J5', Blockly.Python.ORDER_ATOMIC) || 0.0;
    return 'r.add_wp_deg(t=0, des_q=['+j0+','+j1+','+j2+','+j3+','+j4+','+j5+'])\n';
};


Blockly.Python['robot_add_wp_x'] = function(block) {
    defineRobotObject();
    var x = Blockly.Python.valueToCode(block, 'J0', Blockly.Python.ORDER_ATOMIC) || 0.0;
    var y = Blockly.Python.valueToCode(block, 'J1', Blockly.Python.ORDER_ATOMIC) || -0.25;
    var z = Blockly.Python.valueToCode(block, 'J2', Blockly.Python.ORDER_ATOMIC) || 1.0;
    var r_ = Blockly.Python.valueToCode(block, 'J3', Blockly.Python.ORDER_ATOMIC) || -90.0;
    var p_ = Blockly.Python.valueToCode(block, 'J4', Blockly.Python.ORDER_ATOMIC) || 0.0;
    var y_ = Blockly.Python.valueToCode(block, 'J5', Blockly.Python.ORDER_ATOMIC) || -180.0;
    return 'r.add_wp_deg(t=1, des_x=['+x+','+y+','+z+','+r_+','+p_+','+y_+'])\n';
};

// ===========================================================================

Blockly.Blocks['robot_run'] = {
    init: function() {
        this.appendDummyInput()
        .appendField("Run WPs");
        this.setColour(330);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip('Starts waypoint execution');
    }
};

Blockly.Python['robot_run'] = function(block) {
    defineRobotObject();
    return 'r.run_wps()\n';
};


Blockly.Blocks['robot_await_motion'] = {
    init: function() {
        this.appendDummyInput()
        .appendField("Await WPs");
        this.setColour(330);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip('Starts waypoint execution');
    }
};

Blockly.Python['robot_await_motion'] = function(block) {
    defineRobotObject();
    return 'r.await_motion()\n';
};
