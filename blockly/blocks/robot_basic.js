'use_strict';

Blockly.Blocks['robot_delaytime'] = {
    init: function() {
        this.appendValueInput('TIME')
        .setCheck("Number")
        .appendField("Delay");
        this.appendDummyInput()
        .appendField("ms.");
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(330);
    }
};

Blockly.Python['robot_delaytime'] = function(block) {
    Blockly.Python.definitions_['import_time'] = 'import time';
    var value_time = Blockly.Python.valueToCode(block, 'TIME', Blockly.Python.ORDER_NONE) || '0';
    if (value_time === '0') {
        return 'time.sleep(0)\n';
    }
    else if(isNaN(value_time)) {
        return 'time.sleep('+value_time+'*0.001)\n'
    }
    else {
        return 'time.sleep('+value_time*0.001+')\n'
    }
};


Blockly.Blocks['robot_gettime'] = {
    init: function() {
        this.setInputsInline(true);
        this.appendDummyInput()
        .appendField("Cur. DateTime")
        this.setOutput(true, "datetime");
        this.setColour(330);
    }
};

Blockly.Python['robot_gettime'] = function(block) {
    Blockly.Python.definitions_['import_datetime'] = 'from datetime import datetime';
    var code = 'datetime.now()';
    return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Blocks['robot_timediffms'] = {
    init: function() {
        this.setInputsInline(true);
        this.appendDummyInput()
        .appendField("TimeDiff(ms)");
        this.appendValueInput("t1")
        .setCheck('datetime')
        this.appendValueInput("t2")
        .setCheck('datetime')
        this.setOutput(true, "Number");
        this.setColour(330);
        this.setTooltip('Returns time difference in milliseconds');
    }
};

Blockly.Python['robot_timediffms'] = function(block) {
    Blockly.Python.definitions_['import_datetime'] = 'from datetime import datetime';
    var t1 = Blockly.Python.valueToCode(block, 't1', Blockly.Python.ORDER_ATOMIC);
    var t2 = Blockly.Python.valueToCode(block, 't2', Blockly.Python.ORDER_ATOMIC);
    var code = '(('+t2+'-'+t1+').total_seconds() * 1000.0)';
    return [code, Blockly.Python.ORDER_NONE];
};
