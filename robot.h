#ifndef ROBOT_H
#define ROBOT_H

#define HOST "localhost"
#define RTD_PORT 29000
#define CTRL_PORT 29001
#define LOG_PORT 29002
#define JOG_DELAY 50
#define ZG_DELAY 50
#define SIZE(x) (sizeof(x) / sizeof((x)[0]))
#define GET_BIT(a, i) ((a[i / 8] & (1 << i % 8)) != 0)

#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include <QEventLoop>
#include <math.h>
#include <qqml.h>

#include "rtd.h"
#include "coms.h"
#include "utils.h"
#include "math/math.h"

typedef enum
{
    RCV_IDLE,
    RCV_SZ,
    RCV_PLOAD
} rcv_state_t;

class Robot : public QObject
{

    Q_OBJECT

public:
    Robot();
    ~Robot();

    void connect_rtd_sock();
    void disconnect_rtd_sock();

    void connect_ctrl_sock();
    void disconnect_ctrl_sock();

    void connect_log_sock();
    void disconnect_log_sock();

    // Executor

    void set_run(bool run);
    void set_wait_di(int ind, bool val);
    void set_wait_ai(int ind, bool mt, double val);
    bool is_brks_rlsd();

    // State
    void set_pwr_mode(ctrlr_coms_power_cmd_t cmd);
    void spd_acc_scale(double scale_v, double scale_a);

    // Motion
    void stop();
    void add_wp_l(double target[6], double params[4], double blend);
    void add_wp_j(double target[6], double params[2], double blend);
    void run_wps();
    void start_jog(int axis, int value);
    void stop_jog();
    void jogging(double x, double y, double z, double rx, double ry, double rz);
    void start_zg();
    void stop_zg();
    void align();

    // Settings
    void get_settings();
    uint8_t get_crdnt_sys();
    void set_crdnt_sys(bool in_tcp);
    void set_tcp(double tcp[6]);
    void set_payload(double mass, double com_x, double com_y, double com_z);

    // IO
    void set_do(int index, bool value);
    void set_ao(int index, bool currentMode, double value);

    // Logs
    QString get_log_hist();

private slots:
    void rtd_sock_ready_read();

    void ctrl_sock_ready_read();

    void handle_jog();
    void handle_jogging();
    void handle_zg();

    void log_sock_ready_read();

signals:
    void rtd_sock_connected();
    void rtd_sock_error(QAbstractSocket::SocketError err);
    void rtd_sock_disconnected();

    void ctrl_sock_connected();
    void ctrl_sock_error(QAbstractSocket::SocketError err);
    void ctrl_sock_disconnected();

    void log_sock_connected();
    void log_sock_error(QAbstractSocket::SocketError err);
    void log_sock_disconnected();

    void rtd_upd(QVariantMap data);
    void di_upd(QVariantList di);
    void ai_upd(QVariantList ai_v, QVariantList ai_m);
    void io_upd(QVariantMap io);
    void settings_received();
    void settings_upd(QVariantMap settings);
    void log_upd(QString logs);

    void mtn_stp();
    void prog_intr();
    void cmd_acc();
    void pnt_psd(int ind);
    void di_rcv();
    void ai_rcv();
    void brks_rlsd();

private:

    // Executor

    bool run = false;

    enum {
        NI,
        DI,
        AI
    } wait_mode;

    struct {
        int ind; // index (0-31)
        bool val; // value ('true' for 1, 'false' for 0)
    } di_wait;

    struct {
        int ind; // index (0-3)
        bool mt; // more than (if 'true' use '>', if 'false' use '<')
        double val; // value of analog input
    } ai_wait;

    // Real-time data socket

    QTcpSocket *rtd_sock;

    ctrlr_rt_data_t rtd;
    bool rtd_init = false;
    int rtd_rcv_ptr = 0;
    char rtd_rcv_buf[4096];
    uint16_t cmd_cntr;

    void compare_rtd(ctrlr_rt_data_t prev, ctrlr_rt_data_t next);
    void send_rtd();

    // Control socket

    QTcpSocket *ctrl_sock;
    QTimer *jog_timer;
    QTimer *jogging_timer;
    QTimer *zg_timer;

    rcv_state_t ctrl_rcv_state = RCV_IDLE;
    uint32_t ctrl_rcv_sz = 0;
    int ctrl_rcv_ptr = 0;
    char ctrl_rcv_buf[4096];
    ctrlr_coms_settings_t settings;
    double spd_scale = 1;
    double acc_scale = 1;
    int jog_axis = -1;
    int jog_value = 0;
    bool is_jogging = false;
    double jogging_vec[6];
    bool jogging_interrupted = false;
    bool zg_en = false;

    bool send_cmd(void *payload, uint32_t size);
    void parse_ctrl_data();
    void send_settings();
    void await_settings(int msec = 500);

    // Log socket

    QTcpSocket *log_sock;
    QString log_history;
};

bool di_equal(ctrlr_rt_data_t prev, ctrlr_rt_data_t next);
bool ai_equal(ctrlr_rt_data_t prev, ctrlr_rt_data_t next);
QVariantMap io_to_map(ctrlr_rt_data_t rtd);

#endif // ROBOT_H
