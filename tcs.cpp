#include "tcs.h"

TCS::TCS(QString host, quint16 port, quint32 max_size)
    : host(host), port(port), max_size(max_size)
{
    sock = new QTcpSocket();

    connect(sock, SIGNAL(connected()), this, SLOT(sock_connected()));
    connect(sock, SIGNAL(readyRead()), this, SLOT(sock_ready_read()));
    connect(sock, SIGNAL(errorOccurred(QAbstractSocket::SocketError)), this, SLOT(sock_error(QAbstractSocket::SocketError)));
    connect(sock, SIGNAL(disconnected()), this, SLOT(sock_disconnected()));

    payload = new uint8_t[max_size];
}

TCS::~TCS()
{
    delete sock;
}

bool TCS::start()
{
    sock->connectToHost(host, port);

    return sock->waitForConnected(1000);
}

bool TCS::terminate()
{
    sock->disconnectFromHost();

    return sock->waitForDisconnected(500);
}

QString TCS::get_connection_error()
{
    return sock->errorString();
}

bool TCS::send_command(tcs_type_t type, void *payload, size_t size)
{
    bool ret = false;

    if(!sock->isOpen())
    {
        return false;
    }

    tcs_size_t _size = size + sizeof(type);

    bool more = payload && (size > 0);

    this->mutex.lock();

    do
    {

        if(sock->write((char *)&_size, sizeof(_size)) != sizeof(_size))
        {
            break;
        }

        if(sock->write((char *)&type, sizeof(type)) != sizeof(type))
        {
            break;
        }

        if(more)
        {
            if(sock->write((char *)payload, size) != (qint64)size)
            {
                break;
            }
        }
        ret = true;
    }
    while(false);

    this->mutex.unlock();

    return ret;
}

void TCS::sock_connected()
{
    emit connected();
}

void TCS::sock_ready_read()
{

    int alen = 0;
    rcv_state = TCS_RST_LEN;
    conn_state = TCS_CST_NONE;
    bool cont = true;

    while (cont)
    {
        switch(rcv_state)
        {
            case TCS_RST_LEN:
                alen = sock->read(((char *)&chunk_size) + ptr, sizeof(tcs_size_t) - ptr);

                if(alen <= 0)
                {
                    conn_state = TCS_CST_DISCONNECTED;
                    cont = false;
                    break;
                }
                ptr += alen;

                if(ptr == sizeof(tcs_size_t))
                {
                    if(chunk_size > max_size)
                    {
                        conn_state = TCS_CST_CHUNK_TO_LONG;
                        cont = false;
                        break;
                    }
                    if(chunk_size < sizeof(tcs_type_t))
                    {
                        conn_state = TCS_CST_CHUNK_TO_SHORT;
                        cont = false;
                        break;
                    }
                    payload_size = chunk_size - sizeof(tcs_type_t);
                    rcv_state = TCS_RST_TYPE;
                    ptr = 0;
                }
                break;

            case TCS_RST_TYPE:
                alen = sock->read(((char *)&chunk_type) + ptr, sizeof(tcs_type_t) - ptr);

                if(alen <= 0)
                {
                    conn_state = TCS_CST_DISCONNECTED;
                    cont = false;
                    break;
                }
                ptr += alen;

                if(ptr == sizeof(tcs_type_t))
                {
                    if(payload_size > 0)
                    {
                        rcv_state = TCS_RST_PLOAD;
                    }
                    else
                    {
                        rcv_state = TCS_RST_LEN;
                        conn_state = TCS_CST_CHUNK_RECEIVED;
                        cont = false;
                    }
                    ptr = 0;
                }
                break;

            case TCS_RST_PLOAD:
                alen = sock->read(((char *)payload) + ptr, payload_size - ptr);

                if(alen <= 0)
                {
                    conn_state = TCS_CST_DISCONNECTED;
                    cont = false;
                    break;
                }
                ptr += alen;

                if(ptr == payload_size)
                {
                    rcv_state = TCS_RST_LEN;
                    conn_state = TCS_CST_CHUNK_RECEIVED;
                    ptr = 0;
                    cont = false;
                }
                break;
        }
    }

    switch(conn_state)
    {
        case TCS_CST_DISCONNECTED:
        case TCS_CST_CHUNK_TO_SHORT:
        case TCS_CST_CHUNK_TO_LONG:
        case TCS_CST_CHUNK_RECEIVED:
            emit chunk_received(conn_state, chunk_type, payload_size, payload);
            return;
        default:
            break;
    }
}

void TCS::sock_error(QAbstractSocket::SocketError err)
{
    qDebug() << err;
    emit error(err);
}

void TCS::sock_disconnected()
{
    emit disconnected();
}


