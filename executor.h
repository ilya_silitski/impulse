#ifndef EXECUTOR_H
#define EXECUTOR_H

#include <QObject>
#include <QThread>
#include <QEventLoop>
#include <QJsonObject>
#include <QJsonArray>
#include <QMetaType>
#include <QJSEngine>
#include "robot.h"
#include "wristbutton.h"
#include "math/math.h"

#define CHECK_DELAY 10

class Executor : public QObject
{
    Q_OBJECT

public:
    Executor();
    ~Executor();

    WristButton *white_btn;
    WristButton *blue_btn;
    WristButton *green_btn;
    WristButton *red_btn;

    enum CommandType {
        PROGRAM,
        SET,
        GRIPPER,
        WAIT,
        MOVE,
        COMMENT,
        POPUP,
        LOOP,
        IF
    };
    Q_ENUM(CommandType);

    Q_INVOKABLE void connectSockets();
    Q_INVOKABLE void disconnectSockets();

    Q_INVOKABLE void stop();
    Q_INVOKABLE void turnRobotOff();
    Q_INVOKABLE void turnRobotOn();
    Q_INVOKABLE void releaseBrakes();
    Q_INVOKABLE void changeSpeed(int speed);
    Q_INVOKABLE int getCoordinateSystem();
    Q_INVOKABLE void setCoordinateSystem(bool inTCP);
    Q_INVOKABLE QString getToolId();
    Q_INVOKABLE void setTool(QString id, QVariantList tcp);
    Q_INVOKABLE QString getPayloadId();
    Q_INVOKABLE void setPayload(QString id, double mass, double comX, double comY, double comZ);
    Q_INVOKABLE void goToPose(QVariantList target);
    Q_INVOKABLE void goToPosition(QVariantList target);
    Q_INVOKABLE void startJogging(QString axis, bool direction);
    Q_INVOKABLE void stopJogging();
    Q_INVOKABLE void jogging(double x, double y, double z, double rx, double ry, double rz);
    Q_INVOKABLE void startZeroGravity();
    Q_INVOKABLE void stopZeroGravity();
    Q_INVOKABLE void align();
    Q_INVOKABLE void setDigitalOutput(int index, bool value);
    Q_INVOKABLE void setAnalogOutput(int index, bool mode, double value);
    Q_INVOKABLE int getGripperOutputIndex();
    Q_INVOKABLE QString getLogsHistory();

    Q_INVOKABLE void run_prog_on_brks_rlsd(QJsonObject prog, QJsonObject stp);
    Q_INVOKABLE void run_prog(QJsonObject prog, QJsonObject stp);
    Q_INVOKABLE void continue_prog();
    Q_INVOKABLE void stop_prog();

private slots:
    void rtd_sock_connected();
    void io_upd(QVariantMap io);
    void rtd_sock_error(QAbstractSocket::SocketError error);
    void rtd_sock_disconnected();

    void ctrl_sock_connected();
    void ctrl_sock_error(QAbstractSocket::SocketError error);
    void ctrl_sock_disconnected();

    void log_sock_connected();
    void log_sock_error(QAbstractSocket::SocketError error);
    void log_sock_disconnected();

    void prog_intr();
    void pnt_psd(int ind);

signals:
    // Real-time Data Socket Signals
    void realTimeDataConnected();
    void realTimeDataUpdated(QVariantMap data);
    void realTimeDataDisconnected();

    // Control Socket Signals
    void controlConnected();
    void controlUnavailable();
    void controlDisconnected();

    // Logs Socket Signals
    void logConnected();
    void logUpdated(QString history);
    void logDisconnected();

    void exc_com_upd(QString commandId);
    void cur_pnt_upd(QString commandId);
    void prog_started();
    void prog_interrupted();
    void prog_finished();

    void continue_program();
    void stop_program();

    void show_popup(int popupType, QString message, QString buttonText,bool playSound, QString soundName);

    void input_received();

private:
    Robot *robot_cntrlr;

    QJSEngine scriptEngine;

    QString tool_id;
    QString payload_id;

    QJsonObject program;
    QJsonObject setup;

    QStringList di_names;
    QStringList do_names;
    QStringList ai_names;
    QStringList ao_names;

    QStringList pointsIds;

    bool running = false;

    int grpr_out_ind = 23;

    void add_joint_waypoint(QVariantList target, QVariantList params, double blend);
    void add_linear_waypoint(QVariantList target, QVariantList params, double blend);

    void init_executor(QJsonObject prog, QJsonObject stp);
    void exec_comm(QJsonArray commands);
    CommandType get_com_type_enum(QString type);
    QJsonObject get_point(QString id);
    void await_stop();
    void await_accepted();
    void await_time(int ms);
    void await_digital_input(int index, bool value);
    void await_analog_input(int index, bool moreThan, double value);
    void await_continue();

};

#endif // EXECUTOR_H
