QT += qml quick quick3d webenginequick

CONFIG += c++11 qmltypes

INCLUDEPATH += /usr/include/python3.8

LIBS += -lpython3.10 -lssh

QMAKE_CXXFLAGS += -fuse-ld=gold
QMAKE_LFLAGS += -fuse-ld=gold

QML_IMPORT_NAME = Backend
QML_IMPORT_MAJOR_VERSION = 1.0

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    backend.h \
    coms.h \
    executor.h \
    exr_proto.h \
    math/math.h \
    math/math_macro.h \
    math/matrix.h \
    math/matrix_ht.h \
    math/position.h \
    math/quat.h \
    math/vector.h \
    program.h \
    robot.h \
    rtd.h \
    tcs.h \
    utils.h \
    wristbutton.h

SOURCES += \
        backend.cpp \
        executor.cpp \
        main.cpp \
        math/position.c \
        math/quat.c \
        program.cpp \
        robot.cpp \
        tcs.cpp \
        wristbutton.cpp

RESOURCES += qml.qrc

# Default rules for deployment.
target.path = /opt/$${TARGET}/bin
INSTALLS += target

DISTFILES +=
