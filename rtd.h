#ifndef RTD_H
#define RTD_H

#include <stdint.h>
#define NJ 6

#define CTRLR_MAX_DIG_IN		4
#define CTRLR_MAX_DIG_OUT		4

#define CTRLR_MAX_AN_IN			4
#define CTRLR_MAX_AN_OUT		4


typedef struct
{
    double cycle_time; //actual cycle time
    double cycle_duty; //time in % occupied by calculations
    double state; //state of robot controller
    double servo_mode; //mode of joint's servomotors
    double motion_mode; //actual motion mode
    double jcond; //jacobian condition
    //waypoint buffer information
    struct
    {
        uint16_t buff_sz; //waypoint ring buffer size
        uint16_t buff_fill; //waipoint ring_buffer actual buffer fill
        uint16_t cmd_cntr; //counter of accepted commands
        uint16_t res; //reserved
    } wpi;
    double move_des_q[NJ]; //traj. gen. desired position for joint space
    double move_des_qd[NJ]; //traj. gen. desired velocity for joint space
    double move_des_x[6]; //traj. gen. desired position for cart. space
    double move_des_xd[6]; //traj. gen. desired velocity for cart. space
    double act_q[NJ]; //actual positions of joints
    double act_qd[NJ]; //actual vel. of joints
    double act_x[6]; //actual cart. position XYZ, RPY
    double act_xd[6]; //act cart. vel. XYZ, RPY
    double act_tq[NJ]; //actual joints torque
    double frict_tq[NJ]; //predicted friction torque
    double ne_tq[NJ]; //predicted gravity/careolis (newton-euler algorithm) torque
    double act_force_e[6]; //actual force(XYZ)/torque(RPY) applied to TCP in TCP coordinate frame
    double act_force_0[6]; //the same in base coordinate frame
    double des_trq[NJ]; //current command for servomotors
    double des_qd[NJ]; //velocity command for servomotors
    double temp_m[NJ]; //actual temperatures of motor's stator
    double temp_e[NJ]; //actual temperatures of motor's electronics
    double arm_current;
    double arm_voltage;
    double psu_voltage;
    struct
    {
        uint8_t dig_in_count; //number of bits
        uint8_t an_in_count;
        uint8_t dig_in[CTRLR_MAX_DIG_IN];
        uint8_t an_in_curr_mode[CTRLR_MAX_AN_IN];
        double an_in_value[CTRLR_MAX_AN_IN];

        uint8_t dig_out_count; //number of bits
        uint8_t an_out_count;
        uint8_t dig_out[CTRLR_MAX_DIG_OUT];
        uint8_t an_out_curr_mode[CTRLR_MAX_AN_OUT];
        double an_out_value[CTRLR_MAX_AN_OUT];
    } io;
} ctrlr_rt_data_t;

#endif // RTD_H
