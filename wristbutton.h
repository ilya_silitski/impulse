#ifndef WRISTBUTTON_H
#define WRISTBUTTON_H

#include <QObject>
#include <QTimer>
#include <QDebug>
#include <QVariant>

class WristButton : public QObject
{
    Q_OBJECT

public:
    WristButton(int index, int delay = 1000);
    ~WristButton();

public slots:
    void inputsUpdated(QVariantList inputs);

private slots:
    void timeout();

signals:
    void press();
    void release();
    void click();
    void shortPress();
    void longPress();

private:
    int pressAndHoldDelay; // in ms
    int buttonIndex;
    bool pressed = false;
    QTimer *timer;
};

#endif // WRISTBUTTON_H
