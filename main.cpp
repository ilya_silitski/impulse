#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQuick3D/QQuick3D>
#include <QtWebEngineQuick>

int main(int argc, char *argv[])
{
    QtWebEngineQuick::initialize();
//#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
//    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
//#endif

    QGuiApplication app(argc, argv);

    QSurfaceFormat::setDefaultFormat(QQuick3D::idealSurfaceFormat());

    QQmlApplicationEngine engine;
    engine.load(QUrl("qrc:/Main.qml"));
//        if (engine.rootObjects().isEmpty())
//            return -1;

    return app.exec();
}
