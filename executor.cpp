#include "executor.h"

Executor::Executor()
{
    robot_cntrlr = new Robot();

    connect(robot_cntrlr, SIGNAL(rtd_sock_connected()), this, SLOT(rtd_sock_connected()));
//    connect(robot_cntrlr, SIGNAL(rtd_upd(QVariantMap)), this, SLOT(rtd_upd(QVariantMap)));
    connect(robot_cntrlr, SIGNAL(rtd_upd(QVariantMap)), this, SIGNAL(realTimeDataUpdated(QVariantMap)));
    connect(robot_cntrlr, SIGNAL(io_upd(QVariantMap)), this, SLOT(io_upd(QVariantMap)));
    connect(robot_cntrlr, SIGNAL(rtd_sock_error(QAbstractSocket::SocketError)), this, SLOT(rtd_sock_error(QAbstractSocket::SocketError)));
    connect(robot_cntrlr, SIGNAL(rtd_sock_disconnected()), this, SLOT(rtd_sock_disconnected()));

    connect(robot_cntrlr, SIGNAL(ctrl_sock_connected()), this, SLOT(ctrl_sock_connected()));
//    connect(robot_cntrlr, SIGNAL(settings_upd(QVariantMap)), this, SLOT(settings_upd(QVariantMap)));
    connect(robot_cntrlr, SIGNAL(ctrl_sock_error(QAbstractSocket::SocketError)), this, SLOT(ctrl_sock_error(QAbstractSocket::SocketError)));
    connect(robot_cntrlr, SIGNAL(ctrl_sock_disconnected()), this, SLOT(ctrl_sock_disconnected()));

    connect(robot_cntrlr, SIGNAL(log_sock_connected()), this, SLOT(log_sock_connected()));
    connect(robot_cntrlr, SIGNAL(log_upd(QString)), this, SIGNAL(logUpdated(QString)));
    connect(robot_cntrlr, SIGNAL(log_sock_error(QAbstractSocket::SocketError)), this, SLOT(log_sock_error(QAbstractSocket::SocketError)));
    connect(robot_cntrlr, SIGNAL(log_sock_disconnected()), this, SLOT(log_sock_disconnected()));

    connect(robot_cntrlr, SIGNAL(prog_intr()), this, SLOT(prog_intr()));
    connect(robot_cntrlr, SIGNAL(pnt_psd(int)), this, SLOT(pnt_psd(int)));

    white_btn = new WristButton(28);
    connect(robot_cntrlr, SIGNAL(di_upd(QVariantList)), white_btn, SLOT(inputsUpdated(QVariantList)));

    blue_btn = new WristButton(29);
    connect(robot_cntrlr, SIGNAL(di_upd(QVariantList)), blue_btn, SLOT(inputsUpdated(QVariantList)));

    green_btn = new WristButton(30, 300);
    connect(robot_cntrlr, SIGNAL(di_upd(QVariantList)), green_btn, SLOT(inputsUpdated(QVariantList)));

    red_btn = new WristButton(31);
    connect(robot_cntrlr, SIGNAL(di_upd(QVariantList)), red_btn, SLOT(inputsUpdated(QVariantList)));
}

Executor::~Executor()
{
    delete robot_cntrlr;
    delete white_btn;
    delete blue_btn;
    delete green_btn;
    delete red_btn;
}

void Executor::connectSockets()
{
    robot_cntrlr->connect_log_sock();
    robot_cntrlr->connect_rtd_sock();
    robot_cntrlr->connect_ctrl_sock();
}

void Executor::disconnectSockets()
{
    robot_cntrlr->disconnect_ctrl_sock();
    robot_cntrlr->disconnect_rtd_sock();
    robot_cntrlr->disconnect_log_sock();
}

void Executor::stop()
{
    robot_cntrlr->stop();
}

void Executor::turnRobotOff()
{
    robot_cntrlr->set_pwr_mode(CTRLR_COMS_POWER_CMD_OFF);
}

void Executor::turnRobotOn()
{
    robot_cntrlr->set_pwr_mode(CTRLR_COMS_POWER_CMD_ON);
}

void Executor::releaseBrakes()
{
    robot_cntrlr->set_pwr_mode(CTRLR_COMS_POWER_CMD_RUN);
}

void Executor::changeSpeed(int speed)
{
    robot_cntrlr->spd_acc_scale((double)speed / 100.0, 1.0);
}

int Executor::getCoordinateSystem()
{
    return robot_cntrlr->get_crdnt_sys();
}

void Executor::setCoordinateSystem(bool inTCP)
{
    robot_cntrlr->set_crdnt_sys(inTCP);
}

QString Executor::getToolId()
{
    return tool_id;
}

void Executor::setTool(QString id, QVariantList tcp)
{
    tool_id = id;

    double raw_tcp[6];

    COPY_LIST_TO_VECTOR(&tcp, raw_tcp);

    robot_cntrlr->set_tcp(raw_tcp);
}

QString Executor::getPayloadId()
{
    return payload_id;
}

void Executor::setPayload(QString id, double mass, double comX, double comY, double comZ)
{
    payload_id = id;

    robot_cntrlr->set_payload(mass, comX, comY, comZ);
}

void Executor::goToPose(QVariantList target)
{
    if (running) return;

    double pretty_pose[6], raw_pose[6], parameters[2] = {1.047197, 1.396263};
    COPY_LIST_TO_VECTOR(&target, pretty_pose);
    PARSE_POSE(pretty_pose, raw_pose);
    robot_cntrlr->add_wp_j(raw_pose, parameters, 0.0);
    robot_cntrlr->run_wps();
}

void Executor::goToPosition(QVariantList target)
{
    if (running) return;

    double pretty_position[6], raw_position[6], parameters[4] = {0.25, 1.2, 1.047197, 1.396263};
    COPY_LIST_TO_VECTOR(&target, pretty_position);
    PARSE_POSITION(pretty_position, raw_position);
    robot_cntrlr->add_wp_l(raw_position, parameters, 0.0);
    robot_cntrlr->run_wps();
}

void Executor::startJogging(QString axis, bool direction)
{
    if (running) return;

    QStringList axes = {"x", "y", "z", "rx", "ry", "rz"};
    int axisInt = axes.indexOf(axis);
    robot_cntrlr->start_jog(axisInt, direction ? 1 : -1);
}

void Executor::stopJogging()
{
    robot_cntrlr->stop_jog();
}

void Executor::jogging(double x, double y, double z, double rx, double ry, double rz)
{
    robot_cntrlr->jogging(x, y, z, rx, ry, rz);
}

void Executor::startZeroGravity()
{
    if (running) return;

    robot_cntrlr->start_zg();
}

void Executor::stopZeroGravity()
{
    robot_cntrlr->stop_zg();
}

void Executor::align()
{
    robot_cntrlr->align();
}

void Executor::setDigitalOutput(int index, bool value)
{
    robot_cntrlr->set_do(index, value);
}

void Executor::setAnalogOutput(int index, bool mode, double value)
{
    robot_cntrlr->set_ao(index, mode, value);
}

int Executor::getGripperOutputIndex()
{
    return grpr_out_ind;
}

QString Executor::getLogsHistory()
{
    return robot_cntrlr->get_log_hist();
}

void Executor::run_prog_on_brks_rlsd(QJsonObject prog, QJsonObject stp)
{
    if (robot_cntrlr->is_brks_rlsd()) return run_prog(prog, stp);
    QTimer timer;
    timer.setSingleShot(true);
    timer.setInterval(10000);
    QEventLoop loop;
    connect(robot_cntrlr, SIGNAL(brks_rlsd()), &loop, SLOT(quit()));
    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    timer.start();
    loop.exec();
    if (timer.isActive()) run_prog(prog, stp);
    timer.stop();
}

void Executor::run_prog(QJsonObject prog, QJsonObject stp)
{
    init_executor(prog, stp);
    QJsonArray body = program["body"].toArray();
    emit prog_started();
    exec_comm(body);
    emit prog_finished();
}

void Executor::continue_prog()
{
    emit continue_program();
}

void Executor::stop_prog()
{
    robot_cntrlr->stop();
    running = false;
    robot_cntrlr->set_run(running);
    emit stop_program();
}

void Executor::rtd_sock_connected()
{
    emit realTimeDataConnected();
}

void Executor::io_upd(QVariantMap io)
{
    if (running)
    {
        QVariantList di_rtd = io["di"].toList();
        QJSValue di_obj = scriptEngine.newArray(di_rtd.size());
        for (int i = 0; i < di_rtd.size(); i++) di_obj.setProperty(i, di_rtd.at(i).toBool());
        scriptEngine.globalObject().setProperty("digital_in", di_obj);
        for (int i = 0; i < di_names.count(); i++)
        {
            if (di_names.at(i) != QString::asprintf("digital_in[%d]", i))
            {
                scriptEngine.globalObject().setProperty(di_names.at(i), di_rtd[i].toBool());
            }
        }

        QVariantList do_rtd = io["do"].toList();
        QJSValue do_obj = scriptEngine.newArray(do_rtd.size());
        for (int i = 0; i < do_rtd.size(); i++) do_obj.setProperty(i, do_rtd.at(i).toBool());
        scriptEngine.globalObject().setProperty("digital_out", do_obj);
        for (int i = 0; i < do_names.count(); i++)
        {
            if (do_names.at(i) != QString::asprintf("digital_out[%d]", i))
            {
                scriptEngine.globalObject().setProperty(do_names.at(i), do_rtd[i].toBool());
            }
        }

        QVariantList ai_rtd = io["ai"].toList();
        QJSValue ai_obj = scriptEngine.newArray(ai_rtd.size());
        for (int i = 0; i < ai_rtd.size(); i++) ai_obj.setProperty(i, ai_rtd.at(i).toBool());
        scriptEngine.globalObject().setProperty("analog_in", ai_obj);
        for (int i = 0; i < ai_names.count(); i++)
        {
            if (ai_names.at(i) == QString::asprintf("analog_in[%d]", i))
            {
                scriptEngine.globalObject().setProperty(ai_names.at(i), ai_rtd[i].toDouble());
            }
        }

        QVariantList ao_rtd = io["ao"].toList();
        QJSValue ao_obj = scriptEngine.newArray(ao_rtd.size());
        for (int i = 0; i < ao_rtd.size(); i++) ao_obj.setProperty(i, ao_rtd.at(i).toBool());
        scriptEngine.globalObject().setProperty("analog_out", ao_obj);
        for (int i = 0; i < ao_names.count(); i++)
        {
            if (ao_names.at(i) != QString::asprintf("analog_out[%d]", i))
            {
                scriptEngine.globalObject().setProperty(ao_names.at(i), ao_rtd[i].toDouble());
            }
        }
    }
}

void Executor::rtd_sock_error(QAbstractSocket::SocketError error)
{
    qDebug() << "Real-time Data Socket Error:" << error;
}

void Executor::rtd_sock_disconnected()
{
    emit realTimeDataDisconnected();
}

void Executor::ctrl_sock_connected()
{
    robot_cntrlr->get_settings();

    emit controlConnected();
}

void Executor::ctrl_sock_error(QAbstractSocket::SocketError error)
{
    if (error == QAbstractSocket::RemoteHostClosedError)
    {
        emit controlUnavailable();
    }
    else
    {
        qDebug() << "Control Socket Error:" << error;
    }
}

void Executor::ctrl_sock_disconnected()
{
    emit controlDisconnected();
}

void Executor::log_sock_connected()
{
    emit logConnected();
}

void Executor::log_sock_error(QAbstractSocket::SocketError error)
{
    qDebug() << "Log Socket Error:" << error;
}

void Executor::log_sock_disconnected()
{
    emit logDisconnected();
}

void Executor::prog_intr()
{
    running = false;
    robot_cntrlr->set_run(running);
    emit stop_program();
    emit prog_interrupted();
}

void Executor::pnt_psd(int ind)
{
    if (ind == 0)
    {
        pointsIds.clear();
    }
    else
    {
        emit cur_pnt_upd(pointsIds.at(ind - 1));
    }
}

void Executor::add_joint_waypoint(QVariantList target, QVariantList params, double blend)
{
    double pose[6], prms[2];
    COPY_LIST_TO_VECTOR(&target, pose);
    COPY_LIST_TO_VECTOR(&params, prms);
    robot_cntrlr->add_wp_j(pose, prms, blend);
}

void Executor::add_linear_waypoint(QVariantList target, QVariantList params, double blend)
{
    double position[6], prms[4];
    COPY_LIST_TO_VECTOR(&target, position);
    COPY_LIST_TO_VECTOR(&params, prms);
    robot_cntrlr->add_wp_l(position, prms, blend);
}

void Executor::init_executor(QJsonObject prog, QJsonObject stp)
{
    program = prog;
    setup = stp;

    pointsIds.clear();

    di_names.clear();
    do_names.clear();
    ai_names.clear();
    ao_names.clear();

    QJsonObject io_setup = setup["io"].toObject();

    QJsonArray di_setup = io_setup["di"].toArray();
    for (int i = 0; i < di_setup.size(); i ++) di_names.append(di_setup.at(i).toObject()["name"].toString());

    QJsonArray do_setup = io_setup["do"].toArray();
    for (int i = 0; i < do_setup.size(); i ++) do_names.append(do_setup.at(i).toObject()["name"].toString());

    QJsonArray ai_setup = io_setup["ai"].toArray();
    for (int i = 0; i < ai_setup.size(); i ++) ai_names.append(ai_setup.at(i).toObject()["name"].toString());

    QJsonArray ao_setup = io_setup["ao"].toArray();
    for (int i = 0; i < ao_setup.size(); i ++) ao_names.append(ao_setup.at(i).toObject()["name"].toString());

    running = true;
    robot_cntrlr->set_run(running);
}

void Executor::exec_comm(QJsonArray commands)
{
    for (int i = 0; i < commands.count(); i++)
    {
        if (!running) return;
        if (!commands[i].isObject()) continue;
        QJsonObject cmd_obj = commands[i].toObject();
        if (cmd_obj["ignored"].toBool()) continue;
        emit exc_com_upd(cmd_obj["id"].toString());
        CommandType cmd_type = get_com_type_enum(cmd_obj["type"].toString());
        switch (cmd_type) {
        case PROGRAM:
        {
            // handle program command
            bool endless = cmd_obj["endless"].toBool();
            int repeatCount = cmd_obj["repeatCount"].toInt();
            while (endless ? true : repeatCount > 0) {
                exec_comm(cmd_obj["subCommands"].toArray());
                if (!running) return;
                repeatCount--;
            }
            running = false;
            robot_cntrlr->set_run(running);
            robot_cntrlr->stop();
            break;
        }
        case WAIT:
        {
            // handle wait command
            QString option = cmd_obj["option"].toString();
            QJsonObject digital = {
                {"index", 0},
                {"value", false},
            };
            QJsonObject analog = {
                {"index", 0},
                {"moreThan", true},
                {"value", 0.0},
            };
            if (option == "time")
            {
                int time = cmd_obj["time"].toInt();
                await_time(time);
            }
            else if (option == "digital")
            {
                QJsonObject digital = cmd_obj["digital"].toObject();
                int index = digital["index"].toInt();
                bool value = digital["value"].toBool();
                await_digital_input(index, value);
            }
            else if (option == "analog")
            {
                QJsonObject analog = cmd_obj["analog"].toObject();
                int index = analog["index"].toInt();
                bool moreThan = analog["moreThan"].toBool();
                double value = analog["value"].toDouble();
                await_analog_input(index, moreThan, value);
            }
            break;
        }
        case GRIPPER:
        {
            bool value = cmd_obj["value"].toBool();
            robot_cntrlr->set_do(grpr_out_ind, value);
            break;
        }
        case SET:
        {
            // handle set command
            QJsonArray operations = cmd_obj["operations"].toArray();
            for (int i = 0; i < operations.count(); i++)
            {
                QJsonObject op = operations[i].toObject();
                QString op_type = op["type"].toString();
                if (op_type == "digital")
                {
                    robot_cntrlr->set_do(op["key"].toInt(), op["value"].toBool());
                }
                else if (op_type == "analog")
                {
                    robot_cntrlr->set_ao(op["key"].toInt(), op["currentMode"].toBool(), op["value"].toDouble());
                }
                else if (op_type == "tool")
                {
                    QJsonArray tools = setup["tools"].toArray();
                    for (int i = 0; i < tools.count(); i++)
                    {
                        QJsonObject tool = tools[i].toObject();
                        if (tool["id"].toString() == op["toolId"].toString())
                        {
                            setTool(tool["id"].toString(), tool["tcp"].toArray().toVariantList());
                        }
                    }
                }
                else if (op_type == "payload")
                {
                    QJsonArray payloads = setup["payloads"].toArray();
                    for (int i = 0; i < payloads.count(); i++)
                    {
                        QJsonObject payload = payloads[i].toObject();
                        if (payload["id"].toString() == op["payloadId"].toString())
                        {
                            setPayload(payload["id"].toString(), payload["mass"].toDouble(), payload["comX"].toDouble(), payload["comY"].toDouble(), payload["comZ"].toDouble());
                        }
                    }
                }
            }
            break;
        }
        case MOVE:
        {
            // handle move command
            QJsonArray points = cmd_obj["subCommands"].toArray();
            QString motion = cmd_obj["motion"].toString();
            double maxTransVel = cmd_obj["maxTransVel"].toDouble();
            double maxTransAcc = cmd_obj["maxTransAcc"].toDouble();
            double maxRotVel = cmd_obj["maxRotVel"].toDouble();
            double maxRotAcc = cmd_obj["maxRotAcc"].toDouble();
            double maxJointVel = cmd_obj["maxJointVel"].toDouble();
            double maxJointAcc = cmd_obj["maxJointAcc"].toDouble();
            double blend = cmd_obj["blend"].toDouble();
            bool autoBlend = cmd_obj["autoBlend"].toBool(false);
            QVariantList targets;
            for(int i = 0; i < points.count(); i++)
            {
                QJsonObject pnt_cmd_obj = points[i].toObject();
                if (pnt_cmd_obj["ignored"].toBool()) continue;
                QJsonObject pnt_obj = get_point(pnt_cmd_obj["pointId"].toString());
                targets.append(pnt_obj.toVariantMap());
                pointsIds.prepend(pnt_cmd_obj["id"].toString());
            }
            if (targets.count() == 0) break;
            if (autoBlend && targets.count() > 2)
            {
                if (motion == "joint")
                {
                    blend = 0;
                    for(int i = 0; i < targets.count(); i++)
                    {
                        QVariantList target = targets[i].toMap()["pose"].toList();
                        QVariantList params = {maxJointVel, maxJointAcc};
                        add_joint_waypoint(target, params, blend);
                        if ((i + 1) < targets.size() && (i + 2) < targets.size())
                        {
                            QVariantList via = targets[i + 1].toMap()["pose"].toList();
                            QVariantList to = targets[i + 2].toMap()["pose"].toList();
                            double from_raw[6];
                            double via_raw[6];
                            double to_raw[6];
                            COPY_LIST_TO_VECTOR(&target, from_raw);
                            COPY_LIST_TO_VECTOR(&via, via_raw);
                            COPY_LIST_TO_VECTOR(&to, to_raw);
                            double dist_a = DISTANCE(from_raw, via_raw, 6);
                            double dist_b = DISTANCE(via_raw, to_raw, 6);
                            blend = std::min(dist_a, dist_b) / 2;
                        }
                        else
                        {
                            blend = 0;
                        }
                    }
                }
                else if (motion == "linear")
                {
                    blend = 0;
                    for(int i = 0; i < targets.count(); i++)
                    {
                        QVariantList target = targets[i].toMap()["position"].toList();
                        QVariantList params = {maxTransVel, maxTransAcc, maxRotVel, maxRotAcc};
                        add_linear_waypoint(target, params, blend);
                        if ((i + 1) < targets.size() && (i + 2) < targets.size())
                        {
                            QVariantList via = targets[i + 1].toMap()["position"].toList();
                            QVariantList to = targets[i + 2].toMap()["position"].toList();
                            double from_raw[6];
                            double via_raw[6];
                            double to_raw[6];
                            COPY_LIST_TO_VECTOR(&target, from_raw);
                            COPY_LIST_TO_VECTOR(&via, via_raw);
                            COPY_LIST_TO_VECTOR(&to, to_raw);
                            double dist_a = DISTANCE(from_raw, via_raw, 3);
                            double dist_b = DISTANCE(via_raw, to_raw, 3);
                            blend = std::min(dist_a, dist_b) / 2;
                        }
                        else
                        {
                            blend = 0;
                        }
                    }
                }
            }
            else
            {
                for(int i = 0; i < targets.count(); i++)
                {
                    if (motion == "joint")
                    {
                        QVariantList target = targets[i].toMap()["pose"].toList();
                        QVariantList params = {maxJointVel, maxJointAcc};
                        add_joint_waypoint(target, params, blend);
                    }
                    else if (motion == "linear")
                    {
                        QVariantList target = targets[i].toMap()["position"].toList();
                        QVariantList params = {maxTransVel, maxTransAcc, maxRotVel, maxRotAcc};
                        add_linear_waypoint(target, params, blend);
                    }
                }
            }
            await_accepted();
            if (!running) break;
            robot_cntrlr->run_wps();
            await_stop();
            break;
        }
        case COMMENT:
        {
            // handle comment command
            QString comment = cmd_obj["comment"].toString();
            qDebug() << "Comment:" << comment;
            break;
        }
        case POPUP:
        {
            // handle popup command
            int popupType = cmd_obj["popupType"].toInt();
            QString message = cmd_obj["message"].toString();
            QString buttonText = cmd_obj["buttonText"].toString();
            bool playSound = cmd_obj["playSound"].toBool();
            QString soundName = cmd_obj["soundName"].toString();
            emit show_popup(popupType, message, buttonText, playSound, soundName);
            await_continue();
            break;
        }
        case LOOP:
        {
            QString mode = cmd_obj["mode"].toString();
            if (mode == "endless")
            {
                while (true)
                {
                    exec_comm(cmd_obj["subCommands"].toArray());
                    if (!running) return;
                }
            }
            else if (mode == "count")
            {
                int count = cmd_obj["count"].toInt();
                while (count > 0)
                {
                    exec_comm(cmd_obj["subCommands"].toArray());
                    if (!running) return;
                    count--;
                }
            }
            else if (mode == "expression")
            {
                QString expression = cmd_obj["expression"].toString();
                QJSValue result = scriptEngine.evaluate(expression);
                while (!result.isError() && result.isBool() && result.toBool())
                {
                    exec_comm(cmd_obj["subCommands"].toArray());
                    if (!running) return;
                    result = scriptEngine.evaluate(expression);
                }
            }
            break;
        }
        case IF:
        {
            for (int loc_i = i; loc_i < commands.count(); loc_i++)
            {
                if (!commands[loc_i].isObject())
                {
                    if (cmd_obj["type"].toString() == "IF") break;
                    else continue;
                }
                cmd_obj = commands[loc_i].toObject();
                if (cmd_obj["ignored"].toBool())
                {
                    if (cmd_obj["type"].toString() == "IF") break;
                    else continue;
                }
                emit exc_com_upd(cmd_obj["id"].toString());
                if (cmd_obj["type"].toString() == "IF" || cmd_obj["type"].toString() == "ELIF")
                {
                    i = loc_i;
                    QString expression = cmd_obj["expression"].toString();
                    QJSValue result = scriptEngine.evaluate(expression);
                    if (!result.isError() && result.isBool() && result.toBool())
                    {
                        exec_comm(cmd_obj["subCommands"].toArray());
                        if (!running) return;
                        break;
                    }
                }
                else if (cmd_obj["type"].toString() == "ELSE")
                {
                    i = loc_i;
                    exec_comm(cmd_obj["subCommands"].toArray());
                    if (!running) return;
                    break;
                }
                else break;
            }
            break;
        }
        }
    }
}

Executor::CommandType Executor::get_com_type_enum(QString type)
{
    int enumIndex = Executor::staticMetaObject.indexOfEnumerator("CommandType");
    QMetaEnum metaEnum = Executor::staticMetaObject.enumerator(enumIndex);
    return CommandType(metaEnum.keyToValue(type.toLocal8Bit()));
}

QJsonObject Executor::get_point(QString id)
{
    QJsonArray points = program["points"].toArray();
    for (int i = 0; i < points.count(); i++)
    {
        QJsonObject point = points[i].toObject();
        if (point["id"].toString() == id) return point;
    }
    return QJsonObject();
}

void Executor::await_stop()
{
    QEventLoop loop;
    connect(robot_cntrlr, SIGNAL(mtn_stp()), &loop, SLOT(quit()));
    connect(this, SIGNAL(stop_program()), &loop, SLOT(quit()));
    loop.exec();
}

void Executor::await_accepted()
{
    QEventLoop loop;
    connect(robot_cntrlr, SIGNAL(cmd_acc()), &loop, SLOT(quit()));
    connect(this, SIGNAL(stop_program()), &loop, SLOT(quit()));
    loop.exec();
}

void Executor::await_time(int ms)
{
    QTimer timer;
    timer.setSingleShot(true);
    timer.setTimerType(Qt::PreciseTimer);
    QEventLoop loop;
    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    connect(this, SIGNAL(stop_program()), &loop, SLOT(quit()));
    timer.start(ms);
    loop.exec();
}

void Executor::await_digital_input(int index, bool value)
{
    robot_cntrlr->set_wait_di(index, value);
    QEventLoop loop;
    connect(robot_cntrlr, SIGNAL(di_rcv()), &loop, SLOT(quit()));
    connect(this, SIGNAL(stop_program()), &loop, SLOT(quit()));
    loop.exec();
}

void Executor::await_analog_input(int index, bool moreThan, double value)
{
    robot_cntrlr->set_wait_ai(index, moreThan, value);
    QEventLoop loop;
    connect(robot_cntrlr, SIGNAL(ai_rcv()), &loop, SLOT(quit()));
    connect(this, SIGNAL(stop_program()), &loop, SLOT(quit()));
    loop.exec();
}

void Executor::await_continue()
{
    QEventLoop loop;
    connect(this, SIGNAL(continue_program()), &loop, SLOT(quit()));
    connect(this, SIGNAL(stop_program()), &loop, SLOT(quit()));
    loop.exec();
}
