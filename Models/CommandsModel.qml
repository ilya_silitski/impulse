import QtQuick

ListModel {
    ListElement {
        name: "Set"
        icon: "set"
        type: "SET"
    }
    ListElement {
        name: "Wait"
        icon: "wait"
        type: "WAIT"
    }
    ListElement {
        name: "Move"
        icon: "move"
        type: "MOVE"
    }
    ListElement {
        name: "Point"
        icon: "point"
        type: "POINT"
    }
    ListElement {
        name: "Gripper"
        icon: "gripper"
        type: "GRIPPER"
    }
    ListElement {
        name: "Comment"
        icon: "comment"
        type: "COMMENT"
    }
    ListElement {
        name: "Popup"
        icon: "popup"
        type: "POPUP"
    }
//    ListElement {
//        name: "Stop"
//        icon: "stop"
//        type: "STOP"
//    }
//    ListElement {
//        name: "Group"
//        icon: "group"
//        type: "GROUP"
//    }
    ListElement {
        name: "Loop"
        icon: "loop"
        type: "LOOP"
    }
    ListElement {
        name: "If"
        icon: "if"
        type: "IF"
    }
    ListElement {
        name: "Else If"
        icon: "elif"
        type: "ELIF"
    }
    ListElement {
        name: "Else"
        icon: "else"
        type: "ELSE"
    }
//    ListElement {
//        name: "Switch"
//        icon: "switch"
//        type: "SWITCH"
//    }
//    ListElement {
//        name: "Thread"
//        icon: "thread"
//        type: "THREAD"
//    }
//    ListElement {
//        name: "Routine"
//        icon: "routine"
//        type: "ROUTINE"
//    }
//    ListElement {
//        name: "Timer"
//        icon: "timer"
//        type: "TIMER"
//    }
//    ListElement {
//        name: "Script"
//        icon: "script"
//        type: "SCRIPT"
//    }
//    ListElement {
//        name: "Matrix"
//        icon: "matrix"
//        type: "MATRIX"
//    }
//    ListElement {
//        name: "Force"
//        icon: "force"
//        type: "FORCE"
//    }
//    ListElement {
//        name: "Conveyor"
//        icon: "conveyor"
//        type: "CONVEYOR"
//    }
//    ListElement {
//        name: "Screwdrive"
//        icon: "screwdrive"
//        type: "SCREWDRIVE"
//    }
}
