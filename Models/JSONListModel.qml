import QtQuick

Item {
    property var source: null

    property ListModel model : ListModel { id: jsonModel; dynamicRoles: true }
    property alias count: jsonModel.count

    onSourceChanged: {
        jsonModel.clear();

        if (!source)
            return;

        for (let index = 0; index < source.length; index++) {
            jsonModel.append(source[index]);
        }

    }
}
