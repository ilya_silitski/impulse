#ifndef VECTOR_H_
#define VECTOR_H_

#include "math_macro.h"

#include <string.h>
#include <stdio.h>
#include <stdbool.h>


static inline double vector_dot(double *a, double *b, int l)
{
	double r = 0;

	for(int i = 0; i < l; i++)
	{
		r += a[i] * b[i];
	}
	return r;
}

static inline double vector_norm(double *a, int l)
{
	return sqrt(vector_dot(a, a, l));
}


static inline void vector_scale(double *a, double k, double *b, int l)
{
	for(int i = 0; i < l; i++)
	{
		b[i] = a[i] * k;
	}
}

static inline void vector_set(double *a, double v, int l)
{
	for(int i = 0; i < l; i++)
	{
		a[i] = v;
	}
}

static inline void vector_zero(double *a, int l)
{
	for(int i = 0; i < l; i++)
	{
		a[i] = 0;
	}
}

static inline void vector_abs(double *a, double *b, int l)
{
	for(int i = 0; i < l; i++)
	{
		b[i] = fabs(a[i]);
	}
}

static inline void vector_copy(double *a, double *b, int l)
{
	for(int i = 0; i < l; i++)
	{
		b[i] = a[i];
	}
}

static inline void vector_add(double *a, double *b, double *c, int l)
{
	for(int i = 0; i < l; i++)
	{
		c[i] = a[i] + b[i];
	}
}

static inline void vector_scale_add(double *a, double k, double *b, double *c, int l)
{
	for(int i = 0; i < l; i++)
	{
		c[i] = a[i] * k + b[i];
	}
}

static inline void vector_sub(double *a, double *b, double *c, int l)
{
	for(int i = 0; i < l; i++)
	{
		c[i] = a[i] - b[i];
	}
}

/*element-wise multiplication*/
static inline void vector_mult_ew(double *a, double *b, double *c, int l)
{
	for(int i = 0; i < l; i++)
	{
		c[i] = a[i] * b[i];
	}
}

static inline double vector_dist(double *a, double *b, int l)
{
	double r = 0;

	for(int i = 0; i < l; i++)
	{
		r += SQ(a[i] - b[i]);
	}

	return sqrt(r);
}

static inline void vector_cross3(double *a, double *b, double *c)
{
	double r[3];

	r[0] = a[1] * b[2] - a[2] * b[1];
	r[1] = a[2] * b[0] - a[0] * b[2];
	r[2] = a[0] * b[1] - a[1] * b[0];

	vector_copy(r, c, 3);
}

static inline double vector_min(double *v, int n, bool is_abs)
{
    double m = INFINITY;
    
    if(is_abs)
    {
        for(int i = 0; i < n; i++)
        {
            m = MIN(m, ABS(v[i]));
        }
    }
    else
    {
        for(int i = 0; i < n; i++)
        {
            m = MIN(m, v[i]);
        }
    }
    return m;
}


static inline bool vector_is_zero(double *a, int n)
{
    for(int i =0; i< n; i++)
    {
        if(a[i] != 0)
        {
            return false;
        }
    }

    return true;
}

static inline void vector_print(const char *name, double *v, int l)
{
	printf("vector %d '%s':\n", l, name);

	for(int i = 0; i < l; i++)
	{
		printf("%12.4f", v[i]);
	}
	printf("\n");
}

#endif
