#ifndef MATRIX_H_
#define MATRIX_H_

#include "math_macro.h"
#include "vector.h"

#ifndef NOGSL
#include <gsl/gsl_matrix_double.h>
#include <gsl/gsl_linalg.h>
#endif

#include <string.h>
#include <stdio.h>


static inline void matrix_copy_3x3(double a[3][3], double b[3][3])
{
	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			b[i][j] = a[i][j];
		}
	}
}

static inline void matrix_copy_4x4(double a[4][4], double b[4][4])
{
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			b[i][j] = a[i][j];
		}
	}
}

static inline void matrix_copy_6x6(double a[6][6], double b[6][6])
{
	for(int i = 0; i < 6; i++)
	{
		for(int j = 0; j < 6; j++)
		{
			b[i][j] = a[i][j];
		}
	}
}

static inline void matrix_diag_6x6(double a[6][6], double b)
{
	for(int i = 0; i < 6; i++)
	{
		for(int j = 0; j < 6; j++)
		{
			a[i][j] = i == j ? b : 0.0;
		}
	}
}

static inline void matrix_diag_4x4(double a[4][4], double b)
{
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			a[i][j] = i == j ? b : 0.0;
		}
	}
}

static inline void matrix_diag_3x3(double a[3][3], double b)
{
	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			a[i][j] = i == j ? b : 0.0;
		}
	}
}

static inline void matrix_transpose_6x6(double a[6][6], double res[6][6])
{
    double b[6][6];

    for (int i = 0; i < 6; i++)
    {
        for (int j = 0; j < 6; j++)
        {
            b[i][j] = a[j][i];
        }
    }
	matrix_copy_6x6(b, res);
}

static inline void matrix_mult_6x6(double a[6][6], double b[6][6], double res[6][6]) 
{
    double c[6][6] = {0};

    for (int i = 0; i < 6; i++) 
    {
        for (int j = 0; j < 6; j++) 
        {
            for (int k = 0; k < 6; k++) 
            {
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    }
	matrix_copy_6x6(c, res);
}

static inline void matrix_mult_vector_6x6(double a[6][6], double b[6], double res[6])
{
	double c[6] = {0};

	for(int i = 0; i < 6; i++)
	{
		for(int j = 0; j < 6; j++)
		{
			c[i] += a[i][j] * b[j];
		}
	}
	vector_copy(c, res, 6);
}

static inline void matrix_transpose_4x4(double a[4][4], double res[4][4])
{
    double b[4][4];

    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            b[i][j] = a[j][i];
        }
    }
	matrix_copy_4x4(b, res);
}

static inline void matrix_mult_4x4(double a[4][4], double b[4][4], double res[4][4]) 
{
    double c[4][4] = {0};

    for (int i = 0; i < 4; i++) 
    {
        for (int j = 0; j < 4; j++) 
        {
            for (int k = 0; k < 4; k++) 
            {
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    }
	matrix_copy_4x4(c, res);
}

static inline void matrix_mult_vector_4x4(double a[4][4], double b[4], double res[4])
{
	double c[4] = {0};

	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			c[i] += a[i][j] * b[j];
		}
	}
	vector_copy(c, res, 4);
}

static inline void matrix_transpose_3x3(double a[3][3], double res[3][3])
{
    double b[3][3];

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            b[i][j] = a[j][i];
        }
    }
	matrix_copy_3x3(b, res);
}

static inline void matrix_mult_3x3(double a[3][3], double b[3][3], double res[3][3])
{
    double c[3][3] = {0};

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            for (int k = 0; k < 3; k++)
            {
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    }
	matrix_copy_3x3(c, res);
}

static inline void matrix_mult_vector_3x3(double a[3][3], double b[3], double res[3])
{
	double c[3] = {0};

	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			c[i] += a[i][j] * b[j];
		}
	}
	vector_copy(c, res, 3);
}


#ifndef NOGSL
static inline int matrix_inv_3x3(double A[3][3], double AI[3][3])
{
	int s;
	int status;
	size_t pd[3];
	double Ac[3][3];
	memcpy(Ac, A, sizeof(Ac));
	gsl_permutation p = {.size = 3, .data = pd};
	gsl_matrix_view m = gsl_matrix_view_array(*Ac, 3, 3);
	gsl_matrix_view i = gsl_matrix_view_array(*AI, 3, 3);

	status = gsl_linalg_LU_decomp(&m.matrix, &p, &s);
	if(status != 0)
	{
		//printf("!!! WARNING: LU decomp. failed for 3x3 matrix\n");
		return status;

	}
	status = gsl_linalg_LU_invert(&m.matrix, &p, &i.matrix);
	if(status != 0)
	{
		//printf("!!! WARNING: LU invert failed for 3x3 matrix\n");
		return status;
	}
	return 0;
}

static inline int matrix_inv_4x4(double A[4][4], double AI[4][4])
{
	int s;
	int status;
	size_t pd[4];
	double Ac[4][4];
	memcpy(Ac, A, sizeof(Ac));
	gsl_permutation p = {.size = 4, .data = pd};
	gsl_matrix_view m = gsl_matrix_view_array(*Ac, 4, 4);
	gsl_matrix_view i = gsl_matrix_view_array(*AI, 4, 4);

	status = gsl_linalg_LU_decomp(&m.matrix, &p, &s);
	if(status != 0)
	{
		//printf("!!! WARNING: LU decomp. failed for 4x4 matrix\n");
		return status;

	}
	status = gsl_linalg_LU_invert(&m.matrix, &p, &i.matrix);
	if(status != 0)
	{
		//printf("!!! WARNING: LU invert failed for 4x4 matrix\n");
		return status;
	}
	return 0;
}

static inline int matrix_inv_6x6(double A[6][6], double AI[6][6])
{
	int s;
	int status;
	size_t pd[6];
	double Ac[6][6];
	memcpy(Ac, A, sizeof(Ac));
	gsl_permutation p = {.size = 6, .data = pd};
	gsl_matrix_view m = gsl_matrix_view_array(*Ac, 6, 6);
	gsl_matrix_view i = gsl_matrix_view_array(*AI, 6, 6);

	status = gsl_linalg_LU_decomp(&m.matrix, &p, &s);
	if(status != 0)
	{
		//printf("!!! WARNING: LU decomp. failed for 6x6 matrix\n");
		return status;

	}
	status = gsl_linalg_LU_invert(&m.matrix, &p, &i.matrix);
	if(status != 0)
	{
		//printf("!!! WARNING: LU invert failed for 6x6 matrix\n");
		return status;
	}
	return 0;
}

static inline double matrix_cond_6x6(double A[6][6])
{
	int status;
	double Ac[6][6], Vc[6][6];
	double sc[6], wc[6];

	memcpy(Ac, A, sizeof(Ac));
	gsl_matrix_view a = gsl_matrix_view_array(*Ac, 6, 6);
	gsl_matrix_view v = gsl_matrix_view_array(*Vc, 6, 6);
	gsl_vector_view s = gsl_vector_view_array(sc, 6);
	gsl_vector_view w = gsl_vector_view_array(wc, 6);

	status = gsl_linalg_SV_decomp(&a.matrix, &v.matrix, &s.vector, &w.vector);
	if(status != 0)
	{
		return INFINITY;
	}

	double max = -INFINITY;
	double min = INFINITY;

	for(int i = 0; i < 6; i++)
	{
		if(sc[i] == 0)
		{
			return INFINITY;
		}
		max = MAX(max, sc[i]);
		min = MIN(min, sc[i]);
	}

	return max / min;
}
#endif

static inline void matrix_print_3x3(const char *name, double v[3][3])
{
	printf("matrix 3x3 '%s':\n", name);
	
	for(int i = 0; i < 3; i++)
	{
		printf("  ");
		for(int j = 0; j < 3; j++)
		{
			printf("%12.4f", v[i][j]);
		}
		printf("\n");
	}
}

static inline void matrix_print_4x4(const char *name, double v[4][4])
{
	printf("matrix 4x4 '%s':\n", name);
	
	for(int i = 0; i < 4; i++)
	{
		printf("  ");
		for(int j = 0; j < 4; j++)
		{
			printf("%12.4f", v[i][j]);
		}
		printf("\n");
	}
}

static inline void matrix_print_6x6(const char *name, double v[6][6])
{
	printf("matrix 6x6 '%s':\n", name);
	
	for(int i = 0; i < 6; i++)
	{
		printf("  ");
		for(int j = 0; j < 6; j++)
		{
			printf("%12.4f", v[i][j]);
		}
		printf("\n");
	}
}

static inline void matrix_print_8x8(const char *name, double v[8][8])
{
	printf("matrix 8x8 '%s':\n", name);
	
	for(int i = 0; i < 8; i++)
	{
		printf("  ");
		for(int j = 0; j < 8; j++)
		{
			printf("%12.4f", v[i][j]);
		}
		printf("\n");
	}
}

#endif
