#ifndef QUAT_H__
#define QUAT_H__

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

void quat_normalize(double q[4]);
void quat_to_rotm(double q[4], double r[3][3]);
void rot_to_quat(double r[3][3], double q[4]);
void rpy_to_quat(double *rpy, double q[4]);
void quat_to_rpy(double q[4], double *rpy);
bool quat_inv(double qin[4], double qout[4]);
void quat_mult(double qa[4], double qb[4], double qout[4]);
void quat_slerp(double  q1[4], double q2[4], double t, double  qout[4]);
void quat_log(double qin[4], double qout[4]);
void quat_slerp_vel(double  q1[4], double q2[4], double rpy[3]);
double quat_dist(double q1[4], double q2[4]);

#ifdef __cplusplus
}
#endif


#endif
