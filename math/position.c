//
// Created by mariya.novik on 17.04.2020.
//

#include "matrix.h"
#include "matrix_ht.h"

#include <math.h>
#include <stdio.h>

static double eps = 1e-6;

void tr_to_position(double matrix[4][4], double p[6]) 
{
    double roll = 0;
    double pitch = -asin(CLIP(matrix[2][0], -1.0, 1.0));
    double yaw = 0;

    if(fabs(fabs(matrix[2][0]) - 1.0) < eps) 
	{
        if(matrix[2][0] > 0) 
		{
            yaw = atan2(-matrix[0][1], -matrix[0][2]);
        } 
		else 
		{
            yaw = -atan2(matrix[0][1], matrix[0][2]);
        }
    } 
	else 
	{
        roll = atan2(matrix[2][1], matrix[2][2]);
        yaw = atan2(matrix[1][0], matrix[0][0]);
    }
    p[0] = matrix[0][3];
    p[1] = matrix[1][3];
    p[2] = matrix[2][3];
    p[3] = roll;
    p[4] = pitch;
    p[5] = yaw;
}

void position_to_tr(double p[6], double result[4][4]) 
{
    double rotation[3][3];
    double rx[3][3];
    double ry[3][3];
    double rz[3][3];

    matrix_rotx_3x3(p[3], rx);
    matrix_roty_3x3(p[4], ry);
    matrix_rotz_3x3(p[5], rz);

    matrix_mult_3x3(rz, ry, rotation);
    matrix_mult_3x3(rotation, rx, rotation);

    for (int i = 0; i < 3; ++i) 
	{
        for (int j = 0; j < 3; ++j) 
		{
            result[i][j] = rotation[i][j];
        }
        result[i][3] = p[i];
        result[3][i] = 0;
    }
    result[3][3] = 1;
}

void rpy_to_r(double p[3], double result[3][3]) 
{
    double rx[3][3];
    double ry[3][3];
    double rz[3][3];

    matrix_rotx_3x3(p[0], rx);
    matrix_roty_3x3(p[1], ry);
    matrix_rotz_3x3(p[2], rz);

    matrix_mult_3x3(rz, ry, result);
    matrix_mult_3x3(result, rx, result);
}

void r_to_rpy(double matrix[3][3], double p[3]) 
{
    double roll = 0;
    double pitch = -asin(CLIP(matrix[2][0], -1.0, 1.0));
    double yaw = 0;

    if(fabs(fabs(matrix[2][0]) - 1.0) < eps) 
	{
        if(matrix[2][0] > 0) 
		{
            yaw = atan2(-matrix[0][1], -matrix[0][2]);
        } 
		else 
		{
            yaw = -atan2(matrix[0][1], matrix[0][2]);
        }
    } 
	else 
	{
        roll = atan2(matrix[2][1], matrix[2][2]);
        yaw = atan2(matrix[1][0], matrix[0][0]);
    }
    p[0] = roll;
    p[1] = pitch;
    p[2] = yaw;
}

