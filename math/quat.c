#include "quat.h"
#include "position.h"
#include "math_macro.h"
#include "vector.h"
#include <math.h>
#include <string.h>

/*TODO: change function names to start from quat:
	quat_to_rotm
	quat_from_rotm
	etc. ...
*/

void quat_normalize(double q[4])
{
	vector_scale(q, 1.0 / vector_norm(q, 4), q, 4);
}

void rot_to_quat(double r[3][3], double q[4])
{
	q[0] = 0.5 * sqrt(1.0 + r[0][0] + r[1][1] + r[2][2]);
	if(fabs(q[0]) > 1.0e-12)
	{
		q[1] = 0.25 * (r[2][1] - r[1][2]) / q[0];
		q[2] = 0.25 * (r[0][2] - r[2][0]) / q[0];
		q[3] = 0.25 * (r[1][0] - r[0][1]) / q[0];
	}
	else
	{
		q[1] = 0.5 * sqrt(1.0 + r[0][0] - r[1][1] - r[2][2]);
		q[2] = 0.25 * (r[0][1] + r[1][0]) / q[1];
		q[3] = 0.25 * (r[0][2] + r[2][0]) / q[1];
		q[0] = 0.25 * (r[2][1] - r[1][2]) / q[1];
	}
}

void rpy_to_quat(double *rpy, double q[4])
{
	double cr = cos(rpy[2] / 2.0);
	double cp = cos(rpy[1] / 2.0);
	double cy = cos(rpy[0] / 2.0);

	double sr = sin(rpy[2] / 2.0);
	double sp = sin(rpy[1] / 2.0);
	double sy = sin(rpy[0] / 2.0);

	q[0] = cr * cp * cy + sr * sp * sy;
	q[1] = cr * cp * sy - sr * sp * cy;
	q[2] = cr * sp * cy + sr * cp * sy;
	q[3] = sr * cp * cy - cr * sp * sy;
}

void quat_to_rotm(double q[4], double r[3][3])
{
	double sqw = SQ(q[0]);
	double sqx = SQ(q[1]);
	double sqy = SQ(q[2]);
	double sqz = SQ(q[3]);

	double invs = 1.0;

	r[0][0] = ( sqx - sqy - sqz + sqw) * invs ; // since sqw + sqx + sqy + sqz =1/invs*invs
	r[1][1] = (-sqx + sqy - sqz + sqw) * invs ;
	r[2][2] = (-sqx - sqy + sqz + sqw) * invs ;

	double tmp1 = q[1] * q[2];
	double tmp2 = q[3] * q[0];

	r[1][0] = 2.0 * (tmp1 + tmp2) * invs ;
	r[0][1] = 2.0 * (tmp1 - tmp2) * invs ;

	tmp1 = q[1] * q[3];
	tmp2 = q[2] * q[0];

	r[2][0] = 2.0 * (tmp1 - tmp2) * invs ;
	r[0][2] = 2.0 * (tmp1 + tmp2) * invs ;

	tmp1 = q[2] * q[3];
	tmp2 = q[1] * q[0];

	r[2][1] = 2.0 * (tmp1 + tmp2) * invs ;
	r[1][2] = 2.0 * (tmp1 - tmp2) * invs ;      
}

void quat_to_rpy(double q[4], double *rpy)
{
	/*
	double qw = q[0];
	double qx = q[1];
	double qy = q[2];
	double qz = q[3];
	double asin_input = CLIP(-2.0 * (qx * qz - qy * qw), -1.0, 1.0);

	rpy[0] = atan2(2.0 * (qx * qy + qw * qz), SQ(qw) + SQ(qx) - SQ(qy) - SQ(qz));
	rpy[1] = asin(asin_input);
	rpy[2] = atan2(2.0 * (qz * qy + qx * qw), SQ(qw) - SQ(qx) - SQ(qy) + SQ(qz));

	double temp =  rpy[0];
	rpy[0] = rpy[2];
	rpy[2] = temp;
	*/

	double r[3][3];
	quat_to_rotm(q, r);
	r_to_rpy(r, rpy);
}

bool quat_inv(double qin[4], double qout[4])
{
	double n = vector_norm(qin, 4);

	if(n < 1.0e-12)
	{
		vector_copy(qin, qout, 4);
		return false;
	}
	vector_scale(qin, -1.0 / n, qout, 4);
	qout[0] *= -1.0;
	return true;
}

void quat_mult(double qa[4], double qb[4], double qout[4])
{
	double q[4];

	q[0] = qa[0] * qb[0] - qa[1] * qb[1] - qa[2] * qb[2] - qa[3] * qb[3];
	q[1] = qa[0] * qb[1] + qa[1] * qb[0] + qa[2] * qb[3] - qa[3] * qb[2];
	q[2] = qa[0] * qb[2] - qa[1] * qb[3] + qa[2] * qb[0] + qa[3] * qb[1];
	q[3] = qa[0] * qb[3] + qa[1] * qb[2] - qa[2] * qb[1] + qa[3] * qb[0];
	vector_copy(q, qout, 4);
}

double quat_dist(double q1[4], double q2[4])
{
	double q1i[4], q12[4];

	if(quat_inv(q1, q1i))
	{
		quat_mult(q1i, q2, q12);
		double res = 2.0 * atan2(vector_norm(q12 + 1, 3), q12[0]);
		res = fabs(res) > 2.0 * M_PI ? res - SIGN(res) * 2.0 * M_PI : res;
		res = fabs(res) > M_PI ? res - SIGN(res) * 2.0 * M_PI : res;

		return res;
	}
	else
	{
		return 0;
	}
}

void quat_slerp(double  q1[4], double q2[4], double t, double qout[4])
{
	t = CLIP(t, 0.0, 1.0);
	double qm[4] = {0};
	double dot = vector_dot(q1, q2, 4);
	double ratio1 = (1 - t);
	double ratio2 = t;

	if(t == 0)
	{
		vector_copy(q1, qout, 4);
		return;
	}

	if(fabs(t - 1.0) < 1e-12)
	{
		vector_copy(q2, qout, 4);
		return;
	}

	if(fabs(dot) < 1.0)
	{
		double theta = acos(CLIP(dot, -1.0, 1.0));
		ratio1 = sin((1 - t) * theta) / sin(theta);
		ratio2 = sin(t * theta) / sin(theta);
	}
	qm[0] = q1[0] * ratio1 + q2[0] * ratio2;
	qm[1] = q1[1] * ratio1 + q2[1] * ratio2;
	qm[2] = q1[2] * ratio1 + q2[2] * ratio2;
	qm[3] = q1[3] * ratio1 + q2[3] * ratio2;
	vector_copy(qm, qout, 4);
}

void quat_log(double qin[4], double qout[4])
{
	double n, vn, vni;
	double qlog[4];

	vn = vector_norm(qin + 1, 3);
	vni = vn < 1.0e-12 ? 0 : 1.0 / vn;
	n = vector_norm(qin, 4);
	qlog[0] = log(n);
	vector_scale(qin + 1, acos(CLIP(qin[0] / n, -1.0, 1.0)) * vni, qlog + 1, 3);
	vector_copy(qlog, qout, 4);
}

void quat_slerp_vel(double  q1[4], double q2[4], double rpy[3])
{
	double q1i[4], qout[4];
	if(quat_inv(q1, q1i))
	{
		quat_mult(q2, q1i, qout);
		quat_log(qout, qout);
		vector_scale(qout + 1, 2.0, rpy, 3);
	}
	else
	{
		vector_set(rpy, 0, 3);	
	}
}

















