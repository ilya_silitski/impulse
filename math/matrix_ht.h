#ifndef MATRIX_HT_H_
#define MATRIX_HT_H_

//homogeneous transformation matrix tricks

#include "matrix.h"
#include "vector.h"

static inline void matrix_rotx_3x3(double roll, double res[3][3]) 
{
    double s = sin(roll);
    double c = cos(roll);

    double R[3][3] = {{1, 0, 0},
                      {0, c, -s},
                      {0, s, c}};

	matrix_copy_3x3(R, res);
}

static inline void matrix_roty_3x3(double pitch, double res[3][3]) 
{
    double s = sin(pitch);
    double c = cos(pitch);

    double R[3][3] = {{c,  0, s},
                      {0,  1, 0},
                      {-s, 0, c}};

	matrix_copy_3x3(R, res);
}

static inline void matrix_rotz_3x3(double yaw, double res[3][3]) 
{

    double s = sin(yaw);
    double c = cos(yaw);

    double R[3][3] = {{c, -s, 0},
                      {s, c,  0},
                      {0, 0,  1}};

	matrix_copy_3x3(R, res);
}

static inline void matrix_ht2r(double a[4][4], double b[3][3]) 
{
    for (int i = 0; i < 3; i++) 
    {
        for (int j = 0; j < 3; j++) 
        {
        	b[i][j] = a[i][j];
        }
    }
}

static inline void matrix_ht2t(double a[4][4], double b[3]) 
{
	for (int i = 0; i < 3; i++) 
	{
		b[i] = a[i][3];
	}
}


static inline void matrix_rt2ht(double r[3][3], double t[3], double res[4][4])
{
	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			res[i][j] = r[i][j];
		}
		res[3][i] = 0;
		res[i][3] = t[i];
	}
	res[3][3] = 1.0;
}


static inline void matrix_ht_inv(double a[4][4], double res[4][4])
{
	double r[3][3];
	double t[3];

	matrix_ht2r(a, r);
	matrix_ht2t(a, t);

	matrix_transpose_3x3(r, r);
	matrix_mult_vector_3x3(r, t, t);

	vector_scale(t, -1.0, t, 3);

	matrix_rt2ht(r, t, res);
}


#endif
