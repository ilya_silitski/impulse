//
// Created by mariya.novik on 17.04.2020.
//

#ifndef LOOP_POSITION_H
#define LOOP_POSITION_H

#ifdef __cplusplus
extern "C" {
#endif

void tr_to_position(double matrix[4][4], double p[6]);
void position_to_tr(double p[6], double result[4][4]);
void rpy_to_r(double p[3], double result[3][3]);
void r_to_rpy(double result[3][3], double p[3]);

#ifdef __cplusplus
}
#endif

#endif //LOOP_POSITION_H
