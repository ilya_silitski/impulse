import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Drawer {
    id: keyboard
    width: window.width
    dragMargin: 0
    edge: Qt.BottomEdge

    property bool shifted: false
    property bool locked: false
    property string mode
    property string headerText: "Header"
    property string subheaderText: "Subheader"
    property string enterText: "Enter"
    property string defaultText: ""
    property var names
    property string pointId
    property string variableId
    property int ioIndex
    property string toolId
    property string payloadId

    signal save(string result)

    function show(mode) {
        keyboard.mode = mode;
        switch (mode) {
        case "newProgram":
            headerText = "Create New Program";
            subheaderText = "Enter name of new program or click Create to use default name.";
            enterText = "Create";
            input.text = input.generateUniqueName();
            input.selectAll();
            break;
        case "comment":
            headerText = "Enter comment";
            subheaderText = "Enter text of comment. It will be shown in program tree insted of Command text.";
            enterText = "Save";
            input.text = defaultText;
            input.cursorPosition = defaultText.length;
            break;
        case "expression":
            headerText = "Type your expression";
            subheaderText = "Enter text of expression. It will be executed when program is running.";
            enterText = "Save";
            input.text = defaultText;
            input.cursorPosition = defaultText.length;
            break;
        case "message":
            headerText = "Enter message";
            subheaderText = "Enter text of message. It will appear in popup that will be shown when command is executed.";
            enterText = "Save";
            input.text = defaultText;
            input.cursorPosition = defaultText.length;
            break;
        case "buttonText":
            headerText = "Enter button text";
            subheaderText = "Enter text of button in popup. It will be in popup's button that will be shown when command is executed.";
            enterText = "Save";
            input.text = defaultText;
            input.cursorPosition = defaultText.length;
            break;
        case "renamePoint":
        case "renameVariable":
        case "renameTool":
        case "renamePayload":
        case "renameDigitalInput":
        case "renameDigitalOutput":
        case "renameAnalogInput":
        case "renameAnalogOutput":
            switch (mode) {
            case "renamePoint": headerText = "Rename Point"; subheaderText = "Enter new name of Point."; break;
            case "renameVariable": headerText = "Rename Variable"; subheaderText = "Enter new name of Variable."; break;
            case "renameTool": headerText = "Rename Tool"; subheaderText = "Enter new name of Tool."; break;
            case "renamePayload": headerText = "Rename Payload"; subheaderText = "Enter new name of Payload."; break;
            case "renameDigitalInput": headerText = "Rename Digital Input"; subheaderText = "Enter new name of Digital Input."; break;
            case "renameDigitalOutput": headerText = "Rename Digital Output"; subheaderText = "Enter new name of Digital Output."; break;
            case "renameAnalogInput": headerText = "Rename Analog Input"; subheaderText = "Enter new name of Analog Input."; break;
            case "renameAnalogOutput": headerText = "Rename Analog Output"; subheaderText = "Enter new name of Analog Output."; break;
            }
            enterText = "Save";
            input.text = defaultText;
            input.selectAll();
            break;
        }

        keyboard.open();
    }

    onVisibleChanged: if (!visible) input.error = ""

    background: Rectangle {
        color: "#f2f2f2"
        border.width: 0;
        radius: input.height / 2
        anchors.fill: parent

        Rectangle {
            color: parent.color
            border.width: parent.border.width
            height: parent.height / 2
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.right: parent.right
        }
    }

    Column {
        width: parent.width
        spacing: 12
        padding: 12

        Text {
            id: header
            text: headerText
            font.pixelSize: 18
            font.weight: Font.DemiBold
            height: 42
            anchors.left: parent.left
            anchors.right: parent.right
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            id: subheader
            text: input.error !== "" ? input.error : subheaderText
            color: input.error !== "" ? "#f5222d" : "#000000"
            opacity: input.error !== "" ? 1 : 0.65
            font.pixelSize: 14
            height: 24
            anchors.left: parent.left
            anchors.right: parent.right
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        TextField {
            id: input
            height: 48
            width: parent.width - parent.padding * 2
            focus: true
            font.pixelSize: 20
            leftPadding: 16
            topPadding: 8
            rightPadding: 16
            bottomPadding: 8

            function generateUniqueName() {
                const names = window.programsList.map(p => p.name);
                for (let i = 1; ; i++) {
                    if (!names.includes("New_program_" + i)) return "New_program_" + i;
                }
            }

            function validateResult() {
                const result = input.text.trim();
                switch (mode) {
                case "newProgram":
                    if (result === "") {
                        input.error = "Program name cannot be empty."
                    } else if (window.programsList.map(p => p.name).includes(result)) {
                        input.error = "Program with such name already exists."
                    } else if (!/^[0-9a-zA-Z_]*$/.test(result)) {
                        input.error = "Program name must contain only letters, digits and underlines."
                    } else {
                        input.error = "";
                    }
                    break;
                case "buttonText":
                    if (result === "") {
                        input.error = "Button text cannot be empty."
                    } else if (result.length > 12) {
                        input.error = "Button text should be not more than 12 characters."
                    } else {
                        input.error = "";
                    }
                    break;
                case "renamePoint":
                case "renameVariable":
                case "renameTool":
                case "renamePayload":
                case "renameDigitalInput":
                case "renameAnalogInput":
                case "renameDigitalOutput":
                case "renameAnalogOutput":
                    let instanceName = "[UNKNOWN]";
                    switch (mode) {
                    case "renamePoint": instanceName = "Point"; break;
                    case "renameVariable": instanceName = "Variable"; break;
                    case "renameTool": instanceName = "Tool"; break;
                    case "renamePayload": instanceName = "Payload"; break;
                    case "renameDigitalInput": instanceName = "Digital Input"; break;
                    case "renameDigitalOutput": instanceName = "Digital Output"; break;
                    case "renameAnalogInput": instanceName = "Analog Input"; break;
                    case "renameAnalogOutput": instanceName = "Analog Output"; break;
                    }
                    if (result === "") {
                        input.error = instanceName + "'s name cannot be empty."
                    } else if (result === defaultText) {
                        input.error = instanceName + "'s name cannot be the same."
                    } else if (names.includes(result)) {
                        input.error = instanceName + " with such name already exists."
                    } else if (!/^[0-9a-zA-Z_]*$/.test(result)) {
                        input.error = instanceName + " name must contain only letters, digits and underlines."
                    } else if (!/^[a-zA-Z_]/.test(result)) {
                        input.error = instanceName + " name must start with letter or underline."
                    } else {
                        input.error = "";
                    }
                    break;
                }
            }

            property string error: ""

            background: Rectangle {
                anchors.fill: parent
                border.width: 1
                border.color: input.error === "" ? "#40a9ff" : "#f5222d"
                radius: parent.height / 2
                color: "#ffffff"
            }
        }

        ColumnLayout {
            id: keysColumn
            width: parent.width - parent.padding * 2
            spacing: 6

            Repeater {
                model: keyboard.model
                delegate: keyboardRow
            }
        }
    }

    Component {
        id: keyboardRow
        RowLayout {
            spacing: 6
            Layout.alignment: Qt.AlignCenter

            Repeater {
                model: modelData
                delegate: keyButton
            }
        }
    }

    Component {
        id: keyButton
        Button {
            id: keyBase
            focusPolicy: Qt.NoFocus
            Layout.preferredHeight: 54
            Layout.preferredWidth: modelData.keyWidth || 78
            Layout.fillWidth: !!modelData.fillWidth
            checkable: modelData.action === "CAPS LOCK" || modelData.action === "SHIFT"
            checked: modelData.action === "CAPS LOCK" && keyboard.locked || modelData.action === "SHIFT" && keyboard.shifted && !keyboard.locked

            contentItem: Loader {
                property var primary: modelData.primary
                property var secondary: modelData.secondary
                property var showSecondary: modelData.showSecondary
                property var action: modelData.action
                sourceComponent: modelData.special ? specialKey : regularKey
            }

            background: Rectangle {
                anchors.fill: parent
                radius: parent.height / 2
                border.color: "#d9d9d9"
                border.width: ["ENTER", "ESCAPE"].includes(modelData.action) ? 0 : 1
                color: {
                    if (modelData.action === "ENTER") return input.error !== "" ? "#d9d9d9" : parent.down ? Qt.darker("#4682b4", 1.25) : "#4682b4";
                    if (modelData.action === "ESCAPE") return parent.down ? Qt.darker("#ff3333", 1.25) : "#ff3333";
                    return parent.down ? "#e8e8e8" : keyBase.checked ? "#eeeeee" : "#ffffff";
                }
            }

            function handleClick() {
                switch (modelData.action){
                case "SHIFT":
                    if (keyboard.locked) {
                        keyboard.locked = false;
                    } else {
                        keyboard.shifted = !keyboard.shifted;
                    }
                    break;
                case "CAPS LOCK":
                    if (!keyboard.locked && keyboard.shifted) {
                        keyboard.locked = true;
                    } else {
                        keyboard.shifted = !keyboard.shifted;
                        keyboard.locked = !keyboard.locked;
                    }
                    break;
                case "ENTER":
                    if (input.error) return;
                    keyboard.save(input.text.trim());
                    keyboard.close();
                    break;
                case "ESCAPE":
                    keyboard.close();
                    break;
                }
            }

            function handlePress() {
//                const cursor = input.cursorPosition;
//                if (modelData.special) {
//                    switch (modelData.action) {
//                        case "BACKSPACE":
//                            if (input.selectionStart === input.selectionEnd) {
//                                input.text = input.text.substring(0, input.cursorPosition - 1) + input.text.substring(input.cursorPosition);
//                                input.cursorPosition = cursor - 1;
//                            } else {
//                                input.text = input.text.substring(0, input.selectionStart) + input.text.substring(input.selectionEnd);
//                                input.cursorPosition = input.selectionStart;
//                            }
//                            break;
//                        case "LEFT":
//                            if (input.selectionStart === input.selectionEnd) {
//                                input.cursorPosition = cursor - 1;
//                            } else {
//                                input.cursorPosition = input.selectionStart;
//                            }
//                            break;
//                        case "RIGHT":
//                            if (input.selectionStart === input.selectionEnd) {
//                                input.cursorPosition = cursor + 1;
//                            } else {
//                                input.cursorPosition = input.selectionEnd;
//                            }
//                            break;
//                    }
//                } else {
//                    const key = keyboard.shifted ? modelData.secondary : modelData.primary;
//                    if (input.selectionStart === input.selectionEnd) {
//                        input.text = input.text.substring(0, input.cursorPosition) + key + input.text.substring(input.cursorPosition);
//                        input.cursorPosition = cursor + 1;
//                    } else {
//                        input.text = input.text.substring(0, input.selectionStart) + key + input.text.substring(input.selectionEnd);
//                        input.cursorPosition = input.selectionStart + 1;
//                    }
//                    if (keyboard.shifted && !keyboard.locked) keyboard.shifted = false;
//                }
                input.validateResult();
            }

            Timer {
                id: repeatTimer
                interval: 50; repeat: true; triggeredOnStart: true
                onTriggered: parent.handlePress();
            }

            onClicked: handleClick()

            onPressed: handlePress()

            onPressAndHold: if (["BACKSPACE", "LEFT", "RIGHT"].includes(modelData.action)) repeatTimer.start()

            onReleased: if (repeatTimer.running) repeatTimer.stop()
        }
    }

    Component {
        id: regularKey
        Text {
            id: regularKeyText
            text: keyboard.shifted ? parent.secondary : parent.primary
            height: 32
            font.pixelSize: 20
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            Text {
                visible: regularKeyText.parent.showSecondary === true
                text: keyboard.shifted ? regularKeyText.parent.primary : regularKeyText.parent.secondary
                height: 16
                font.pixelSize: 12
                color: "#888888"
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.rightMargin: 8
            }
        }
    }

    Component {
        id: specialKey
        Text {
            text: parent.action === "ENTER" ? keyboard.enterText : parent.primary
            height: 32
            font.pixelSize: 20
            anchors.centerIn: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: {
                if (parent.action === "ENTER") return input.error !== "" ? "#eeeeee" : "#ffffff"
                if (parent.action === "ESCAPE") return "#ffffff"
                return "#000000"
            }
        }
    }

    readonly property var model: [
        [
            {
                primary: "Esc",
                action: "ESCAPE",
                special: true,
            },
            {
                primary: "1",
                secondary: "!",
                showSecondary: true,
            },
            {
                primary: "2",
                secondary: "@",
                showSecondary: true,
            },
            {
                primary: "3",
                secondary: "#",
                showSecondary: true,
            },
            {
                primary: "4",
                secondary: "$",
                showSecondary: true,
            },
            {
                primary: "5",
                secondary: "%",
                showSecondary: true,
            },
            {
                primary: "6",
                secondary: "^",
                showSecondary: true,
            },
            {
                primary: "7",
                secondary: "&",
                showSecondary: true,
            },
            {
                primary: "8",
                secondary: "*",
                showSecondary: true,
            },
            {
                primary: "9",
                secondary: "(",
                showSecondary: true,
            },
            {
                primary: "0",
                secondary: ")",
                showSecondary: true,
            },
            {
                primary: "-",
                secondary: "_",
                showSecondary: true,
            },
            {
                primary: "=",
                secondary: "+",
                showSecondary: true,
            },
            {
                fillWidth: true,
                primary: "← Backspace",
                action: "BACKSPACE",
                special: true,
            }
        ],
        [
            {
                primary: "`",
                secondary: "~",
                showSecondary: true,
            },
            {
                primary: "q",
                secondary: "Q",
            },
            {
                primary: "w",
                secondary: "W",
            },
            {
                primary: "e",
                secondary: "E",
            },
            {
                primary: "r",
                secondary: "R",
            },
            {
                primary: "t",
                secondary: "T",
            },
            {
                primary: "y",
                secondary: "Y",
            },
            {
                primary: "u",
                secondary: "U",
            },
            {
                primary: "i",
                secondary: "I",
            },
            {
                primary: "o",
                secondary: "O",
            },
            {
                primary: "p",
                secondary: "P",
            },
            {
                primary: "[",
                secondary: "{",
                showSecondary: true,
            },
            {
                primary: "]",
                secondary: "}",
                showSecondary: true,
            },
            {
                primary: "\\",
                secondary: "|",
                showSecondary: true,
            }
        ],
        [
            {
                keyWidth: 160,
                primary: "Caps Lock",
                action: "CAPS LOCK",
                special: true,
            },
            {
                primary: "a",
                secondary: "A",
            },
            {
                primary: "s",
                secondary: "S",
            },
            {
                primary: "d",
                secondary: "D",
            },
            {
                primary: "f",
                secondary: "F",
            },
            {
                primary: "g",
                secondary: "G",
            },
            {
                primary: "h",
                secondary: "H",
            },
            {
                primary: "j",
                secondary: "J",
            },
            {
                primary: "k",
                secondary: "K",
            },
            {
                primary: "l",
                secondary: "L",
            },
            {
                primary: ";",
                secondary: ":",
                showSecondary: true,
            },
            {
                primary: "'",
                secondary: "\"",
                showSecondary: true,
            },
            {
                fillWidth: true,
                primary: "Submit",
                action: "ENTER",
                special: true,
            }
        ],
        [
            {
                keyWidth: 192,
                primary: "Shift",
                action: "SHIFT",
                special: true,
            },
            {
                primary: "z",
                secondary: "Z",
            },
            {
                primary: "x",
                secondary: "X",
            },
            {
                primary: "c",
                secondary: "C",
            },
            {
                primary: "v",
                secondary: "V",
            },
            {
                primary: "b",
                secondary: "B",
            },
            {
                primary: "n",
                secondary: "N",
            },
            {
                primary: "m",
                secondary: "M",
            },
            {
                primary: ",",
                secondary: "<",
                showSecondary: true,
            },
            {
                primary: ".",
                secondary: ">",
                showSecondary: true,
            },
            {
                primary: "/",
                secondary: "?",
                showSecondary: true,
            },
            {
                fillWidth: true,
                primary: "Shift",
                action: "SHIFT",
                special: true,
            }
        ],
        [
            {
                fillWidth: true,
                primary: " ",
                secondary: " ",
            },
            {
                primary: "<",
                action: "LEFT",
                special: true,
            },
            {
                primary: ">",
                action: "RIGHT",
                special: true,
            }
        ]
    ]

}
