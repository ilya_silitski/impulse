import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import "../Elements"

Popup {
    id: numpad
    width: 350
    height: 446
    padding: 12
    margins: 0

    signal result(real result)

    function setDefaultValue(value) {
        input.text = value;
    }

    background: Rectangle {
        anchors.fill: parent
        border.width: 1
        border.color: "#d9d9d9"
        radius: 12
    }

    enter: Transition {
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; duration: 100 }
    }

    exit: Transition {
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0; duration: 100 }
    }

    onVisibleChanged: {
        if (visible) {
            input.forceActiveFocus();
            input.selectAll();
        }
    }

    TextField {
        id: input
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        height: 48
        focus: true
        font.pixelSize: 20
        leftPadding: 16
        topPadding: 8
        rightPadding: 16
        bottomPadding: 8

        property string error: ""

        background: Rectangle {
            anchors.fill: parent
            border.width: 1
            border.color: input.error === "" ? "#40a9ff" : "#f5222d"
            radius: parent.height / 2
            color: "#ffffff"

            Text {
                visible: input.error !== ""
                text: input.error
                color: "#f5222d"
                height: 12
                anchors.left: parent.left
                anchors.top: parent.bottom
                anchors.right: parent.right
                anchors.topMargin: 6
                font.pixelSize: 12
            }
        }
    }

    Component {
        id: textButton
        Text {
            text: key
            font.pixelSize: 20
            font.weight: Font.Normal
            color: "#a6000000"
        }
    }

    Component {
        id: iconButton
        Image {
            source: "qrc:///img/" + key
            height: 20
            width: height
        }
    }

    GridView {
        id: keys
        interactive: false
        width: 338
        height: 300
        anchors.left: parent.left
        anchors.top: input.bottom
        anchors.right: parent.right
        anchors.leftMargin: -6
        anchors.topMargin: 18
        anchors.rightMargin: -6

        cellHeight: 60
        cellWidth: 84
        model: ["(", ")", "*", "/", "7", "8", "9", "-", "4", "5", "6", "+", "1", "2", "3", "backspace", "0", ".", "left", "right"]

        delegate: Item {
            height: 60
            width: 84

            property bool down: false

            Rectangle {
                anchors.fill: parent
                anchors.margins: 6
                border.width: 1
                border.color: parent.down ? "#cccccc" : "#d9d9d9"
                color: parent.down ? "#eeeeee" : "#ffffff"
                radius: parent.height / 2
            }

            Loader {
                property string key: modelData
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                sourceComponent: modelData.length > 1 ? iconButton : textButton
            }

            MouseArea {
                anchors.fill: parent
                anchors.margins: 6
                onPressed: parent.down = true;
                onReleased: parent.down = false;
                onClicked: {
//                    const cursor = input.cursorPosition;
//                    switch (modelData) {
//                        case "backspace":
//                            if (input.selectionStart === input.selectionEnd) {
//                                input.text = input.text.substring(0, input.cursorPosition - 1) + input.text.substring(input.cursorPosition);
//                                input.cursorPosition = cursor - 1;
//                            } else {
//                                input.text = input.text.substring(0, input.selectionStart) + input.text.substring(input.selectionEnd);
//                                input.cursorPosition = input.selectionStart;
//                            }
//                            break;
//                        case "left":
//                            if (input.selectionStart === input.selectionEnd) {
//                                input.cursorPosition = cursor - 1;
//                            } else {
//                                input.cursorPosition = input.selectionStart;
//                            }
//                            break;
//                        case "right":
//                            if (input.selectionStart === input.selectionEnd) {
//                                input.cursorPosition = cursor + 1;
//                            } else {
//                                input.cursorPosition = input.selectionEnd;
//                            }
//                            break;
//                        default:
//                            if (input.selectionStart === input.selectionEnd) {
//                                input.text = input.text.substring(0, input.cursorPosition) + modelData + input.text.substring(input.cursorPosition);
//                                input.cursorPosition = cursor + 1;
//                            } else {
//                                input.text = input.text.substring(0, input.selectionStart) + modelData + input.text.substring(input.selectionEnd);
//                                input.cursorPosition = input.selectionStart + 1;
//                            }
//                            break;
//                    }
                    try {
                        if (typeof Function("return (" + input.text + ");")() !== "number") throw {message: "Invalid"};
                        input.error = "";
                    } catch (e) {
                        if (input.text === "") {
                            input.error = "Cannot be empty";
                        } else if (e.message.startsWith("Unexpected") || e.message.startsWith("Invalid") || e.message.startsWith("Parse")) {
                            input.error = "Invalid expression";
                        } else {
                            input.error = e.message;
                        }
                    }
                }
            }

        }
    }

    onClosed: {
        input.error = "";
    }

    RowLayout {
        id: buttonsRow
        spacing: 12
        height: 48
        anchors.left: parent.left
        anchors.top: keys.bottom
        anchors.right: parent.right
        anchors.topMargin: 6

        Button {
            id: cancelButton
            Layout.fillHeight: true
            Layout.fillWidth: true

            background: Rectangle {
                anchors.fill: parent
                border.width: 1
                border.color: "#e03943"
                color: parent.down ? "#d7222b" : "#f5222d"
                radius: parent.height / 2
            }

            Icon {
                source: "qrc:///img/close"
                size: 24
                color: "#ffffff"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
            }

            onClicked: {
                numpad.close();
            }
        }

        Button {
            id: okButton
            Layout.fillHeight: true
            Layout.fillWidth: true

            background: Rectangle {
                anchors.fill: parent
                border.width: 1
                border.color: "#62c635"
                color: parent.down ? "#52c41a" : "#55ce0c"
                radius: parent.height / 2
            }

            Icon {
                source: "qrc:///img/check"
                size: 24
                color: "#ffffff"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
            }

            onClicked: {
                try {
                    numpad.result(Function("return (" + input.text + ");")());
                } catch (e) {
                    console.log(e.message);
                }
                numpad.close();
            }
        }
    }
}
