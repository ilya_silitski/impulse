import QtQuick
import QtQuick.Controls
import "../Elements"

Popup {
    id: popup
    modal: true
    width: 360
    padding: 24
    anchors.centerIn: Overlay.overlay

    property string title: "Title"
    property string description: "Description"
    property string okText: "Ok"
    property string cancelText: "Cancel"
    property int okButtonType: PushButton.Primary
    property int cancelButtonType: PushButton.Danger

    signal ok()
    signal cancel()

    enter: Transition {
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; duration: 100 }
    }

    exit: Transition {
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0; duration: 100 }
    }

    background: Rectangle {
        color: "#ffffff"
        border.width: 0
        radius: 12
        anchors.fill: parent
    }

    Column {
        anchors.fill: parent
        spacing: 24

        Text {
            text: popup.title
            font.pixelSize: 18
            font.weight: Font.DemiBold
            height: 36
            anchors.left: parent.left
            anchors.right: parent.right
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: popup.description
            wrapMode: Text.WordWrap
            color: "#555555"
            font.pixelSize: 14
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 24
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Row {
            spacing: 24
            anchors.horizontalCenter: parent.horizontalCenter

            PushButton {
                text: cancelText
                type: cancelButtonType

                onClicked: popup.cancel()
            }

            PushButton {
                text: okText
                type: okButtonType

                onClicked: popup.ok()
            }
        }
    }
}

