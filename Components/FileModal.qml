import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import "../Elements"

//function parseSize(size, dimension = 0) {
//    return size >= 1024 ? parseSize(size / 1024, dimension + 1) : parseFloat(size.toPrecision(3)) + [" B", " KB", " MB"][dimension];
//}
//Qt.formatDateTime(modelData.created, "dd.MM.yyyy hh:mm:ss")

Popup {
    id: popup
    modal: true
    height: 480
    width: 520
    padding: 12
    anchors.centerIn: Overlay.overlay

    enter: Transition {
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; duration: 100 }
    }

    exit: Transition {
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0; duration: 100 }
    }

    background: Rectangle {
        color: "#ffffff"
        border.width: 0
        radius: 12
        anchors.fill: parent
    }

    onAboutToShow: {
        list.currentIndex = -1;
    }

    signal selected(string programName)

    function onCancel() {
        popup.close();
    }

    function onOpen() {
        const programName = window.programsList[list.currentIndex].name;
        popup.selected(programName);
        popup.close();
    }

    Component {
        id: listItem

        Text {
            text: modelData.name
            height: 36
            width: !!parent ? parent.width : 0
            leftPadding: 12
            rightPadding: 12
            clip: true
            elide: Text.ElideRight
            font.pixelSize: 14
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            color: ListView.isCurrentItem ? "#ffffff" : "#000000"

            MouseArea {
                anchors.fill: parent
                onClicked: list.currentIndex = index
                onDoubleClicked: popup.onOpen()
            }
        }
    }

    ColumnLayout {
        id: header
        height: 96
        width: parent.width


        Text {
            text: "Open Program"
            Layout.preferredHeight: 36
            Layout.topMargin: 18
            Layout.alignment: Qt.AlignCenter
            font.pixelSize: 18
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "Select program and press Open or double click program to open"
            color: "#555555"
            Layout.preferredHeight: 24
            Layout.bottomMargin: 18
            Layout.alignment: Qt.AlignCenter
            font.pixelSize: 14
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }


    ListView {
        id: list
        anchors.left: parent.left
        anchors.top: header.bottom
        anchors.right: parent.right
        anchors.bottom: footer.top
        anchors.topMargin: 12
        anchors.bottomMargin: 12
        clip: true
        highlightFollowsCurrentItem: true
        highlightMoveDuration: 0
        highlightMoveVelocity: -1
        spacing: 12
        currentIndex: -1

        model: window.programsList

        delegate: listItem

        highlight: Rectangle { color: "#4682b4"; radius: 18; border.width: 0 }
    }

    Row {
        id: footer
        spacing: 12
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        PushButton {
            width: 96
            text: "Cancel"

            onClicked: popup.onCancel()
        }

        PushButton {
            enabled: list.currentIndex !== -1
            width: 96
            text: "Open"
            type: PushButton.Primary

            onClicked: popup.onOpen()
        }
    }
}
