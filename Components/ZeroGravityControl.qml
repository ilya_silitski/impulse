import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import "../Elements"

Rectangle {
    id: zeroGravity
    height: open ? column.height + 12 : 48
    width: 420
    color: "#ffffff"
    border.color: "#e8e8e8"
    radius: open ? 12 : (height / 2)
    clip: true

    signal started()
    signal stopped()

    enum Mode {
        Free,
        Translate,
        Rotate,
        Plane,
        Custom
    }

    property bool disabled: false
    property bool open: false
    property bool active: false
    property bool inTCP: false
    property bool tx: true
    property bool ty: true
    property bool tz: true
    property bool rx: true
    property bool ry: true
    property bool rz: true
    property int mode: {
        if (tx && ty && tz && rx && ry && rz) return ZeroGravityControl.Mode.Free;
        else if (tx && ty && tz && !rx && !ry && !rz) return ZeroGravityControl.Mode.Translate;
        else if (!tx && !ty && !tz && rx && ry && rz) return ZeroGravityControl.Mode.Rotate;
        else if (tx && ty && !tz && !rx && !ry && !rz) return ZeroGravityControl.Mode.Plane;
        else return ZeroGravityControl.Mode.Custom;
    }

    Behavior on radius { NumberAnimation { duration: 100 } }

    Behavior on height { NumberAnimation { duration: 100 } }

    Column {
        id: column
        spacing: 12

        Button {
            height: 48
            width: zeroGravity.width

            contentItem: Item {
                anchors.fill: parent

                Text {
                    text: "Zero Gravity"
                    height: 36
                    font.pixelSize: 18
                    color: "#555555"
                    anchors.centerIn: parent
                    verticalAlignment: Text.AlignVCenter

                    Icon {
                        size: 18
                        width: 36
                        height: width
                        rotation: zeroGravity.open ? 90 : -90
                        color: "#555555"
                        source: "qrc:///img/left"
                        anchors.left: parent.right

                        Behavior on rotation { NumberAnimation { duration: 200 } }
                    }
                }
            }

            background: Rectangle {
                anchors.fill: parent
                color: "transparent"
                border.width: 0
            }

            onClicked: {
                zeroGravity.open = !zeroGravity.open
            }
        }

        Component {
            id: zeroGravityLabel

            Text {
                height: 48
                text: "Zero Gravity Active"
                anchors.left: parent.left
                anchors.right: parent.right
                font.pixelSize: 24
                color: "#4682b4"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }

        Component {
            id: zeroGravityButton

            PushButton {
                enabled: !zeroGravity.disabled
                text: down ? "Release to Disable" : "Enable"
                type: down ? PushButton.Danger : PushButton.Primary
                size: PushButton.Large
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: 12

                onPressed: {
                    zeroGravity.started();
                }
                onReleased: {
                    zeroGravity.stopped();
                }
                onCanceled: {
                    zeroGravity.stopped();
                }
                onHoveredChanged: {
                    if (!hovered && down) zeroGravity.stopped();
                }
            }
        }

        Loader {
            anchors.left: parent.left
            anchors.right: parent.right
            sourceComponent: zeroGravity.active ? zeroGravityLabel : zeroGravityButton
        }

        Row {
            spacing: 12
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 12

            Button {
                id: freeButton
                height: 90
                width: height

                property bool active: zeroGravity.mode === ZeroGravityControl.Mode.Free

                contentItem: Item {
                    anchors.fill: parent;

                    Column {
                        anchors.centerIn: parent
                        spacing: 6

                        Icon {
                            source: "qrc:///img/move"
                            size: 36
                            height: 36
                            width: height
                            color: freeButton.active ? "#4682b4" : "#555555"
                            anchors.horizontalCenter: parent.horizontalCenter
                        }

                        Text {
                            text: "Free"
                            font.pixelSize: 14
                            color: freeButton.active ? "#4682b4" : "#555555"
                            anchors.horizontalCenter: parent.horizontalCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }
                }

                background: Rectangle {
                    anchors.fill: parent
                    radius: 12
                    border.color: freeButton.active ? "#4682b4" : "#e8e8e8"
                    color: "#ffffff"
                }

                onClicked: {
                    zeroGravity.tx = true;
                    zeroGravity.ty = true;
                    zeroGravity.tz = true;
                    zeroGravity.rx = true;
                    zeroGravity.ry = true;
                    zeroGravity.rz = true;
                }
            }

            Button {
                id: translateButton
                height: 90
                width: height

                property bool active: zeroGravity.mode === ZeroGravityControl.Mode.Translate

                contentItem: Item {
                    anchors.fill: parent;

                    Column {
                        anchors.centerIn: parent
                        spacing: 6

                        Icon {
                            source: "qrc:///img/move"
                            size: 36
                            height: 36
                            width: height
                            color: translateButton.active ? "#4682b4" : "#555555"
                            anchors.horizontalCenter: parent.horizontalCenter
                        }

                        Text {
                            text: "Translate"
                            font.pixelSize: 14
                            color: translateButton.active ? "#4682b4" : "#555555"
                            anchors.horizontalCenter: parent.horizontalCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }
                }

                background: Rectangle {
                    anchors.fill: parent
                    radius: 12
                    border.color: translateButton.active ? "#4682b4" : "#e8e8e8"
                    color: "#ffffff"
                }

                onClicked: {
                    zeroGravity.tx = true;
                    zeroGravity.ty = true;
                    zeroGravity.tz = true;
                    zeroGravity.rx = false;
                    zeroGravity.ry = false;
                    zeroGravity.rz = false;
                }
            }

            Button {
                id: rotateButton
                height: 90
                width: height

                property bool active: zeroGravity.mode === ZeroGravityControl.Mode.Rotate

                contentItem: Item {
                    anchors.fill: parent;

                    Column {
                        anchors.centerIn: parent
                        spacing: 6

                        Icon {
                            source: "qrc:///img/move"
                            size: 36
                            height: 36
                            width: height
                            color: rotateButton.active ? "#4682b4" : "#555555"
                            anchors.horizontalCenter: parent.horizontalCenter
                        }

                        Text {
                            text: "Rotate"
                            font.pixelSize: 14
                            color: rotateButton.active ? "#4682b4" : "#555555"
                            anchors.horizontalCenter: parent.horizontalCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }
                }

                background: Rectangle {
                    anchors.fill: parent
                    radius: 12
                    border.color: rotateButton.active ? "#4682b4" : "#e8e8e8"
                    color: "#ffffff"
                }

                onClicked: {
                    zeroGravity.tx = false;
                    zeroGravity.ty = false;
                    zeroGravity.tz = false;
                    zeroGravity.rx = true;
                    zeroGravity.ry = true;
                    zeroGravity.rz = true;
                }
            }

            Button {
                id: palneButton
                height: 90
                width: height

                property bool active: zeroGravity.mode === ZeroGravityControl.Mode.Plane

                contentItem: Item {
                    anchors.fill: parent;

                    Column {
                        anchors.centerIn: parent
                        spacing: 6

                        Icon {
                            source: "qrc:///img/move"
                            size: 36
                            height: 36
                            width: height
                            color: palneButton.active ? "#4682b4" : "#555555"
                            anchors.horizontalCenter: parent.horizontalCenter
                        }

                        Text {
                            text: "Plane"
                            font.pixelSize: 14
                            color: palneButton.active ? "#4682b4" : "#555555"
                            anchors.horizontalCenter: parent.horizontalCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }
                }

                background: Rectangle {
                    anchors.fill: parent
                    radius: 12
                    border.color: palneButton.active ? "#4682b4" : "#e8e8e8"
                    color: "#ffffff"
                }

                onClicked: {
                    zeroGravity.tx = true;
                    zeroGravity.ty = true;
                    zeroGravity.tz = false;
                    zeroGravity.rx = false;
                    zeroGravity.ry = false;
                    zeroGravity.rz = false;
                }
            }
        }

        RowLayout {
            spacing: 12
            height: 36
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 12

            ToggleButton {
                Layout.fillWidth: true
                Layout.fillHeight: true

                text: "Base"
                active: zeroGravity.inTCP === false

                onClicked: zeroGravity.inTCP = false
            }

            ToggleButton {
                Layout.fillWidth: true
                Layout.fillHeight: true

                text: "Tool"
                active: zeroGravity.inTCP === true

                onClicked: zeroGravity.inTCP = true
            }
        }

        Grid {
            spacing: 12
            columns: 3
            rows: 2
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 12

            Switcher {
                text: "X"
                checkedColor: "#ff0000"
                textColor: "#ff0000"
                checked: zeroGravity.tx
                onCheckedChanged: zeroGravity.tx = checked
            }

            Switcher {
                width: 124
                text: "Y"
                checkedColor: "#00ff00"
                textColor: "#00ff00"
                checked: zeroGravity.ty
                onCheckedChanged: zeroGravity.ty = checked
            }

            Switcher {
                width: 124
                text: "Z"
                checkedColor: "#0000ff"
                textColor: "#0000ff"
                checked: zeroGravity.tz
                onCheckedChanged: zeroGravity.tz = checked
            }

            Switcher {
                width: 124
                text: "RX"
                checkedColor: "#ff0000"
                textColor: "#ff0000"
                checked: zeroGravity.rx
                onCheckedChanged: zeroGravity.rx = checked
            }

            Switcher {
                width: 124
                text: "RY"
                checkedColor: "#00ff00"
                textColor: "#00ff00"
                checked: zeroGravity.ry
                onCheckedChanged: zeroGravity.ry = checked
            }

            Switcher {
                width: 124
                text: "RZ"
                checkedColor: "#0000ff"
                textColor: "#0000ff"
                checked: zeroGravity.rz
                onCheckedChanged: zeroGravity.rz = checked
            }
        }
    }
}
