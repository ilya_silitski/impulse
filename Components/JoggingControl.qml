import QtQuick
import QtQuick.Controls
import "../Elements"

Item {
    id: control
    height: radius
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.margins: 12

    property int radius: 240
    property int radiusOuter: radius / 2
    property int radiusInner: radius / 4
    property color colorX: "#ff0000"
    property color colorY: "#00ff00"
    property color colorZ: "#0000ff"

    Item {
        id: rightJogging
        width: control.radius
        height: control.radius
        anchors.left: parent.left
        anchors.bottom: parent.bottom

        SectorJoggingButton {
            enabled: control.enabled
            anchors.bottom: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: "X+"
            placement: SectorJoggingButton.Placement.Top
            outerRadius: radiusOuter
            innerRadius: radiusInner
            color: colorX
            onPressed: backend.startJogging("x", true)
            onReleased: backend.stopJogging()
        }

        SectorJoggingButton {
            enabled: control.enabled
            anchors.bottom: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Y-"
            placement: SectorJoggingButton.Placement.Right
            outerRadius: radiusOuter
            innerRadius: radiusInner
            color: colorY
            onPressed: backend.startJogging("y", false)
            onReleased: backend.stopJogging()
        }

        SectorJoggingButton {
            enabled: control.enabled
            anchors.bottom: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: "X-"
            placement: SectorJoggingButton.Placement.Bottom
            outerRadius: radiusOuter
            innerRadius: radiusInner
            color: colorX
            onPressed: backend.startJogging("x", false)
            onReleased: backend.stopJogging()
        }

        SectorJoggingButton {
            enabled: control.enabled
            anchors.bottom: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: "+Y"
            placement: SectorJoggingButton.Placement.Left
            outerRadius: radiusOuter
            innerRadius: radiusInner
            color: colorY
            onPressed: backend.startJogging("y", true)
            onReleased: backend.stopJogging()
        }

        SemicircleJoggingButton {
            enabled: control.enabled
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.verticalCenter
            text: "Z+"
            placement: SemicircleJoggingButton.Placement.Top
            radius: radiusInner
            color: colorZ
            onPressed: backend.startJogging("z", true)
            onReleased: backend.stopJogging()
        }

        SemicircleJoggingButton {
            enabled: control.enabled
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.verticalCenter
            text: "Z-"
            placement: SemicircleJoggingButton.Placement.Bottom
            radius: radiusInner
            color: colorZ
            onPressed: backend.startJogging("z", false)
            onReleased: backend.stopJogging()
        }
    }

    Item {
        id: leftJogging
        width: control.radius
        height: control.radius
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        SectorJoggingButton {
            enabled: control.enabled
            anchors.bottom: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: "RY+"
            placement: SectorJoggingButton.Placement.Top
            outerRadius: radiusOuter
            innerRadius: radiusInner
            color: colorY
            onPressed: backend.startJogging("ry", true)
            onReleased: backend.stopJogging()
        }

        SectorJoggingButton {
            enabled: control.enabled
            anchors.bottom: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: "RX+"
            placement: SectorJoggingButton.Placement.Right
            outerRadius: radiusOuter
            innerRadius: radiusInner
            color: colorX
            onPressed: backend.startJogging("rx", true)
            onReleased: backend.stopJogging()
        }

        SectorJoggingButton {
            enabled: control.enabled
            anchors.bottom: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: "RY-"
            placement: SectorJoggingButton.Placement.Bottom
            outerRadius: radiusOuter
            innerRadius: radiusInner
            color: colorY
            onPressed: backend.startJogging("ry", false)
            onReleased: backend.stopJogging()
        }

        SectorJoggingButton {
            enabled: control.enabled
            anchors.bottom: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: "-RX"
            placement: SectorJoggingButton.Placement.Left
            outerRadius: radiusOuter
            innerRadius: radiusInner
            color: colorX
            onPressed: backend.startJogging("rx", false)
            onReleased: backend.stopJogging()
        }

        SemicircleJoggingButton {
            enabled: control.enabled
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.verticalCenter
            text: "+RZ"
            placement: SemicircleJoggingButton.Placement.Left
            radius: radiusInner
            color: colorZ
            onPressed: backend.startJogging("rz", true)
            onReleased: backend.stopJogging()
        }

        SemicircleJoggingButton {
            enabled: control.enabled
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.verticalCenter
            text: "RZ-"
            placement: SemicircleJoggingButton.Placement.Right
            radius: radiusInner
            color: colorZ
            onPressed: backend.startJogging("rz", false)
            onReleased: backend.stopJogging()
        }
    }
}
