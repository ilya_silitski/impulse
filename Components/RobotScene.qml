import QtQuick
import QtQuick3D
import "../Elements"

View3D {
    id: robotView
    anchors.fill: parent
    camera: camera
    focus: true

    state: "ROTATE"

    property string robotModel: window.robotModel
    property bool inTCP: false

    property alias isRotate: mouseArea.isRotate;

    property var urdf: {
        if (robotModel === "N7") {
            return [
                        {
                            position: [0, 0, 0.274],
                            rotation: [90, 0, 0]
                        },
                        {
                            position: [-0.65, 0, 0],
                            rotation: [0, 0, 0]
                        },
                        {
                            position: [-0.59, 0, 0],
                            rotation: [0, 0, 0]
                        },
                        {
                            position: [0, 0, 0.135],
                            rotation: [0, 0, 0]
                        },
                        {
                            position: [0, -0.127, 0],
                            rotation: [0, 0, 0]
                        },
                        {
                            position: [0, 0, 0],
                            rotation: [0, 0, 0.139]
                        }
                    ]
        } else if (robotModel === "Pulse75") {
            return [
                        {
                            position: [0, 0, 0.2245],
                            rotation: [90, 0, 0]
                        },
                        {
                            position: [-0.375, 0, 0],
                            rotation: [0, 0, 0]
                        },
                        {
                            position: [-0.295, 0, 0],
                            rotation: [0, 0, 0]
                        },
                        {
                            position: [0, 0, 0.1285],
                            rotation: [0, 90, 90]
                        },
                        {
                            position: [0, 0, 0.1726],
                            rotation: [0, 90, -90]
                        },
                        {
                            position: [0, 0, 0.1241],
                            rotation: [0, 0, 0]
                        }
                    ]
        } else if (robotModel === "eRob5") {
            return [
                        {
                            position: [0, 0, 0.1123],
                            rotation: [90, 0, 0]
                        },
                        {
                            position: [-0.425, 0, 0],
                            rotation: [0, 0, 0]
                        },
                        {
                            position: [-0.401, 0, 0],
                            rotation: [0, 0, 0]
                        },
                        {
                            position: [0, 0, 0.0856],
                            rotation: [90, 0, 0]
                        },
                        {
                            position: [0, 0, 0.086],
                            rotation: [-90, 0, 0]
                        },
                        {
                            position: [0, 0, 0.0725],
                            rotation: [0, 0, 0]
                        }
                    ]
        }
    }

    function setJoints(pose) {
        if (robotModel === "N7") {
            link1Model.eulerRotation.y = pose[0];
            link2Node.eulerRotation.z = pose[1];
            link3Node.eulerRotation.z = pose[2];
            link4Model.eulerRotation.z = pose[3];
            link5Model.eulerRotation.y = -pose[4];
        } else if (robotModel === "Pulse75") {
            link1Model.eulerRotation.y = pose[0];
            link2Node.eulerRotation.z = pose[1];
            link3Node.eulerRotation.z = pose[2];
            link4Model.eulerRotation.x = -(pose[3] - 90);
            link5Model.eulerRotation.x = -(pose[4] + 90);
            link6Model.eulerRotation.z = pose[5] - 90;
        } else if (robotModel === "eRob5") {
            link1Model.eulerRotation.y = pose[0];
            link2Node.eulerRotation.z = pose[1];
            link3Node.eulerRotation.z = pose[2];
            link4Node.eulerRotation.z = pose[3];
            link5Node.eulerRotation.z = pose[4];
            link6Model.eulerRotation.z = pose[5];
        }
    }

    function setTCP(position) {
        tcp.position.x = position[0];
        tcp.position.y = position[1];
        tcp.position.z = position[2];

        if (!inTCP) return tcp.rotation = Qt.quaternion(1, 0, 0, 0);

        const cy = Math.cos(position[5] * 0.5);
        const sy = Math.sin(position[5] * 0.5);
        const cp = Math.cos(position[4] * 0.5);
        const sp = Math.sin(position[4] * 0.5);
        const cr = Math.cos(position[3] * 0.5);
        const sr = Math.sin(position[3] * 0.5);

        tcp.rotation = Qt.quaternion(
                    cr * cp * cy + sr * sp * sy,
                    sr * cp * cy - cr * sp * sy,
                    cr * sp * cy + sr * cp * sy,
                    cr * cp * sy - sr * sp * cy
                    );
    }

    function zoom(value) {
        const toFixed = (v, d) => parseFloat((v).toFixed(d));
        const zoomMultiplier = 2;
        camera.zoomValue = toFixed((0.5 + (1 - value) * zoomMultiplier), 2);
    }

    function changeMode(rotate) {
        if (rotate) {
            mouseArea.isRotate = true;
            state = "ROTATE";
        } else {
            mouseArea.isRotate = false;
            state = "MOVE";
        }
    }

    states: [
        State {
            name: "ROTATE"
            PropertyChanges {target: cameraNode; eulerRotation.z: 45}
            PropertyChanges {target: camera; position.y: 0}
        },
        State {
            name: "MOVE"
            PropertyChanges {target: cameraNode; eulerRotation.z: 0}
            PropertyChanges {target: camera; position.y: 0}
        }
    ]

    transitions: [
        Transition {
               from: "ROTATE"
               to: "MOVE"
               RotationAnimation {target: cameraNode; duration: 300; property: "eulerRotation.z"; direction: RotationAnimation.Shortest}
               NumberAnimation {target: camera; duration: 300; property: "position.y"}
           },
           Transition {
               from: "MOVE"
               to: "ROTATE"
               RotationAnimation {target: cameraNode; duration: 300; property: "eulerRotation.z"; direction: RotationAnimation.Shortest}
               NumberAnimation {target: camera; duration: 300; property: "position.y"}
           }
    ]

    environment: SceneEnvironment {
        clearColor: "white"
        backgroundMode: SceneEnvironment.Color
        antialiasingMode: SceneEnvironment.MSAA
        antialiasingQuality: SceneEnvironment.High
    }

    DirectionalLight {
        eulerRotation.y: -90
        brightness: 0.8
    }

    DirectionalLight {
        eulerRotation.x: 90
        brightness: 0.8
    }

    DirectionalLight {
        eulerRotation.y: 90
        brightness: 0.8
    }

    DirectionalLight {
        eulerRotation.x: -90
        brightness: 0.8
    }

    PointLight {
        position: Qt.vector3d(0, 0, 2)
        brightness: 1
    }


    Node {
        id: cameraNode

        eulerRotation.z: 45

        PerspectiveCamera {
            id: camera

            property real zoomValue: 1.5

            position.x: zoomValue
            position.z: 0.5
            eulerRotation.x: 0
            eulerRotation.y: 90
            eulerRotation.z: 90
            clipFar: 5
            clipNear: 0.01
        }
    }


    PrincipledMaterial {
        id: defaultMaterial
        baseColor: "silver"
    }


    PrincipledMaterial {
        id: tcpMaterial
        baseColor: "green"
        opacity: 0.85
    }


    MouseArea {
        id: mouseArea

        anchors.fill: parent
        clip: true
        hoverEnabled: true
        acceptedButtons: Qt.LeftButton

        property bool isRotate: true

        property point lastPoint;

        onPressed: (mouse) => {
            lastPoint = Qt.point(mouse.x, mouse.y);
        }
        onPositionChanged: (mouse) => {
            if (mouse.buttons === 1) {
                const zoomSliderValue = (camera.zoomValue - 0.5) / 2;
                const deltaZoom = zoomSliderValue > 0.5 ? zoomSliderValue * 2 : zoomSliderValue + 0.5;
                const deltaX = -(mouse.x - lastPoint.x) * (isRotate ? 0.35 : 0.0025) * deltaZoom;
                const deltaY = (mouse.y - lastPoint.y) * 0.0025 * deltaZoom;
                if (isRotate) {
                    cameraNode.eulerRotation.z = (cameraNode.eulerRotation.z + deltaX);
                } else {
                    camera.position.y = Math.max(Math.min(camera.position.y + deltaX, 1.5), -1.5);
                }
                camera.position.z = Math.max(Math.min(camera.position.z + deltaY, 1.4), -1.4);
                lastPoint = Qt.point(mouse.x, mouse.y);
            }
        }
    }

    Node {
        id: robotBaseNode

        AxesHelper {
            scale: Qt.vector3d(0.01, 0.01, 0.01)
            lineWidth: 0.5
            lineLength: 100
            showArrow: false
            showLine: true
            enableXYGrid: true
        }

        Node {
            id: tcp

            visible: window.robotRunning

            position: Qt.vector3d(0, 0, 0)
            rotation: Qt.quaternion(1, 0, 0, 0)

            Model {
                scale: Qt.vector3d(0.0002, 0.0002, 0.0002)

                source: "#Sphere"

                materials: tcpMaterial
            }

            AxesHelper {
                scale: Qt.vector3d(0.01, 0.01, 0.01)
                lineWidth: robotModel === "N7" ? 0.3 : 0.2
                lineLength: robotModel === "N7" ? 30 : 20
            }
        }

        Model {
            id: link0Model
            visible: true

            source: "qrc:///model/link0.mesh"

            materials: defaultMaterial

            Node {
                id: link1Node

                position: Qt.vector3d(urdf[0].position[0], urdf[0].position[1], urdf[0].position[2])
                eulerRotation: Qt.vector3d(urdf[0].rotation[0], urdf[0].rotation[1], urdf[0].rotation[2])

                Model {
                    id: link1Model

                    source: "qrc:///model/link1.mesh"

                    materials: defaultMaterial

                    eulerRotation.y: 0.0

                    Node {
                        id:link2Node

                        eulerRotation.z: 0.0

                        Model {
                            id: link2Model

                            source: "qrc:///model/link2.mesh"

                            materials: defaultMaterial

                            position: Qt.vector3d(urdf[1].position[0], urdf[1].position[1], urdf[1].position[2])
                            eulerRotation: Qt.vector3d(urdf[1].rotation[0], urdf[1].rotation[1], urdf[1].rotation[2])

                            Node {
                                id: link3Node

                                eulerRotation.z: 0.0

                                Model {
                                    id: link3Model

                                    source: "qrc:///model/link3.mesh"

                                    materials: defaultMaterial

                                    position: Qt.vector3d(urdf[2].position[0], urdf[2].position[1], urdf[2].position[2])
                                    eulerRotation: Qt.vector3d(urdf[2].rotation[0], urdf[2].rotation[1], urdf[2].rotation[2])

                                    Node {
                                        id: link4Node

                                        eulerRotation.z: 0.0

                                        Model {
                                            id: link4Model

                                            source: "qrc:///model/link4.mesh"

                                            materials: defaultMaterial

                                            position: Qt.vector3d(urdf[3].position[0], urdf[3].position[1], urdf[3].position[2])
                                            eulerRotation: Qt.vector3d(urdf[3].rotation[0], urdf[3].rotation[1], urdf[3].rotation[2])

                                            Node {
                                                id: link5Node

                                                eulerRotation.z: 0.0

                                                Model {
                                                    id: link5Model

                                                    source: ("qrc:///model/link5.mesh")

                                                    materials: defaultMaterial

                                                    position: Qt.vector3d(urdf[4].position[0], urdf[4].position[1], urdf[4].position[2])
                                                    eulerRotation: Qt.vector3d(urdf[4].rotation[0], urdf[4].rotation[1], urdf[4].rotation[2])

                                                    Node {
                                                        id: link6Node

                                                        Model {
                                                            id: link6Model

                                                            source: ("qrc:///model/link6.mesh")

                                                            materials: defaultMaterial

                                                            position: Qt.vector3d(urdf[5].position[0], urdf[5].position[1], urdf[5].position[2])
                                                            eulerRotation: Qt.vector3d(urdf[5].rotation[0], urdf[5].rotation[1], urdf[5].rotation[2])
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
