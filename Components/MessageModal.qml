import QtQuick
import QtQuick.Controls
import "../Elements"

Popup {
    id: popup
    modal: true
    width: 360
    padding: 24
    anchors.centerIn: Overlay.overlay
    closePolicy: Popup.NoAutoClose

    enum Type {
        Info,
        Warning,
        Error,
        Success
    }

    property int type: MessageModal.Type.Info
    property string title: "Title"
    property string description: "Description"
    property string okText: "Ok"
    property string cancelText: "Cancel"
    property bool showCancel: false
    property bool showIcon: true
    property int okButtonType: PushButton.Danger
    property int cancelButtonType: PushButton.Default

    signal ok()
    signal cancel()

    enter: Transition {
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; duration: 100 }
    }

    exit: Transition {
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0; duration: 100 }
    }

    background: Rectangle {
        color: "#ffffff"
        border.width: 0
        radius: 12
        anchors.fill: parent
    }

    Column {
        anchors.fill: parent
        spacing: 24

        Component {
            id: iconItem

            Icon {
                source: {
                    switch (popup.type) {
                    case MessageModal.Info: return "qrc:///img/info";
                    case MessageModal.Success: return "qrc:///img/success";
                    case MessageModal.Warning: return "qrc:///img/warning";
                    case MessageModal.Error: return "qrc:///img/error";
                    default: return "qrc:///img/info";
                    }
                }
                color: {
                    switch (popup.type) {
                    case MessageModal.Info: return "#4682b4";
                    case MessageModal.Success: return "#52c41a";
                    case MessageModal.Warning: return "#ffc107";
                    case MessageModal.Error: return "#ff3333";
                    default: return "#4682b4";
                    }
                }
                size: 64
                height: 64
                width: popup.availableWidth
            }
        }

        Loader {
            sourceComponent: if (popup.showIcon) iconItem
        }

        Text {
            text: popup.title
            font.pixelSize: 18
            font.weight: Font.DemiBold
            height: 36
            anchors.left: parent.left
            anchors.right: parent.right
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: popup.description
            wrapMode: Text.WordWrap
            color: "#555555"
            font.pixelSize: 14
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 24
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Row {
            spacing: 24
            anchors.horizontalCenter: parent.horizontalCenter

            Component {
                id: cancelButton

                PushButton {
                    text: cancelText
                    type: cancelButtonType

                    onClicked: popup.cancel()
                }
            }

            Loader {
                sourceComponent: if (popup.showCancel) cancelButton
            }

            PushButton {
                text: okText
                type: okButtonType

                onClicked: popup.ok()
            }
        }
    }
}

